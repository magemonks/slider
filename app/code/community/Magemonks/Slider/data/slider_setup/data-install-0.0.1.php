<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
$slideshowtemplateData = array(
    'label' => 'Normal slider',
    'css_class' => 'normal',
    'width' => '960',
    'height' => '480',
    'full_width' => '0',
    'full_width_center_images' => '1',
    'background_color' => '#ffffff',
    'touch_enabled' => '1',
    'on_hover_stop' => '1',
    'navigation_toggle' => '0',
    'navigation_toggle_delay' => '2000',
    'responsive' => '0',
    'stop_after_loops' => '',
    'stop_at_slide' => '',
    'delay' => '5000',
    'transition_style' => 'fade',
    'transition_duration' => '300',
    'transition_slots' => '6',
    'loader_show' => '0',
    'loader_transition_duration' => '500',
    'init_transition_style' => 'fade',
    'init_transition_duration' => '500',
    'init_transition_slots' => '6',
    'timer_show' => '1',
    'timer_enable' => '1',
    'timer_position' => 'top',
    'timer_size' => '3',
    'timer_color' => '#ffffff',
    'timer_opacity' => '0.8',
    'bullets_show' => '1',
    'bullets_shape' => 'square',
    'bullets_horizontal_space' =>'5',
    'bullets_indicators_show' => '1',
    'bullets_arrows_show' => '0',
    'bullets_position' => 'bottom-right',
    'bullets_offset_horizontal' => '20',
    'bullets_offset_vertical' => '20',
    'bullets_background_color' => '#1166cc',
    'bullets_indicator_color' => '#ffffff',
    'bullets_opacity' => '1',
    'bullets_border_size' => '2',
    'bullets_border_color' => '#ffffff',
    'bullets_shadow_size' => '4',
    'bullets_shadow_opacity' => '0.8',
    'bullets_selected_background_color' => '#ffffff',
    'bullets_selected_indicator_color' => '#1166cc',
    'bullets_selected_opacity' => '1',
    'bullets_selected_border_color' => '#ffffff',
    'bullets_hover_background_color' => '#ffffff',
    'bullets_hover_indicator_color' => '#1166cc',
    'bullets_hover_opacity' => '1',
    'bullets_hover_border_color' => '#1166cc',
    'bullets_arrows_background_color' => '',
    'bullets_arrows_arrow_color' => '#ffffff',
    'bullets_arrows_opacity' => '1',
    'bullets_arrows_border_size' => '2',
    'bullets_arrows_border_color' => '#ffffff',
    'bullets_arrows_shadow_size' => '4',
    'bullets_arrows_shadow_opacity' => '0.8',
    'bullets_arrows_hover_background_color' => '',
    'bullets_arrows_hover_arrow_color' => '#ffffff',
    'bullets_arrows_hover_opacity' => '1',
    'bullets_arrows_hover_border_color' => '#ffffff',
    'arrows_show' => '0',
    'arrows_shape' => 'square',
    'arrows_position' => 'middle',
    'arrows_offset_horizontal' => '20',
    'arrows_offset_vertical' => '0',
    'arrows_background_color' => '',
    'arrows_arrow_color' => '#1166cc',
    'arrows_opacity' => '1',
    'arrows_border_size' => '3',
    'arrows_border_color' => '#ffffff',
    'arrows_shadow_size' => '2',
    'arrows_shadow_opacity' => '0.5',
    'arrows_hover_background_color' => '',
    'arrows_hover_arrow_color' => '#ffffff',
    'arrows_hover_opacity' => '1',
    'arrows_hover_border_color' => '#ffffff',
    'shadow_show' => '1',
    'shadow_style' => 'arch',
);
$slideshowTemplate = Mage::getModel('slider/slideshowtemplate')->set($slideshowtemplateData)->save();


/**
 * Caption templates
 */
$captionTemplateData1 = array(
    'label' => 'Text',
    'start' => '500',
    'position_x' => '0',
    'position_y' => '353',
    'css_class' => 'title',
    'animation_style' => 'sfl',
    'animation_easing' => 'easeOutExpo',
    'animation_duration' => '600',
);
$captionTemplate1 = Mage::getModel('slider/captiontemplate')->set($captionTemplateData1)->save();

$captionTemplateData2 = array(
    'label' => 'Button',
    'start' => '700',
    'position_x' => '0',
    'position_y' => '410',
    'css_class' => 'button',
    'animation_style' => 'sfb',
    'animation_easing' => 'easeOutExpo',
    'animation_duration' => '600',
);
$captionTemplate2 = Mage::getModel('slider/captiontemplate')->set($captionTemplateData2)->save();


/**
 * Slideshows
 */
$slideshowData = array(
    'slideshowtemplate_id'      => $slideshowTemplate->getId(),
    'identifier'                => 'home_slider',
    'label'                     => 'Home slider',
    'stores'                    => array(0),

);
$slideshow = Mage::getModel('slider/slideshow')->set($slideshowData)->save();



/**
 * Slides
 */
$slideData1 = array(
    'label'                     => 'Test 1',
    'image'                     => '/media/wysiwyg/slides/naf.jpg',
);
$slide1 = Mage::getModel('slider/slide')->set($slideData1);
$slideshow->addSlide($slide1);

$slideData2 = array(
    'label'                     => 'Test 2',
    'image'                     => '/media/wysiwyg/slides/naf.jpg',
);
$slide2 = Mage::getModel('slider/slide')->set($slideData2);
$slideshow->addSlide($slide2);

$slideData3 = array(
    'label'                     => 'Test 3',
    'image'                     => '/media/wysiwyg/slides/naf.jpg',
);
$slide3 = Mage::getModel('slider/slide')->set($slideData3);
$slideshow->addSlide($slide3);


/**
 * Captions
 */
$captionData1 = array(
    'label'                     => 'Caption 1',
    'content'                   => 'Titel',
);
$caption1 = Mage::getModel('slider/caption')
    ->set($captionData1)
    ->setCaptionTemplate($captionTemplate1);
$slide1->addCaption($caption1);


$captionData2 = array(
    'label'                     => 'Caption 2',
    'content'                   => 'Lorem ipsum dolor sit amet consectet',
);
$caption2 = Mage::getModel('slider/caption')
    ->set($captionData2)
    ->setCaptionTemplate($captionTemplate2);
$slide1->addCaption($caption2);


//$captionData3 = array(
//    'label'                     => 'Caption 3',
//    'start'                     => 3000,
//    'position_x'                => 100,
//    'position_y'                => 100,
//    'content'                   => 'My third caption',
//);
//$caption3 = Mage::getModel('slider/caption')
//    ->set($captionData3)
//    ->setCaptionTemplate($captionTemplate1);
//$slide1->addCaption($caption3);

