<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Helper_Loader extends Mage_Core_Helper_Abstract{

    /**
     * Retreives the item types from the config and returns the model path in an array
     * @return array
     */
    public function getSlideshowTypes()
    {
        $elements =  Mage::getConfig()->getFieldset('slider_slideshowtypes');
        $types = array();

        if(!Mage::helper('magemonks/license')->isSerialValid(Mage::helper('slider')->getSerial(), Mage::helper('slider')->getProductCode())){
            die(Mage::helper('slider')->getProductCode());
            return $types;
        }

        foreach($elements as $type => $element){
            if(isset($element->label) && !isset($element->disabled)){
                $types[$type] = (array) $element;
            }
        }
        return $types;
    }
}