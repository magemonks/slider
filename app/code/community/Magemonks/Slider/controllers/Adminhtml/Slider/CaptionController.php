<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Adminhtml_Slider_CaptionController extends Mage_Adminhtml_Controller_Action{

    protected $_slide = null;
    protected $_slideshow = null;
    protected $_sliderHelper = null;

    protected function _construct()
    {
        parent::_construct();
        $this->_sliderHelper = Mage::helper('slider');
        $this->setUsedModuleName('Magemonks_Slider');
        $this->_helper = Mage::helper('slider');
    }

    /**
     * Init the slideshow
     *
     * @return bool|Magemonks_Slider_Adminhtml_Slider_SlideController
     */
    protected function _initSlide()
    {
        $slide_id = $this->getRequest()->getParam('slide_id');
        if(!$slide_id){
            $slide_id = $this->getRequest()->getPost('slide_id');
        }

        if (!$slide_id) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('slider')->__('This slide no longer exists.'));
            $this->_redirect('*/slider_slide/');
            return false;
        }

        $slide = Mage::getModel('slider/slide')->load($slide_id);
        if (! $slide->getId()) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('slider')->__('This slide no longer exists.'));
            $this->_redirect('*/slider_slide/');
            return false;
        }

        $this->_slide = $slide;
        $this->_slideshow = $this->_slide->getSlideshow();
        Mage::register('slider_slide', $this->_slide);
        Mage::register('slider_slideshow', $this->_slideshow);

        return $this;
    }

    /**
     * Init layout, Slide and breadcrumb
     *
     * @return Magemonks_Slide_Adminhtml_SlideController
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('cms/slider')
            ->_addBreadcrumb(Mage::helper('slider')->__('CMS'), Mage::helper('slider')->__('CMS'))
            ->_addBreadcrumb(Mage::helper('slider')->__('Slider'), Mage::helper('slider')->__('Slider'))
            ->_addBreadcrumb(Mage::helper('slider')->__('Slideshow'), Mage::helper('slider')->__('Slideshow'))
            ->_addBreadcrumb(Mage::helper('slider')->__('Slide'), Mage::helper('slider')->__('Slide'))
            ->_title($this->__('CMS'))->_title($this->__('Slider'))->_title($this->__('Slideshow'))->_title($this->__('Edit Slide'));
        return $this;
    }

    /**
     * Index action
     */
    public function indexAction()
    {
        if(!$this->_initSlide()) return;
        $this->_initAction();
        $this->_title($this->__('Captions'));
        $this->renderLayout();
    }


    /**
     * Create action
     */
    public function newAction()
    {
        if(!$this->_initSlide()) return;

        $model = Mage::getModel('slider/caption');
        Mage::register('slider_caption', $model);

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (! empty($data)) {
            $model->setData($data);
        }

        $this->_initAction()
            ->_addBreadcrumb(Mage::helper('slider')->__('New Caption'), Mage::helper('slider')->__('New Caption'))
            ->_title($this->__('New Caption'))
            ->renderLayout();
    }


    /**
     * Edit action
     */
    public function editAction()
    {
        if(!$this->_initSlide()) return;

        $id = $this->getRequest()->getParam('id');

        if (!$id) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('slider')->__('This caption no longer exists.'));
            $this->_redirect('*/slider_caption/', array('slide_id' => $this->_slide->getId()));
            return;
        }

        $caption = Mage::getModel('slider/caption')->load($id);
        if (! $caption->getId()) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('slider')->__('This slide no longer exists.'));
            $this->_redirect('*/slider_caption/', array('slide_id' => $this->_slide->getId()));
            return;
        }

        $this->_title($caption->get('label'));

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (! empty($data)) {
            $caption->set($data);
        }

        Mage::register('slider_caption', $caption);

        $this->_initAction()
            ->_addBreadcrumb(Mage::helper('slider')->__('Edit Caption'), Mage::helper('slider')->__('Edit Caption'))
            ->_title($this->__('Edit Caption'))
            ->renderLayout();

    }

    /**
     * Save action
     */
    public function saveAction()
    {
        if(!$this->_initSlide()) return;

        // check if data sent
        if ($data = $this->getRequest()->getPost()) {

            $id = $this->getRequest()->getParam('id');
            $model = Mage::getModel('slider/caption')->load($id);
            if (!$model->getId() && $id) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('slider')->__('This caption no longer exists.'));
                $this->_redirect('*/slider_caption/', array('slide_id' => $this->_slide->getId()));
                return;
            }

            foreach($data as $key=>$value){
                if(!is_numeric($value) && empty($value)) $data[$key] = null;
            }

            // init model and set data
            $model->set($data);


            // try to save it
            try {
                // save the data
                $model->save();
                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('slider')->__('The caption has been saved.'));
                // clear previously saved data from session
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), 'slide_id' => $this->_slide->getId(), '_current'=>true));
                    return;
                }
                // go to grid
                $this->_redirect('*/*/', array('slide_id' => $this->_slide->getId()));
                return;

            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // save data in session
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                // redirect to edit form
                $this->_redirect('*/*/edit', array('id' => $model->getId(), 'slide_id' => $this->_slide->getId()));
                return;
            }
        }
        $this->_redirect('*/slider_slide/');
    }

    /**
     * Sort Action
     *
     * @return Magemonks_Menumanager_Adminhtml_Menumanager_ItemController
     */
    public function sortAction()
    {
        if(!$this->_initSlide()) return $this->_showAjaxResponse(array('error' => true, 'message' => $this->_sliderHelper->__('Slide not found')));

        if ($data = $this->getRequest()->getPost()) {
            if(!isset($data['itemData']) || !is_array($data['itemData'])){
                return $this->_showAjaxResponse(array('error' => true, 'message' => $this->_sliderHelper->__('Some random error')));
            }
            foreach($data['itemData'] as $index=>$captionId){
                $captionId = explode('-', $captionId);
                $captionId = end($captionId);
                $slide = Mage::getModel('slider/caption')->load($captionId);
                if($slide->getId()){
                    $slide->set('position', $index)->save();
                }
            }

            return $this->_showAjaxResponse();
        }
        else {
            return $this->_showAjaxResponse(array('error' => true, 'message' => $this->_sliderHelper->__('Some random error')));
        }
    }

    /**
     * Delete action
     */
    public function deleteAction()
    {
        if(!$this->_initSlide()) return;

        // check if we know what should be deleted
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                // init model and delete
                $model = Mage::getModel('slider/caption');
                $model->load($id);
                $label = $model->get('label');
                $model->delete();
                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The caption (%s) has been deleted.', $label));
                // go to grid
                $this->_redirect('*/*/', array('slide_id' => $this->_slide->getId()));
                return;

            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // go back to edit form
                $this->_redirect('*/*/', array('slide_id' => $this->_slide->getId()));
                return;
            }
        }
        // display error message
        Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find a caption to delete.'));
        // go to grid
        $this->_redirect('*/*/');
    }


    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('cms/slider');
    }

    /**
     * Show AJAX Response
     *
     * @param array $data
     * @return Magemonks_Menumanager_Adminhtml_Menumanager_ItemController
     */
    protected function _showAjaxResponse($data = array())
    {
        if(!isset($data['success'])){
            if(isset($data['error'])){
                $data['success'] = false;
            }
            else{
                $data['success'] = true;
            }
        }

        $json = Mage::helper('magemonks')->getJsonWrapper($data);
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($json));
        return $this;
    }
}