<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Adminhtml_Slider_CaptiontemplateController extends Mage_Adminhtml_Controller_Action{

    protected function _construct()
    {
        parent::_construct();
        $this->setUsedModuleName('Magemonks_Slider');
        $this->_helper = Mage::helper('slider');
    }

    /**
     * Init layout, slideshow and breadcrumb
     *
     * @return Magemonks_Slideshow_Adminhtml_SlideshowController
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('cms/slider')
            ->_addBreadcrumb($this->__('CMS'), $this->__('CMS'))
            ->_addBreadcrumb($this->__('Slider'), $this->__('Slider'))
            ->_addBreadcrumb($this->__('Caption Templates'), $this->__('Caption Templates'))
            ->_title($this->__('CMS'))
            ->_title($this->__('Slider'))
            ->_title($this->__('Caption Templates'));
        return $this;
    }

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->_initAction();
        $this->renderLayout();
    }

    /**
     * Create action
     */
    public function newAction()
    {
        $model = Mage::getModel('slider/captiontemplate');
        Mage::register('slider_captiontemplate', $model);

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (! empty($data)) {
            $model->set($data);
        }

        $this->_initAction();
        $this->_addBreadcrumb($this->__('New Caption template'), $this->__('New Caption template'));
        $this->_title($this->__('New Caption template'));
        $this->renderLayout();
    }

    /**
     * Edit action
     */
    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');

        if (!$id) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('This caption template no longer exists.'));
            $this->_redirect('*/*/');
            return;
        }

        $captiontemplate = Mage::getModel('slider/captiontemplate')->load($id);
        if (! $captiontemplate->getId()) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('This caption template no longer exists.'));
            $this->_redirect('*/*/');
            return;
        }

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (! empty($data)) {
            $captiontemplate->set($data);
        }

        Mage::register('slider_captiontemplate', $captiontemplate);

        $this->_initAction()
            ->_addBreadcrumb($captiontemplate->get('label'), $captiontemplate->get('label'))
            ->_title($captiontemplate->get('label'))
            ->renderLayout();
    }

    /**
     * Save action
     */
    public function saveAction()
    {
//        foreach($this->getRequest()->getParams() as $key => $value){
//            echo "'".$key."' => '".$value."',<br />";
//        }
//        die;
        // check if data sent
        if ($data = $this->getRequest()->getPost()) {

            $id = $this->getRequest()->getParam('id');
            $model = Mage::getModel('slider/captiontemplate')->load($id);
            if (!$model->getId() && $id) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('This caption template no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }

            foreach($data as $key=>$value){
                if(!is_numeric($value) && empty($value)) $data[$key] = null;
            }

            // init model and set data
            $model->set($data);

            // try to save it
            try {
                // save the data
                $model->save();
                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The caption template has been saved.'));
                // clear previously saved data from session
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), '_current'=>true));
                    return;
                }
                // go to grid
                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // save data in session
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                // redirect to edit form
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Delete action
     */
    public function deleteAction()
    {
        // check if we know what should be deleted
        if ($id = $this->getRequest()->getParam('id')) {
            $label = "";
            try {
                // init model and delete
                $model = Mage::getModel('slider/slideshow');
                $model->load($id);
                $label = $model->get('label');
                $model->delete();
                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The caption template (%s) has been deleted.', $label));
                // go to grid
                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // go back to edit form
                $this->_redirect('*/*/edit', array('id' => $id));
                return;
            }
        }
        // display error message
        Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find a caption template to delete.'));
        // go to grid
        $this->_redirect('*/*/');
    }
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('cms/slider');
    }

}