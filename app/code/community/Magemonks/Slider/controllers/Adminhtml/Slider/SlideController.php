<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Adminhtml_Slider_SlideController extends Mage_Adminhtml_Controller_Action{

    protected $_slideshow = null;
    protected $_sliderHelper = null;

    protected function _construct()
    {
        parent::_construct();
        $this->_sliderHelper = Mage::helper('slider');
        $this->setUsedModuleName('Magemonks_Slider');
        $this->_helper = Mage::helper('slider');
    }

    /**
     * Init the slideshow
     *
     * @return bool|Magemonks_Slider_Adminhtml_Slider_SlideController
     */
    protected function _initSlideshow()
    {
        $slideshow_id = $this->getRequest()->getParam('slideshow_id');
        if(!$slideshow_id){
            $slideshow_id = $this->getRequest()->getPost('slideshow_id');
        }

        if (!$slideshow_id) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('slider')->__('This slideshow no longer exists.'));
            $this->_redirect('*/slider_slideshow/');
            return false;
        }

        $slideshow = Mage::getModel('slider/slideshow')->load($slideshow_id);
        if (! $slideshow->getId()) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('slider')->__('This slideshow no longer exists.'));
            $this->_redirect('*/slider_slideshow/');
            return false;
        }

        $this->_slideshow = $slideshow;
        Mage::register('slider_slideshow', $slideshow);

        return $this;
    }

    /**
     * Init layout, Slide and breadcrumb
     *
     * @return Magemonks_Slide_Adminhtml_SlideController
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('cms/slider')
            ->_addBreadcrumb(Mage::helper('slider')->__('CMS'), Mage::helper('slider')->__('CMS'))
            ->_addBreadcrumb(Mage::helper('slider')->__('Slider'), Mage::helper('slider')->__('Slider'))
            ->_addBreadcrumb(Mage::helper('slider')->__('Slideshow'), Mage::helper('slider')->__('Slideshow'));

        return $this;
    }

    /**
     * Index action
     */
    public function indexAction()
    {
        if(!$this->_initSlideshow()) return;
        $this->_title($this->__('CMS'))->_title($this->__('Slider'))->_title($this->__('Slideshow'));
        $this->_initAction();
        $this->renderLayout();
    }


    /**
     * Create action
     */
    public function newAction()
    {
        if(!$this->_initSlideshow()) return;
        $this->_title($this->__('CMS'))->_title($this->__('Slider'))->_title($this->__('Slideshow'))->_title($this->__('New Slide'));

        $model = Mage::getModel('slider/slide');
        Mage::register('slider_slide', $model);

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (! empty($data)) {
            $model->setData($data);
        }

        $this->_initAction()
            ->_addBreadcrumb(Mage::helper('slider')->__('New Slide'), Mage::helper('slider')->__('New Slide'))
            ->renderLayout();
    }


    /**
     * Edit action
     */
    public function editAction()
    {
        if(!$this->_initSlideshow()) return;
        $this->_title($this->__('CMS'))->_title($this->__('Slider'))->_title($this->__('Slideshow'))->_title($this->__('Edit Slide'));

        $id = $this->getRequest()->getParam('id');

        if (!$id) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('slider')->__('This slide no longer exists.'));
            $this->_redirect('*/slider_slide/', array('slideshow_id' => $this->_slideshow->getId()));
            return;
        }

        $slide = Mage::getModel('slider/slide')->load($id);
        if (! $slide->getId()) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('slider')->__('This slide no longer exists.'));
            $this->_redirect('*/slider_slide/', array('slideshow_id' => $this->_slideshow->getId()));
            return;
        }

        $this->_title($slide->get('label'));

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (! empty($data)) {
            $slide->set($data);
        }

        Mage::register('slider_slide', $slide);

        $this->_initAction()
            ->_addBreadcrumb(Mage::helper('slider')->__('Edit Slide'), Mage::helper('slider')->__('Edit Slide'))
            ->renderLayout();

    }

    /**
     * Save action
     */
    public function saveAction()
    {
        if(!$this->_initSlideshow()) return;

        // check if data sent
        if ($data = $this->getRequest()->getPost()) {

            $id = $this->getRequest()->getParam('id');
            $model = Mage::getModel('slider/slide')->load($id);
            if (!$model->getId() && $id) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('slider')->__('This slide no longer exists.'));
                $this->_redirect('*/slider_slide/', array('slideshow_id' => $this->_slideshow->getId()));
                return;
            }

            foreach($data as $key=>$value){
                if(!is_numeric($value) && empty($value)) $data[$key] = null;
            }

            // init model and set data
            $model->set($data);


            // try to save it
            try {
                // save the data
                $model->save();
                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('slider')->__('The slide has been saved.'));
                // clear previously saved data from session
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), 'slideshow_id' => $this->_slideshow->getId()));
                    return;
                }
                // go to grid
                $this->_redirect('*/*/', array('slideshow_id' => $this->_slideshow->getId()));
                return;

            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // save data in session
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                // redirect to edit form
                $this->_redirect('*/*/edit', array('id' => $model->getId(), 'slideshow_id' => $this->_slideshow->getId()));
                return;
            }
        }
        $this->_redirect('*/slider_slideshow/');
    }

    /**
     * Sort Action
     *
     * @return Magemonks_Menumanager_Adminhtml_Menumanager_ItemController
     */
    public function sortAction()
    {
        if(!$this->_initSlideshow()) return $this->_showAjaxResponse(array('error' => true, 'message' => $this->_sliderHelper->__('Slideshow not found')));

        if ($data = $this->getRequest()->getPost()) {
            if(!isset($data['itemData']) || !is_array($data['itemData'])){
                return $this->_showAjaxResponse(array('error' => true, 'message' => $this->_sliderHelper->__('Some random error')));
            }
            foreach($data['itemData'] as $index=>$slideId){
                $slideId = explode('-', $slideId);
                $slideId = end($slideId);
                $slide = Mage::getModel('slider/slide')->load($slideId);
                if($slide->getId()){
                    $slide->set('position', $index)->save();
                }
            }

            return $this->_showAjaxResponse();
        }
        else {
            return $this->_showAjaxResponse(array('error' => true, 'message' => $this->_sliderHelper->__('Some random error')));
        }
    }

    /**
     * Delete action
     */
    public function deleteAction()
    {
        if(!$this->_initSlideshow()) return;

        // check if we know what should be deleted
        if ($id = $this->getRequest()->getParam('id')) {
            $label = "";
            try {
                // init model and delete
                $model = Mage::getModel('slider/slide');
                $model->load($id);
                $label = $model->get('label');
                $model->delete();
                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The slide (%s) has been deleted.', $label));
                // go to grid
                $this->_redirect('*/*/', array('slideshow_id' => $this->_slideshow->getId()));
                return;

            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // go back to edit form
                $this->_redirect('*/*/', array('slideshow_id' => $this->_slideshow->getId()));
                return;
            }
        }
        // display error message
        Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find a slideshow to delete.'));
        // go to grid
        $this->_redirect('*/*/');
    }


    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('cms/slider');
    }

    /**
     * Show AJAX Response
     *
     * @param array $data
     * @return Magemonks_Menumanager_Adminhtml_Menumanager_ItemController
     */
    protected function _showAjaxResponse($data = array())
    {
        if(!isset($data['success'])){
            if(isset($data['error'])){
                $data['success'] = false;
            }
            else{
                $data['success'] = true;
            }
        }

        $json = Mage::helper('magemonks')->getJsonWrapper($data);
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($json));
        return $this;
    }
}