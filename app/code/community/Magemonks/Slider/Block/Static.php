<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Block_Static extends Magemonks_GeneratedStatic_Block_Abstract {

    public function _construct()
    {
        $this->addData(array(
            'cache_lifetime'=> false,
            'cache_tags'    => array(
                Magemonks_Slider_Model_Slideshow::CACHE_TAG,
                Mage_Core_Model_Store::CACHE_TAG,
                Mage_Core_Model_Store_Group::CACHE_TAG,
            )
        ));

        //Set Default templates.
        if(!$this->hasData('css_template')){
            $this->setData('css_template', 'magemonks/slider/static/css.phtml');
        }
        if(!$this->hasData('js_template')){
            $this->setData('js_template', 'magemonks/slider/static/js.phtml');
        }

        parent::_construct();
    }
}