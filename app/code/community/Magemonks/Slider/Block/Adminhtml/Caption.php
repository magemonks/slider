<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Block_Adminhtml_Caption extends Mage_Adminhtml_Block_Widget_Grid_Container {


    public function __construct()
    {
        $this->_controller = 'adminhtml_caption';
        $this->_blockGroup = 'slider';
        $this->_headerText = Mage::helper('slider')->__('Captions (for Slide: %s)', Mage::registry('slider_slide')->get('label'));
        $this->_addButtonLabel = Mage::helper('slider')->__('Add New Caption');
        $this->_backButtonLabel = Mage::helper('slider')->__('Back');
        $this->_addBackButton();
        parent::__construct();
    }

    public function getBackUrl()
    {
        return $this->getUrl('*/slider_slide/', array('slideshow_id' => Mage::registry('slider_slideshow')->getId(), '_current' => true));
    }

    public function getCreateUrl()
    {
        return $this->getUrl('*/slider_caption/new', array('_current' => true));
    }
}