<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Block_Adminhtml_Caption_Edit_Tab_Animation
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Load Wysiwyg on demand and Prepare layout
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

    protected function _prepareForm()
    {
        /** @var $model Magemonks_Slider_Model_Caption */
        $model = Mage::registry('slider_caption');

        $isElementDisabled = $this->_isAllowedAction('save') ? false : true;

        $form = new Magemonks_Data_Form();
        $form->setHtmlIdPrefix('caption_');

        $fieldset = $form->addFieldset('animation_fieldset', array('legend'=>Mage::helper('slider')->__('Animation'), 'class' => 'fieldset-wide'));

        $fieldset->addField('animation_style', 'select', array(
            'name'      => 'animation_style',
            'label'     => Mage::helper('slider')->__('Animation style'),
            'title'     => Mage::helper('slider')->__('Animation style'),
            'required'  => false,
            'note'      => Mage::helper('slider')->__('Animation style. When empty, the Caption Template value will be used. (Default: Empty)'),
            'values'    => Mage::getModel('slider/source_captionanimation')->toOptionArray(false),
            'disabled'  => $isElementDisabled,
        ));

        $fieldset->addField('animation_easing', 'select', array(
            'name'      => 'animation_easing',
            'label'     => Mage::helper('slider')->__('Animation easing'),
            'title'     => Mage::helper('slider')->__('Animation easing'),
            'required'  => false,
            'note'      => Mage::helper('slider')->__('Animation easing. When empty, the Caption Template value will be used.  (Default: Empty)'),
            'values'    => Mage::getModel('slider/source_easing')->toOptionArray(false),
            'disabled'  => $isElementDisabled,
        ));

        $fieldset->addField('animation_duration', 'text', array(
            'name'      => 'animation_duration',
            'label'     => Mage::helper('slider')->__('Animation duration (miliseconds)'),
            'title'     => Mage::helper('slider')->__('Animation duration (miliseconds)'),
            'required'  => false,
            'note'      => Mage::helper('slider')->__('The caption will animate for the given amount of miliseconds. When empty, the Caption Template value will be used.  (Default: Empty)'),
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));

        $form->setValues($model->get(), true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('slider')->__('Animation');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('slider')->__('Animation');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('slider/caption/' . $action);
    }
}
