<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Block_Adminhtml_Slideshow_Edit_Embed extends Mage_Core_Block_Template
{

    /**
     * Get's the slideshow identifier
     * @return mixed
     */
    public function getIdentifier(){
        return Mage::registry('slider_slideshow')->get('identifier');
    }

    protected function _toHtml(){
        if (!Mage::registry('slider_slideshow')) {
            return '';
        }
        else return parent::_toHtml();
    }
}