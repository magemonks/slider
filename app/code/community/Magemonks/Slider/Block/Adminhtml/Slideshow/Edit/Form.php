<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Block_Adminhtml_Slideshow_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Init form
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('slideshow_form');
        $this->setTitle(Mage::helper('slider')->__('Slideshow Information'));
    }

    /**
     * Load Wysiwyg on demand and Prepare layout
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

    protected function _prepareForm()
    {
        $model = Mage::registry('slider_slideshow');

        $form = new Magemonks_Data_Form(
            array('id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post')
        );

        $form->setHtmlIdPrefix('slideshow_');

        $fieldset = $form->addFieldset('base_fieldset', array('legend'=>Mage::helper('slider')->__('General'), 'class' => 'fieldset-wide'));

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', array(
                'name' => 'id',
            ));
        }

        $fieldset->addField('label', 'text', array(
            'name'      => 'label',
            'label'     => Mage::helper('slider')->__('Slideshow Label'),
            'title'     => Mage::helper('slider')->__('Slideshow Label'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The label of the slideshow. For internal reference only. Not displayed on the front-end'),
        ));

        $fieldset->addField('identifier', 'text', array(
            'name'      => 'identifier',
            'label'     => Mage::helper('slider')->__('Identifier'),
            'title'     => Mage::helper('slider')->__('Identifier'),
            'required'  => true,
            'class'     => 'validate-xml-identifier',
            'note'      => Mage::helper('slider')->__('Used to load the slideshow'),
        ));

        /**
         * Check is single store mode
         */
        if (!Mage::app()->isSingleStoreMode()) {
            $field =$fieldset->addField('store_id', 'multiselect', array(
                'name'      => 'stores[]',
                'label'     => Mage::helper('slider')->__('Store View'),
                'title'     => Mage::helper('slider')->__('Store View'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
                'note'      => Mage::helper('slider')->__('Select the stores where this slideshow is visible'),
                'class'     => 'validate-select',
            ));
            $renderer = $this->getLayout()->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
            $field->setRenderer($renderer);
        }
        else {
            $fieldset->addField('store_id', 'hidden', array(
                'name'      => 'stores[]',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
            $model->setStoreId(Mage::app()->getStore(true)->getId());
        }

        $fieldset->addField('is_active', 'select', array(
            'label'     => Mage::helper('slider')->__('Status'),
            'title'     => Mage::helper('slider')->__('Status'),
            'name'      => 'is_active',
            'required'  => true,
            'options'   => array(
                '1' => Mage::helper('slider')->__('Enabled'),
                '0' => Mage::helper('slider')->__('Disabled'),
            ),
        ));
        if (!$model->getId()) {
            $model->set('is_active', '1');
        }


        $fieldset = $form->addFieldset('Slideshow', array('legend'=>Mage::helper('slider')->__('Slideshow'), 'class' => 'fieldset-wide'));

        $fieldset->addField('slideshowtemplate_id', 'select', array(
            'name'      => 'slideshowtemplate_id',
            'label'     => Mage::helper('slider')->__('Slideshow Template'),
            'title'     => Mage::helper('slider')->__('Slideshow Template'),
            'note'      => Mage::helper('slider')->__('Select the slideshow template.'),
            'required'  => true,
            'values'   => Mage::getModel('slider/slideshowtemplate')->getCollection()->toOptionArray(false)
        ));


        $form->setValues($model->get(), true);
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}
