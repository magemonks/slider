<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Block_Adminhtml_Slide extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct()
    {
        $slideshow = Mage::registry('slider_slideshow');
        $this->_controller = 'adminhtml_slide';
        $this->_blockGroup = 'slider';
        $this->_headerText = Mage::helper('slider')->__('Slides (for Slideshow: %s)', $slideshow->get('label'));
        $this->_addButtonLabel = Mage::helper('slider')->__('Add New Slide');
        $this->_backButtonLabel = Mage::helper('slider')->__('Back');
        $this->_addBackButton();
        parent::__construct();
    }

    public function getBackUrl()
    {
        return $this->getUrl('*/slider_slideshow/');
    }

    public function getCreateUrl()
    {
        $slideshow = Mage::registry('slider_slideshow');
        return $this->getUrl('*/slider_slide/new', array('slideshow_id' => $slideshow->getId()));
    }

}