<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Block_Adminhtml_Captiontemplate_Edit_Tab_Style
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Load Wysiwyg on demand and Prepare layout
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

    protected function _prepareForm()
    {
        /** @var $model Magemonks_Slider_Model_Captiontemplate */
        $model = Mage::registry('slider_captiontemplate');

        $isElementDisabled = $this->_isAllowedAction('save') ? false : true;

        $form = new Magemonks_Data_Form();
        $form->setHtmlIdPrefix('captiontemplate_');

        $fieldset = $form->addFieldset('style_fieldset', array('legend'=>Mage::helper('slider')->__('Style'), 'class' => 'fieldset-wide'));

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'text_color',
            'label'     => Mage::helper('slider')->__('Text color'),
            'title'     => Mage::helper('slider')->__('Text color'),
            'note'      => Mage::helper('slider')->__('Text color of the caption. Leave empty to use the text color defined in the Caption Template. (Default: empty)'),
            'required'  => false,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('text_color');
        $fieldset->addElement($field);

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'background_color',
            'label'     => Mage::helper('slider')->__('Background color'),
            'title'     => Mage::helper('slider')->__('Background color'),
            'note'      => Mage::helper('slider')->__('Background color of the caption. Leave empty to use the background color defined in the CSS. (Default: empty)'),
            'required'  => false,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('background_color');
        $fieldset->addElement($field);


        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'border_color',
            'label'     => Mage::helper('slider')->__('Border color'),
            'title'     => Mage::helper('slider')->__('Border color'),
            'note'      => Mage::helper('slider')->__('Border color of the caption. Leave empty to use the border color defined in the CSS. (Default: empty)'),
            'required'  => false,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('border_color');
        $fieldset->addElement($field);




        $fieldset = $form->addFieldset('style_hover_fieldset', array('legend'=>Mage::helper('slider')->__('Hover Style'), 'class' => 'fieldset-wide'));
        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'text_hover_color',
            'label'     => Mage::helper('slider')->__('Text color'),
            'title'     => Mage::helper('slider')->__('Text color'),
            'note'      => Mage::helper('slider')->__('Text color of the caption. Leave empty to use the text color defined in the CSS. (Default: empty)'),
            'required'  => false,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('text_hover_color');
        $fieldset->addElement($field);

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'background_hover_color',
            'label'     => Mage::helper('slider')->__('Background color'),
            'title'     => Mage::helper('slider')->__('Background color'),
            'note'      => Mage::helper('slider')->__('Background color of the caption. Leave empty to use the background color defined in the CSS. (Default: empty)'),
            'required'  => false,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('background_hover_color');
        $fieldset->addElement($field);


        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'border_hover_color',
            'label'     => Mage::helper('slider')->__('Border color'),
            'title'     => Mage::helper('slider')->__('Border color'),
            'note'      => Mage::helper('slider')->__('Border color of the caption. Leave empty to use the border color defined in the CSS. (Default: empty)'),
            'required'  => false,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('border_hover_color');
        $fieldset->addElement($field);

        $form->setValues($model->get(), true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('slider')->__('Style');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('slider')->__('Style');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('slider/captiontemplate/' . $action);
    }
}
