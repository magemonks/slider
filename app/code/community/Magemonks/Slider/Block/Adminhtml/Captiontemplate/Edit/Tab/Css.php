<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Block_Adminhtml_Captiontemplate_Edit_Tab_Css
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Load Wysiwyg on demand and Prepare layout
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

    protected function _prepareForm()
    {
        /** @var $model Magemonks_Slider_Model_Captiontemplate */
        $model = Mage::registry('slider_captiontemplate');

        $isElementDisabled = $this->_isAllowedAction('save') ? false : true;

        $form = new Magemonks_Data_Form();
        $form->setHtmlIdPrefix('captiontemplate_');

        $fieldset = $form->addFieldset('css_fieldset', array('legend'=>Mage::helper('slider')->__('CSS'), 'class' => 'fieldset-wide'));

        $fieldset->addField('css_class', 'select', array(
            'name'      => 'css_class',
            'label'     => Mage::helper('slider')->__('CSS Class'),
            'title'     => Mage::helper('slider')->__('CSS Class'),
            'required'  => false,
            'note'      => Mage::helper('slider')->__('CSS Class. Classes are fetched from /skin/[yourpackage]/[yourskin]/magemonks/slider/css/captions.css. All classes should be in the format ".slider .caption.[yourname]{ ... }"'),
            'value'     => 'fade',
            'values'    => Mage::getModel('slider/source_captioncssclass')->toOptionArray(false),
            'disabled'  => $isElementDisabled,
        ));

        $fieldset->addField('css_custom', 'textarea', array(
            'name'      => 'css_custom',
            'label'     => Mage::helper('slider')->__('Custom CSS'),
            'title'     => Mage::helper('slider')->__('Custom CSS'),
            'required'  => false,
            'note'      => Mage::helper('slider')->__('Custom CSS. Don\'t wrap in curly braces {}. The CSS classname will be automatically inserted. The attributes color / background-color / border-color can be overriden per caption.'),
            'disabled'  => $isElementDisabled,
        ));

        $fieldset->addField('css_hover_custom', 'textarea', array(
            'name'      => 'css_hover_custom',
            'label'     => Mage::helper('slider')->__('Custom CSS (Hover)'),
            'title'     => Mage::helper('slider')->__('Custom CSS (Hover)'),
            'required'  => false,
            'note'      => Mage::helper('slider')->__('Custom CSS on hover (:hover). Don\'t wrap in curly braces {}. The CSS classname will be automatically inserted. The attributes color / background-color / border-color can be overriden per caption.'),
            'disabled'  => $isElementDisabled,
        ));

        $form->setValues($model->get(), true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('slider')->__('CSS');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('slider')->__('CSS');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('slider/captiontemplate/' . $action);
    }
}
