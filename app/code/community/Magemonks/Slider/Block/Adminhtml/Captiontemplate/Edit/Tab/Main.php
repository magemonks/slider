<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Block_Adminhtml_Captiontemplate_Edit_Tab_Main
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Load Wysiwyg on demand and Prepare layout
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

    protected function _prepareForm()
    {
        /** @var $model Magemonks_Slider_Model_Captiontemplate */
        $model = Mage::registry('slider_captiontemplate');

        $isElementDisabled = $this->_isAllowedAction('save') ? false : true;

        $form = new Magemonks_Data_Form();
        $form->setHtmlIdPrefix('captiontemplate_');

        $fieldset = $form->addFieldset('main_fieldset', array('legend'=>Mage::helper('slider')->__('Main'), 'class' => 'fieldset-wide'));

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', array(
                'name' => 'id',
            ));
        }

        $fieldset->addField('label', 'text', array(
            'name'      => 'label',
            'label'     => Mage::helper('slider')->__('Label'),
            'title'     => Mage::helper('slider')->__('Label'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The label of the captiontemplate. For internal reference only. Not displayed on the front-end'),
            'disabled'  => $isElementDisabled,
        ));

        $fieldset->addField('start', 'text', array(
            'name'      => 'start',
            'label'     => Mage::helper('slider')->__('Start after (miliseconds)'),
            'title'     => Mage::helper('slider')->__('Start after (miliseconds)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The caption will animate in after the given amount of miliseconds. Can be overriden per caption.'),
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));

        $fieldset->addField('position_x', 'text', array(
            'name'      => 'position_x',
            'label'     => Mage::helper('slider')->__('X Position (pixels)'),
            'title'     => Mage::helper('slider')->__('X Position (pixels)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The X position of the caption relative from the left. Can be a negative number. Can be overriden per caption.'),
            'class'     => 'validate-number',
            'disabled'  => $isElementDisabled,
        ));

        $fieldset->addField('position_y', 'text', array(
            'name'      => 'position_y',
            'label'     => Mage::helper('slider')->__('Y Position (pixels)'),
            'title'     => Mage::helper('slider')->__('Y Position (pixels)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The Y position of the caption relative from the left. Can be a negative number. Can be overriden per caption.'),
            'class'     => 'validate-number',
            'disabled'  => $isElementDisabled,
        ));

        $form->setValues($model->get(), true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('slider')->__('Main');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('slider')->__('Main');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('slider/captiontemplate/' . $action);
    }
}
