<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Block_Adminhtml_Slideshowtemplate_Edit_Tab_Bullets
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    protected function _prepareForm()
    {
        /** @var $model Magemonks_Slider_Model_Slideshowtemplate */
        $model = Mage::registry('slider_slideshowtemplate');

        $isElementDisabled = $this->_isAllowedAction('save') ? false : true;

        $form = new Magemonks_Data_Form();
        $form->setHtmlIdPrefix('slideshowtemplate_');

        /*
         *  BULLETS
         */
        $fieldset = $form->addFieldset('bullets_fieldset', array('legend'=>Mage::helper('slider')->__('Bullets'), 'class' => 'fieldset-wide'));
        $fieldset->addField('bullets_show', 'select', array(
            'name'      => 'bullets_show',
            'label'     => Mage::helper('slider')->__('Show bullets'),
            'title'     => Mage::helper('slider')->__('Show bullets'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('Show navigation bullets. (Default: On)'),
            'values'    => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
            'value'     => 1,
            'class'     => 'validate-select',
        ));
        $fieldset->addField('bullets_indicators_show', 'select', array(
            'name'      => 'bullets_indicators_show',
            'label'     => Mage::helper('slider')->__('Show bullets indicators'),
            'title'     => Mage::helper('slider')->__('Show bullets indicators'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('Show indicators (numbers) inside the bullets. (Default: On)'),
            'values'    => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
            'value'     => 1,
            'class'     => 'validate-select',
        ));
        $fieldset->addField('bullets_arrows_show', 'select', array(
            'name'      => 'bullets_arrows_show',
            'label'     => Mage::helper('slider')->__('Show bullets arrows'),
            'title'     => Mage::helper('slider')->__('Show bullets arrows'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('Show arrows (previous/next) outside the bullets. (Default: On)'),
            'values'    => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
            'value'     => 1,
            'class'     => 'validate-select',
        ));
        $fieldset->addField('bullets_position', 'select', array(
            'name'      => 'bullets_position',
            'label'     => Mage::helper('slider')->__('Bullets position'),
            'title'     => Mage::helper('slider')->__('Bullets position'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The position of the bullets, relative to the slider (Default: bottom)'),
            'value'     => 'bottom',
            'values'    => Mage::getModel('slider/source_bulletsposition')->toOptionArray(false),
            'class'     => 'validate-select',
        ));
        $fieldset->addField('bullets_shape', 'select', array(
            'name'      => 'bullets_shape',
            'label'     => Mage::helper('slider')->__('Bullets shape'),
            'title'     => Mage::helper('slider')->__('Bullets shape'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('Bullets shape. Note: rounded bullets might not work in every browser, they will be shown as square bullets automatically.'),
            'value'     => 'round',
            'values'    => Mage::getModel('slider/source_bulletsshape')->toOptionArray(false),
        ));

        $fieldset->addField('bullets_horizontal_space', 'text', array(
            'name'      => 'bullets_horizontal_space',
            'label'     => Mage::helper('slider')->__('Bullets horizontal space (pixels)'),
            'title'     => Mage::helper('slider')->__('Bullets horizontal space  (pixels)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The horizontal space between the bullets. (Default: 5)'),
            'value'     => 5,
            'class'     => 'validate-not-negative-number',
        ));

        $fieldset->addField('bullets_offset_horizontal', 'text', array(
            'name'      => 'bullets_offset_horizontal',
            'label'     => Mage::helper('slider')->__('Bullets horizontal offset (pixels)'),
            'title'     => Mage::helper('slider')->__('Bullets horizontal offset (pixels)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The horizontal offset of the bullets. Can be a negative number. (Default: 0)'),
            'value'     => 0,
            'class'     => 'validate-number'
        ));
        $fieldset->addField('bullets_offset_vertical', 'text', array(
            'name'      => 'bullets_offset_vertical',
            'label'     => Mage::helper('slider')->__('Bullets vertical offset (pixels)'),
            'title'     => Mage::helper('slider')->__('Bullets vertical offset (pixels)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The vertical offset of the bullets. Can be a negative number. (Default: 20)'),
            'value'     => 20,
            'class'     => 'validate-number'
        ));

        /*
         *  BULLETS STYLE
         */
        $fieldset = $form->addFieldset('bullets_style_fieldset', array('legend'=>Mage::helper('slider')->__('Bullets style'), 'class' => 'fieldset-wide'));

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'bullets_background_color',
            'label'     => Mage::helper('slider')->__('Background color'),
            'title'     => Mage::helper('slider')->__('Background color'),
            'note'      => Mage::helper('slider')->__('Background color of the bullet. Leave empty for transparent. (Default: empty)'),
            'required'  => false,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('bullets_background_color');
        $fieldset->addElement($field);

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'bullets_indicator_color',
            'label'     => Mage::helper('slider')->__('Indicator color'),
            'title'     => Mage::helper('slider')->__('Indicator color'),
            'note'      => Mage::helper('slider')->__('Indicator color of the bullet. (Default: #ffffff)'),
            'value'     => "#ffffff",
            'required'  => true,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('bullets_indicator_color');
        $fieldset->addElement($field);

        $fieldset->addField('bullets_opacity', 'text', array(
            'name'      => 'bullets_opacity',
            'label'     => Mage::helper('slider')->__('Opacity (0-1)'),
            'title'     => Mage::helper('slider')->__('Opacity (0-1)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The bullets opacity. (Default: 1)'),
            'value'     => 1,
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));

        $fieldset->addField('bullets_border_size', 'text', array(
            'name'      => 'bullets_border_size',
            'label'     => Mage::helper('slider')->__('Border size (pixels)'),
            'title'     => Mage::helper('slider')->__('Border size (pixels)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The bullets border size. (Default: 2)'),
            'value'     => 2,
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'bullets_border_color',
            'label'     => Mage::helper('slider')->__('Border color'),
            'title'     => Mage::helper('slider')->__('Border color'),
            'note'      => Mage::helper('slider')->__('Border color of the bullet. (Default: #ffffff)'),
            'value'     => "#ffffff",
            'required'  => true,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('bullets_border_color');
        $fieldset->addElement($field);

        $fieldset->addField('bullets_shadow_size', 'text', array(
            'name'      => 'bullets_shadow_size',
            'label'     => Mage::helper('slider')->__('Shadow size (pixels)'),
            'title'     => Mage::helper('slider')->__('Shadow size (pixels)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The bullets shadow size. (Default: 2)'),
            'value'     => 2,
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));

        $fieldset->addField('bullets_shadow_opacity', 'text', array(
            'name'      => 'bullets_shadow_opacity',
            'label'     => Mage::helper('slider')->__('Shadow opacity (0-1)'),
            'title'     => Mage::helper('slider')->__('Shadow opacity (0-1)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The bullets shadow opacity. (Default: 0.5)'),
            'value'     => 0.5,
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));


        /*
        *  BULLETS SELECTED STYLE
        */
        $fieldset = $form->addFieldset('bullets_selected_style_fieldset', array('legend'=>Mage::helper('slider')->__('Bullets selected style'), 'class' => 'fieldset-wide'));
        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'bullets_selected_background_color',
            'label'     => Mage::helper('slider')->__('Background color'),
            'title'     => Mage::helper('slider')->__('Background color'),
            'note'      => Mage::helper('slider')->__('Background color of the bullet. Leave empty for transparent. (Default: empty)'),
            'required'  => false,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('bullets_selected_background_color');
        $fieldset->addElement($field);

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'bullets_selected_indicator_color',
            'label'     => Mage::helper('slider')->__('Indicator color'),
            'title'     => Mage::helper('slider')->__('Indicator color'),
            'note'      => Mage::helper('slider')->__('Indicator color of the bullet. (Default: #ffffff)'),
            'value'     => "#ffffff",
            'required'  => true,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('bullets_selected_indicator_color');
        $fieldset->addElement($field);

        $fieldset->addField('bullets_selected_opacity', 'text', array(
            'name'      => 'bullets_selected_opacity',
            'label'     => Mage::helper('slider')->__('Opacity (0-1)'),
            'title'     => Mage::helper('slider')->__('Opacity (0-1)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The bullets opacity. (Default: 1)'),
            'value'     => 1,
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'bullets_selected_border_color',
            'label'     => Mage::helper('slider')->__('Border color'),
            'title'     => Mage::helper('slider')->__('Border color'),
            'note'      => Mage::helper('slider')->__('Border color of the bullet. (Default: #ffffff)'),
            'value'     => "#ffffff",
            'required'  => true,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('bullets_selected_border_color');
        $fieldset->addElement($field);



        /*
        *  BULLETS HOVER STYLE
        */
        $fieldset = $form->addFieldset('bullets_hover_style_fieldset', array('legend'=>Mage::helper('slider')->__('Bullets hover style'), 'class' => 'fieldset-wide'));
        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'bullets_hover_background_color',
            'label'     => Mage::helper('slider')->__('Background color'),
            'title'     => Mage::helper('slider')->__('Background color'),
            'note'      => Mage::helper('slider')->__('Background color of the bullet. Leave empty for transparent. (Default: empty)'),
            'required'  => false,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('bullets_hover_background_color');
        $fieldset->addElement($field);

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'bullets_hover_indicator_color',
            'label'     => Mage::helper('slider')->__('Indicator color'),
            'title'     => Mage::helper('slider')->__('Indicator color'),
            'note'      => Mage::helper('slider')->__('Indicator color of the bullet. (Default: #ffffff)'),
            'value'     => "#ffffff",
            'required'  => true,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('bullets_hover_indicator_color');
        $fieldset->addElement($field);

        $fieldset->addField('bullets_hover_opacity', 'text', array(
            'name'      => 'bullets_hover_opacity',
            'label'     => Mage::helper('slider')->__('Opacity (0-1)'),
            'title'     => Mage::helper('slider')->__('Opacity (0-1)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The bullets opacity. (Default: 1)'),
            'value'     => 1,
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'bullets_hover_border_color',
            'label'     => Mage::helper('slider')->__('Border color'),
            'title'     => Mage::helper('slider')->__('Border color'),
            'note'      => Mage::helper('slider')->__('Border color of the bullet. (Default: #ffffff)'),
            'value'     => "#ffffff",
            'required'  => true,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('bullets_hover_border_color');
        $fieldset->addElement($field);




        /*
        *  BULLETS ARROW STYLE
        */
        $fieldset = $form->addFieldset('bullets_arrows_style_fieldset', array('legend'=>Mage::helper('slider')->__('Arrow style'), 'class' => 'fieldset-wide'));

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'bullets_arrows_background_color',
            'label'     => Mage::helper('slider')->__('Background color'),
            'title'     => Mage::helper('slider')->__('Background color'),
            'note'      => Mage::helper('slider')->__('Background color of the arrow. Leave empty for transparent. (Default: empty)'),
            'required'  => false,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('bullets_arrows_background_color');
        $fieldset->addElement($field);

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'bullets_arrows_arrow_color',
            'label'     => Mage::helper('slider')->__('Arrow color'),
            'title'     => Mage::helper('slider')->__('Arrow color'),
            'note'      => Mage::helper('slider')->__('Color of the arrow. (Default: #ffffff)'),
            'value'     => "#ffffff",
            'required'  => true,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('bullets_arrows_arrow_color');
        $fieldset->addElement($field);

        $fieldset->addField('bullets_arrows_opacity', 'text', array(
            'name'      => 'bullets_arrows_opacity',
            'label'     => Mage::helper('slider')->__('Opacity (0-1)'),
            'title'     => Mage::helper('slider')->__('Opacity (0-1)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The arrow opacity. (Default: 1)'),
            'value'     => 1,
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));

        $fieldset->addField('bullets_arrows_border_size', 'text', array(
            'name'      => 'bullets_arrows_border_size',
            'label'     => Mage::helper('slider')->__('Border size (pixels)'),
            'title'     => Mage::helper('slider')->__('Border size (pixels)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The arrow border size. (Default: 2)'),
            'value'     => 2,
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'bullets_arrows_border_color',
            'label'     => Mage::helper('slider')->__('Border color'),
            'title'     => Mage::helper('slider')->__('Border color'),
            'note'      => Mage::helper('slider')->__('Border color of the arrow. (Default: #ffffff)'),
            'value'     => "#ffffff",
            'required'  => true,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('bullets_arrows_border_color');
        $fieldset->addElement($field);

        $fieldset->addField('bullets_arrows_shadow_size', 'text', array(
            'name'      => 'bullets_arrows_shadow_size',
            'label'     => Mage::helper('slider')->__('Shadow size (pixels)'),
            'title'     => Mage::helper('slider')->__('Shadow size (pixels)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The arrow shadow size. (Default: 2)'),
            'value'     => 2,
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));

        $fieldset->addField('bullets_arrows_shadow_opacity', 'text', array(
            'name'      => 'bullets_arrows_shadow_opacity',
            'label'     => Mage::helper('slider')->__('Shadow opacity (0-1)'),
            'title'     => Mage::helper('slider')->__('Shadow opacity (0-1)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The arrow shadow opacity. (Default: 0.5)'),
            'value'     => 0.5,
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));



        /*
        *  BULLETS ARROW HOVER STYLE
        */
        $fieldset = $form->addFieldset('bullets_arrows_hover_style_fieldset', array('legend'=>Mage::helper('slider')->__('Arrow hover style'), 'class' => 'fieldset-wide'));

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'bullets_arrows_hover_background_color',
            'label'     => Mage::helper('slider')->__('Background color'),
            'title'     => Mage::helper('slider')->__('Background color'),
            'note'      => Mage::helper('slider')->__('Background color of the arrow. Leave empty for transparent. (Default: empty)'),
            'required'  => false,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('bullets_arrows_hover_background_color');
        $fieldset->addElement($field);

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'bullets_arrows_hover_arrow_color',
            'label'     => Mage::helper('slider')->__('Arrow color'),
            'title'     => Mage::helper('slider')->__('Arrow color'),
            'note'      => Mage::helper('slider')->__('Color of the arrow. (Default: #ffffff)'),
            'value'     => "#ffffff",
            'required'  => true,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('bullets_arrows_hover_arrow_color');
        $fieldset->addElement($field);

        $fieldset->addField('bullets_arrows_hover_opacity', 'text', array(
            'name'      => 'bullets_arrows_hover_opacity',
            'label'     => Mage::helper('slider')->__('Opacity (0-1)'),
            'title'     => Mage::helper('slider')->__('Opacity (0-1)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The arrow opacity. (Default: 1)'),
            'value'     => 1,
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'bullets_arrows_hover_border_color',
            'label'     => Mage::helper('slider')->__('Border color'),
            'title'     => Mage::helper('slider')->__('Border color'),
            'note'      => Mage::helper('slider')->__('Border color of the arrow. (Default: #ffffff)'),
            'value'     => "#ffffff",
            'required'  => true,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('bullets_arrows_hover_border_color');
        $fieldset->addElement($field);



        $form->setValues($model->get(), true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('slider')->__('Bullets');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('slider')->__('Bullets');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('slider/slideshowtemplate/' . $action);
    }
}
