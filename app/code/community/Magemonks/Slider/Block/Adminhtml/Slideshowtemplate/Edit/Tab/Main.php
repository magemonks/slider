<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Block_Adminhtml_Slideshowtemplate_Edit_Tab_Main
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Load Wysiwyg on demand and Prepare layout
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

    protected function _prepareForm()
    {
        /** @var $model Magemonks_Slider_Model_Slideshowtemplate */
        $model = Mage::registry('slider_slideshowtemplate');

        $isElementDisabled = $this->_isAllowedAction('save') ? false : true;

        $form = new Magemonks_Data_Form();
        $form->setHtmlIdPrefix('slideshowtemplate_');

        $fieldset = $form->addFieldset('main_fieldset', array('legend'=>Mage::helper('slider')->__('Main'), 'class' => 'fieldset-wide'));
        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', array(
                'name' => 'id',
            ));
        }
        $fieldset->addField('label', 'text', array(
            'name'      => 'label',
            'label'     => Mage::helper('slider')->__('Label'),
            'title'     => Mage::helper('slider')->__('Label'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The label of the slideshow template. For internal reference only. Not displayed on the front-end'),
            'disabled'  => $isElementDisabled,
        ));

        $fieldset->addField('css_class', 'text', array(
            'name'      => 'css_class',
            'label'     => Mage::helper('slider')->__('CSS Class'),
            'title'     => Mage::helper('slider')->__('CSS Class'),
            'required'  => true,
            'class'     => 'validate-xml-identifier',
            'note'      => Mage::helper('slider')->__('Used to prepend all other css classes. Make sure to update the CSS files accordingly!'),
            'disabled'  => $isElementDisabled,
        ));
        $fieldset->addField('width', 'text', array(
            'name'      => 'width',
            'label'     => Mage::helper('slider')->__('Width (pixels)'),
            'title'     => Mage::helper('slider')->__('Width (pixels)'),
            'required'  => true,
            'value'     => 960,
            'note'      => Mage::helper('slider')->__('Width in pixels. Other screen sizes will be calculated from this (Default: 960). Note: should match the Width defined in CSS.'),
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));
        $fieldset->addField('height', 'text', array(
            'name'      => 'height',
            'label'     => Mage::helper('slider')->__('Height (pixels)'),
            'title'     => Mage::helper('slider')->__('Height (pixels)'),
            'required'  => true,
            'value'     => 480,
            'note'      => Mage::helper('slider')->__('Height in pixels. Other screen sizes will be calculated from this (Default: 480). Note: should match the Height defined in CSS.'),
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));

        $fieldset->addField('full_width', 'select', array(
            'name'      => 'full_width',
            'label'     => Mage::helper('slider')->__('Full width'),
            'title'     => Mage::helper('slider')->__('Full width'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('Full width. (Default: No)'),
            'values'    => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
            'value'     => 0,
            'class'     => 'validate-select',
            'disabled'  => $isElementDisabled,
        ));

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'background_color',
            'label'     => Mage::helper('slider')->__('Background color'),
            'title'     => Mage::helper('slider')->__('Background color'),
            'note'      => Mage::helper('slider')->__('Background color of the slideshow (visible when loading and during some animations). Leave empty for transparent. (Default: empty)'),
            'required'  => false,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('background_color');
        $fieldset->addElement($field);

        $form->setValues($model->get(), true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('slider')->__('Main');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('slider')->__('Main');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('slider/slideshowtemplate/' . $action);
    }
}
