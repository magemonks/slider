<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Block_Adminhtml_Slideshowtemplate_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('sliderSlideshowtemplateGrid');
        $this->setDefaultSort('slideshowtemplate_identifier');
        $this->setDefaultDir('ASC');
    }

    /**
     * Prepare collection
     *
     * @return this
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('slider/slideshowtemplate')->getCollection();
        /* @var $collection Magemonks_Slider_Model_Resource_Slideshowtemplate_Collection */
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Prepare columns
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('label', array(
            'header'    => Mage::helper('slider')->__('Label'),
            'align'     => 'left',
            'index'     => 'label',
        ));

        $this->addColumn('creation_time', array(
            'header'    => Mage::helper('slider')->__('Date Created'),
            'index'     => 'creation_time',
            'type'      => 'datetime',
        ));

        $this->addColumn('update_time', array(
            'header'    => Mage::helper('slider')->__('Last Modified'),
            'index'     => 'update_time',
            'type'      => 'datetime',
        ));

        return parent::_prepareColumns();
    }

    /**
     * After load collection
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        return parent::_afterLoadCollection();
    }

    /**
     * Get the row URL
     *
     * @param $slideshowtemplate
     * @return string
     */
    public function getRowUrl($slideshowtemplate)
    {
        return $this->getUrl('*/slider_slideshowtemplate/edit', array('id' => $slideshowtemplate->getId()));
    }

}
