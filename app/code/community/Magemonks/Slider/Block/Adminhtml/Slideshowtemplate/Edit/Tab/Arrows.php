<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Block_Adminhtml_Slideshowtemplate_Edit_Tab_Arrows
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Load Wysiwyg on demand and Prepare layout
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

    protected function _prepareForm()
    {
        /** @var $model Magemonks_Slider_Model_Slideshowtemplate */
        $model = Mage::registry('slider_slideshowtemplate');

        $isElementDisabled = $this->_isAllowedAction('save') ? false : true;

        $form = new Magemonks_Data_Form();
        $form->setHtmlIdPrefix('slideshowtemplate_');

        /*
         *  ARROWS
         */
        $fieldset = $form->addFieldset('arrows_fieldset', array('legend'=>Mage::helper('slider')->__('Arrows'), 'class' => 'fieldset-wide'));
        $fieldset->addField('arrows_show', 'select', array(
            'name'      => 'arrows_show',
            'label'     => Mage::helper('slider')->__('Show arrows'),
            'title'     => Mage::helper('slider')->__('Show arrows'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('Show navigation arrows. (Default: On)'),
            'values'    => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
            'value'     => 1,
            'class'     => 'validate-select',
        ));
        $fieldset->addField('arrows_shape', 'select', array(
            'name'      => 'arrows_shape',
            'label'     => Mage::helper('slider')->__('Arrows shape'),
            'title'     => Mage::helper('slider')->__('Arrows shape'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('Arrows shape. Note: rounded arrows might not work in every browser, they will be shown as square arrows automatically.'),
            'value'     => 'round',
            'values'    => Mage::getModel('slider/source_arrowsshape')->toOptionArray(false),
        ));
        $fieldset->addField('arrows_position', 'select', array(
            'name'      => 'arrows_position',
            'label'     => Mage::helper('slider')->__('Arrows position'),
            'title'     => Mage::helper('slider')->__('Arrows position'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The position of the arrows. (Default: middle)'),
            'value'     => 'middle',
            'values'    => Mage::getModel('slider/source_arrowsposition')->toOptionArray(false),
            'class'     => 'validate-select',
        ));
        $fieldset->addField('arrows_offset_horizontal', 'text', array(
            'name'      => 'arrows_offset_horizontal',
            'label'     => Mage::helper('slider')->__('Arrows horizontal offset (pixels)'),
            'title'     => Mage::helper('slider')->__('Arrows horizontal offset (pixels)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The horizontal offset of the arrows. Can be a negative number. (Default: 20)'),
            'value'     => 20,
            'class'     => 'validate-number'
        ));
        $fieldset->addField('arrows_offset_vertical', 'text', array(
            'name'      => 'arrows_offset_vertical',
            'label'     => Mage::helper('slider')->__('Arrows vertical offset (pixels)'),
            'title'     => Mage::helper('slider')->__('Arrows vertical offset (pixels)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The vertical offset of the arrows. Can be a negative number. (Default: 0)'),
            'value'     => 0,
            'value'     => 0,
            'class'     => 'validate-number'
        ));


        /*
        *  ARROWS STYLE
        */
        $fieldset = $form->addFieldset('arrows_style_fieldset', array('legend'=>Mage::helper('slider')->__('Arrows style'), 'class' => 'fieldset-wide'));

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'arrows_background_color',
            'label'     => Mage::helper('slider')->__('Background color'),
            'title'     => Mage::helper('slider')->__('Background color'),
            'note'      => Mage::helper('slider')->__('Background color of the arrow. Leave empty for transparent. (Default: empty)'),
            'required'  => false,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('arrows_background_color');
        $fieldset->addElement($field);

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'arrows_arrow_color',
            'label'     => Mage::helper('slider')->__('Arrow color'),
            'title'     => Mage::helper('slider')->__('Arrow color'),
            'note'      => Mage::helper('slider')->__('Collor of the arrow. (Default: #ffffff)'),
            'value'     => "#ffffff",
            'required'  => true,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('arrows_arrow_color');
        $fieldset->addElement($field);

        $fieldset->addField('arrows_opacity', 'text', array(
            'name'      => 'arrows_opacity',
            'label'     => Mage::helper('slider')->__('Opacity (0-1)'),
            'title'     => Mage::helper('slider')->__('Opacity (0-1)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The arrows opacity. (Default: 1)'),
            'value'     => 1,
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));

        $fieldset->addField('arrows_border_size', 'text', array(
            'name'      => 'arrows_border_size',
            'label'     => Mage::helper('slider')->__('Border size (pixels)'),
            'title'     => Mage::helper('slider')->__('Border size (pixels)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The arrows border size. (Default: 3)'),
            'value'     => 3,
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'arrows_border_color',
            'label'     => Mage::helper('slider')->__('Border color'),
            'title'     => Mage::helper('slider')->__('Border color'),
            'note'      => Mage::helper('slider')->__('Border color of the arrow. (Default: #ffffff)'),
            'value'     => "#ffffff",
            'required'  => true,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('arrows_border_color');
        $fieldset->addElement($field);

        $fieldset->addField('arrows_shadow_size', 'text', array(
            'name'      => 'arrows_shadow_size',
            'label'     => Mage::helper('slider')->__('Shadow size (pixels)'),
            'title'     => Mage::helper('slider')->__('Shadow size (pixels)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The arrows shadow size. (Default: 2)'),
            'value'     => 2,
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));

        $fieldset->addField('arrows_shadow_opacity', 'text', array(
            'name'      => 'arrows_shadow_opacity',
            'label'     => Mage::helper('slider')->__('Shadow opacity (0-1)'),
            'title'     => Mage::helper('slider')->__('Shadow opacity (0-1)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The arrows shadow opacity. (Default: 0.5)'),
            'value'     => 0.5,
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));



        /*
        *  ARROWS HOVER STYLE
        */
        $fieldset = $form->addFieldset('arrows_hover_style_fieldset', array('legend'=>Mage::helper('slider')->__('Arrows hover style'), 'class' => 'fieldset-wide'));
        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'arrows_hover_background_color',
            'label'     => Mage::helper('slider')->__('Background color'),
            'title'     => Mage::helper('slider')->__('Background color'),
            'note'      => Mage::helper('slider')->__('Background color of the arrow. Leave empty for transparent. (Default: empty)'),
            'required'  => false,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('arrows_hover_background_color');
        $fieldset->addElement($field);

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'arrows_hover_arrow_color',
            'label'     => Mage::helper('slider')->__('Arrow color'),
            'title'     => Mage::helper('slider')->__('Arrow color'),
            'note'      => Mage::helper('slider')->__('Color of the arrow. (Default: #ffffff)'),
            'value'     => "#ffffff",
            'required'  => true,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('arrows_hover_indicator_color');
        $fieldset->addElement($field);

        $fieldset->addField('arrows_hover_opacity', 'text', array(
            'name'      => 'arrows_hover_opacity',
            'label'     => Mage::helper('slider')->__('Opacity (0-1)'),
            'title'     => Mage::helper('slider')->__('Opacity (0-1)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The arrows opacity. (Default: 1)'),
            'value'     => 1,
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'arrows_hover_border_color',
            'label'     => Mage::helper('slider')->__('Border color'),
            'title'     => Mage::helper('slider')->__('Border color'),
            'note'      => Mage::helper('slider')->__('Border color of the arrow. (Default: #ffffff)'),
            'value'     => "#ffffff",
            'required'  => true,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('arrows_hover_border_color');
        $fieldset->addElement($field);        
        

        $form->setValues($model->get(), true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('slider')->__('Arrows');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('slider')->__('Arrows');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('slider/slideshowtemplate/' . $action);
    }
}
