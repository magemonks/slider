<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Block_Adminhtml_Slideshowtemplate_Edit_Tab_Transitions
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    protected function _prepareForm()
    {
        /** @var $model Magemonks_Slider_Model_Slideshowtemplate */
        $model = Mage::registry('slider_slideshowtemplate');

        $isElementDisabled = $this->_isAllowedAction('save') ? false : true;

        $form = new Magemonks_Data_Form();
        $form->setHtmlIdPrefix('slideshowtemplate_');

        $fieldset = $form->addFieldset('transitions_fieldset', array('legend'=>Mage::helper('slider')->__('Transitions'), 'class' => 'fieldset-wide'));
        $fieldset->addField('delay', 'text', array(
            'name'      => 'delay',
            'label'     => Mage::helper('slider')->__('Delay (miliseconds)'),
            'title'     => Mage::helper('slider')->__('Delay (miliseconds)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The time one slide stays on the screen. Can be overidden per slide. (Default: 10000)'),
            'value'     => 10000,
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));
        $fieldset->addField('transition_style', 'select', array(
            'name'      => 'transition_style',
            'label'     => Mage::helper('slider')->__('Transition style'),
            'title'     => Mage::helper('slider')->__('Transition style'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('Defualt transition style. Can be overidden per slide.'),
            'value'     => 'fade',
            'values'    => Mage::getModel('slider/source_transitionstyle')->toOptionArray(false),
            'disabled'  => $isElementDisabled,
        ));
        $fieldset->addField('transition_duration', 'text', array(
            'name'      => 'transition_duration',
            'label'     => Mage::helper('slider')->__('Transition duration (miliseconds)'),
            'title'     => Mage::helper('slider')->__('Transition duration (miliseconds)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('Defualt transition duration. Can be overidden per slide. (Default: 300)'),
            'value'     => 300,
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));
        $fieldset->addField('transition_slots', 'text', array(
            'name'      => 'transition_slots',
            'label'     => Mage::helper('slider')->__('Transition slots'),
            'title'     => Mage::helper('slider')->__('Transition slots'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('Defualt amount of Slots (used by certain transitions). Values over 8 might have a performance impact. Can be overidden per slide. (Default: 6)'),
            'value'     => 6,
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));

        $form->setValues($model->get(), true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('slider')->__('Transitions');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('slider')->__('Transitions');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('slider/slideshowtemplate/' . $action);
    }
}
