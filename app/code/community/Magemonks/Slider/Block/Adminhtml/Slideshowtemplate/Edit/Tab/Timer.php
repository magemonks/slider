<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Block_Adminhtml_Slideshowtemplate_Edit_Tab_Timer
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    protected function _prepareForm()
    {
        /** @var $model Magemonks_Slider_Model_Slideshowtemplate */
        $model = Mage::registry('slider_slideshowtemplate');

        $isElementDisabled = $this->_isAllowedAction('save') ? false : true;

        $form = new Magemonks_Data_Form();
        $form->setHtmlIdPrefix('slideshowtemplate_');

        $fieldset = $form->addFieldset('timer_fieldset', array('legend'=>Mage::helper('slider')->__('Timer'), 'class' => 'fieldset-wide'));

        $fieldset->addField('timer_enable', 'select', array(
            'name'      => 'timer_enable',
            'label'     => Mage::helper('slider')->__('Enable timer'),
            'title'     => Mage::helper('slider')->__('Enable timer'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('Enable the timer. (Default: Yes)'),
            'values'    => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(false),
            'value'     => 1,
            'disabled'  => $isElementDisabled,
        ));
        $fieldset->addField('timer_show', 'select', array(
            'name'      => 'timer_show',
            'label'     => Mage::helper('slider')->__('Show timer'),
            'title'     => Mage::helper('slider')->__('Show timer'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('Show the time indicator. (Default: Yes)'),
            'values'    => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(false),
            'value'     => 1,
            'disabled'  => $isElementDisabled,
        ));

        $fieldset->addField('timer_position', 'select', array(
            'name'      => 'timer_position',
            'label'     => Mage::helper('slider')->__('Timer position'),
            'title'     => Mage::helper('slider')->__('Timer position'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('Position of the timer.'),
            'values'    => Mage::getModel('slider/source_topbottom')->toOptionArray(false),
            'class'     => 'validate-select',
            'disabled'  => $isElementDisabled,
        ));

        $fieldset->addField('timer_size', 'text', array(
            'name'      => 'timer_size',
            'label'     => Mage::helper('slider')->__('Timer size (pixels)'),
            'title'     => Mage::helper('slider')->__('Timer size (pixels)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The size (height) of the timer (pixels). (Default: 5)'),
            'value'     => 5,
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));
        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'timer_color',
            'label'     => Mage::helper('slider')->__('Timer color'),
            'title'     => Mage::helper('slider')->__('Timer color'),
            'note'      => Mage::helper('slider')->__('Color of the timer (visible when loading). Leave empty for transparent. (Default: #ffffff)'),
            'value'     => '#ffffff',
            'required'  => true,
            'disabled'  => $isElementDisabled,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('timer_color');
        $fieldset->addElement($field);

        $fieldset->addField('timer_opacity', 'text', array(
            'name'      => 'timer_opacity',
            'label'     => Mage::helper('slider')->__('Timer opacity (value 0 - 1)'),
            'title'     => Mage::helper('slider')->__('Timer opacity (value 0 - 1)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The opacity of the timer (pixels). (Default: 0.8)'),
            'value'     => 0.8,
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));

        $form->setValues($model->get(), true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('slider')->__('Timer');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('slider')->__('Timer');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('slider/slideshowtemplate/' . $action);
    }
}
