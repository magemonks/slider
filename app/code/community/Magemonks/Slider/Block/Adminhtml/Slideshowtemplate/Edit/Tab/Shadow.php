<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Block_Adminhtml_Slideshowtemplate_Edit_Tab_Shadow
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    protected function _prepareForm()
    {
        /** @var $model Magemonks_Slider_Model_Slideshowtemplate */
        $model = Mage::registry('slider_slideshowtemplate');

        $isElementDisabled = $this->_isAllowedAction('save') ? false : true;

        $form = new Magemonks_Data_Form();
        $form->setHtmlIdPrefix('slideshowtemplate_');

        $fieldset = $form->addFieldset('shadow_fieldset', array('legend'=>Mage::helper('slider')->__('Shadow'), 'class' => 'fieldset-wide'));
        $fieldset->addField('shadow_show', 'select', array(
            'name'      => 'shadow_show',
            'label'     => Mage::helper('slider')->__('Show shadow'),
            'title'     => Mage::helper('slider')->__('Show shadow'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('Show a shadow. (Default: No)'),
            'values'    => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(false),
            'value'     => 0,
            'disabled'  => $isElementDisabled,
        ));
        $fieldset->addField('shadow_style', 'select', array(
            'name'      => 'shadow_style',
            'label'     => Mage::helper('slider')->__('Shadow style'),
            'title'     => Mage::helper('slider')->__('Shadow style'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The style of the shadow. (Default: Subtle)'),
            'values'    => Mage::getModel('slider/source_shadowstyle')->toOptionArray(false),
            'value'     => 'subtle',
            'disabled'  => $isElementDisabled,
        ));

        $form->setValues($model->get(), true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('slider')->__('Shadow');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('slider')->__('Shadow');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('slider/slideshowtemplate/' . $action);
    }
}
