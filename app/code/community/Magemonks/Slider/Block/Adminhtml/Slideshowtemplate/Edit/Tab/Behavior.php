<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Block_Adminhtml_Slideshowtemplate_Edit_Tab_Behavior
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    protected function _prepareForm()
    {
        /** @var $model Magemonks_Slider_Model_Slideshowtemplate */
        $model = Mage::registry('slider_slideshowtemplate');

        $isElementDisabled = $this->_isAllowedAction('save') ? false : true;

        $form = new Magemonks_Data_Form();
        $form->setHtmlIdPrefix('slideshowtemplate_');

        $fieldset = $form->addFieldset('behavior_fieldset', array('legend'=>Mage::helper('slider')->__('Behavior'), 'class' => 'fieldset-wide'));
        $fieldset->addField('touch_enabled', 'select', array(
            'name'      => 'touch_enabled',
            'label'     => Mage::helper('slider')->__('Touch enabled'),
            'title'     => Mage::helper('slider')->__('Touch enabled'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('Enable Swipe Function on touch devices. (Default: Yes)'),
            'values'    => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(false),
            'value'     => 1,
            'class'     => 'validate-select',
            'disabled'  => $isElementDisabled,
        ));
        $fieldset->addField('on_hover_stop', 'select', array(
            'name'      => 'on_hover_stop',
            'label'     => Mage::helper('slider')->__('On Hover Stop'),
            'title'     => Mage::helper('slider')->__('On Hover Stop'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('Stop the timer when hovering the slider. (Default: Yes)'),
            'values'    => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
            'value'     => 1,
            'class'     => 'validate-select',
            'disabled'  => $isElementDisabled,
        ));

        $fieldset->addField('navigation_toggle', 'select', array(
            'name'      => 'navigation_toggle',
            'label'     => Mage::helper('slider')->__('Navigation toggle'),
            'title'     => Mage::helper('slider')->__('Navigation toggle'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('Automatically hide and show the navigation (bullets and arrows). (Default: On)'),
            'values'    => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
            'value'     => 1,
            'class'     => 'validate-select',
            'disabled'  => $isElementDisabled,
        ));
        $fieldset->addField('navigation_toggle_delay', 'text', array(
            'name'      => 'navigation_toggle_delay',
            'label'     => Mage::helper('slider')->__('Navigation toggle delay (miliseconds)'),
            'title'     => Mage::helper('slider')->__('Navigation toggle delay (miliseconds)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('Navigation toggle delay. (Default: 200)'),
            'value'     => 200,
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));

        $fieldset->addField('full_width_center_images', 'select', array(
            'name'      => 'full_width_center_images',
            'label'     => Mage::helper('slider')->__('Center images (full-width)'),
            'title'     => Mage::helper('slider')->__('Center images (full-width)'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('Center the images in full-width mode. (Default: Yes)'),
            'values'    => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
            'value'     => 1,
            'class'     => 'validate-select',
            'disabled'  => $isElementDisabled,
        ));
        $fieldset->addField('responsive', 'select', array(
            'name'      => 'responsive',
            'label'     => Mage::helper('slider')->__('Responsive'),
            'title'     => Mage::helper('slider')->__('Responsive'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('Make the slider responsive. (Default: Yes)'),
            'values'    => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
            'value'     => 1,
            'class'     => 'validate-select',
            'disabled'  => $isElementDisabled,
        ));
        $fieldset->addField('stop_after_loops', 'text', array(
            'name'      => 'stop_after_loops',
            'label'     => Mage::helper('slider')->__('Stop after x loops.'),
            'title'     => Mage::helper('slider')->__('Stop after x loops.'),
            'required'  => false,
            'note'      => Mage::helper('slider')->__('Stop after x loops. Will stop at selected slide Number (only if "Stop at slide x"). Leave blank to loop continuously. (Default: empty)'),
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));
        $fieldset->addField('stop_at_slide', 'text', array(
            'name'      => 'stop_at_slide',
            'label'     => Mage::helper('slider')->__('Stop at slide x'),
            'title'     => Mage::helper('slider')->__('Stop at slide x'),
            'required'  => false,
            'note'      => Mage::helper('slider')->__('Stop at slide x (only if "Stop after x loops" is set). (Default: empty)'),
            'class'     => 'validate-not-negative-number',
            'disabled'  => $isElementDisabled,
        ));

        $form->setValues($model->get(), true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('slider')->__('Behavior');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('slider')->__('Behavior');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('slider/slideshowtemplate/' . $action);
    }
}
