<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Block_Adminhtml_Slide_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_slide';
        $this->_blockGroup = 'slider';

        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('slider')->__('Save Slide'));
        $this->_updateButton('delete', 'label', Mage::helper('slider')->__('Delete Slide'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";

        if (Mage::registry('slider_slide')->getId()) {
            $this->_addButton('edititems', array(
                'label'     => Mage::helper('adminhtml')->__('Edit Captions'),
                'onclick'   => 'setLocation(\'' . $this->getUrl('*/slider_caption', array('slide_id' => Mage::registry('slider_slide')->getId())) . '\')',
                'class'     => 'go',
            ), -100, 100);
        }

    }

    public function getBackUrl()
    {
        return $this->getUrl('*/slider_slide/', array('slideshow_id' => Mage::registry('slider_slideshow')->getId()));
    }

    public function getDeleteUrl()
    {
        return $this->getUrl('*/*/delete', array('id' => Mage::registry('slider_slide')->getId(), 'slideshow_id' => Mage::registry('slider_slideshow')->getId() ));
    }

    /**
     * Get edit form container header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('slider_slide')->getId()) {
            return Mage::helper('cms')->__("Edit Slide '%s'", $this->escapeHtml(Mage::registry('slider_slide')->get('label')));
        }
        else {
            return Mage::helper('cms')->__('New Slide');
        }
    }

    /**
     * Get the form action url
     *
     * @return string
     */
    public function getFormActionUrl(){
        return $this->getUrl('*/*/save', array('id' => Mage::registry('slider_slide')->getId()));
    }
}
