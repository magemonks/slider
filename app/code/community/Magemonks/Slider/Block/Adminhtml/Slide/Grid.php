<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Block_Adminhtml_Slide_Grid extends Magemonks_Core_Block_Adminhtml_Widget_Grid_Sortable
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('sliderSlideGrid');
        $this->setDefaultSort('position');
        $this->setDefaultDir('ASC');
    }

    /**
     * Prepare collection
     *
     * @return this
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('slider/slide')->getCollection()
            ->addFieldToFilter('slideshow_id', Mage::registry('slider_slideshow')->getId());

        /* @var $collection Mage_Cms_Model_Mysql4_Block_Collection */
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Prepare columns
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('label', array(
            'header'    => Mage::helper('slider')->__('Label'),
            'align'     => 'left',
            'index'     => 'label',
        ));

        $this->addColumn('is_active', array(
            'header'    => Mage::helper('slider')->__('Status'),
            'index'     => 'is_active',
            'type'      => 'options',
            'options'   => array(
                0 => Mage::helper('slider')->__('Disabled'),
                1 => Mage::helper('slider')->__('Enabled')
            ),
        ));

        $this->addColumn('creation_time', array(
            'header'    => Mage::helper('slider')->__('Date Created'),
            'index'     => 'creation_time',
            'type'      => 'datetime',
        ));

        $this->addColumn('update_time', array(
            'header'    => Mage::helper('slider')->__('Last Modified'),
            'index'     => 'update_time',
            'type'      => 'datetime',
        ));

        $this->addColumn('edit_captions', array(
            'header'    => Mage::helper('slider')->__('Captions'),
            'width'     => 10,
            'sortable'  => false,
            'filter'    => false,
            'renderer'  => 'slider/adminhtml_slide_grid_render_action',
        ));

        return parent::_prepareColumns();
    }

    /**
     * After load collection
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        return parent::_afterLoadCollection();
    }

    /**
     * Filter on store
     *
     * @param $collection
     * @param $column
     * @return mixed
     */
    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }

        $this->getCollection()->addStoreFilter($value);
    }

    /**
     * Get the row URL
     *
     * @param $slide
     * @return string
     */
    public function getRowUrl($slide)
    {
        $slideshow = Mage::registry('slider_slideshow');

        return $this->getUrl('adminhtml/slider_slide/edit', array('id' => $slide->getId(), 'slideshow_id' => $slideshow->getId()));
    }

}
