<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Block_Adminhtml_Slide_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Init form
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('slide_form');
        $this->setTitle(Mage::helper('slider')->__('Slide Information'));
    }

    /**
     * Load Wysiwyg on demand and Prepare layout
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

    protected function _prepareForm()
    {
        $slideshow = Mage::registry('slider_slideshow');
        $model = Mage::registry('slider_slide');

        $form = new Magemonks_Data_Form(
            array('id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post')
        );

        $form->setHtmlIdPrefix('slide_');

        $fieldset = $form->addFieldset('general_fieldset', array('legend'=>Mage::helper('slider')->__('General'), 'class' => 'fieldset-wide'));

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', array(
                'name' => 'id',
            ));
        }
        $fieldset->addField('slideshow_id', 'hidden', array(
            'name' => 'slideshow_id',
            'value' => $slideshow->getId()
        ));

        $fieldset->addField('label', 'text', array(
            'name'      => 'label',
            'label'     => Mage::helper('slider')->__('Slide Label'),
            'title'     => Mage::helper('slider')->__('Slide Label'),
            'required'  => true,
            'note'      => Mage::helper('slider')->__('The label of the slide. For internal reference only. Not displayed on the front-end'),
        ));

        $fieldset->addField('is_active', 'select', array(
            'label'     => Mage::helper('slider')->__('Status'),
            'title'     => Mage::helper('slider')->__('Status'),
            'name'      => 'is_active',
            'required'  => true,
            'options'   => array(
                '1' => Mage::helper('slider')->__('Enabled'),
                '0' => Mage::helper('slider')->__('Disabled'),
            ),
        ));
        if (!$model->getId()) {
            $model->set('is_active', '1');
        }

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Mediapicker(array(
            'name'                  => 'image',
            'label'                 => Mage::helper('slider')->__('Image'),
            'title'                 => Mage::helper('slider')->__('Image'),
            'required'              => false,
            'note'                  => Mage::helper('slider')->__('Slide image.'),
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('image');
        $fieldset->addElement($field);

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Mediapicker(array(
            'name'                  => 'thumbnail',
            'label'                 => Mage::helper('slider')->__('Thumbnail'),
            'title'                 => Mage::helper('slider')->__('Thumbnail'),
            'required'              => false,
            'note'                  => Mage::helper('slider')->__('Slide thumbnail.'),
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('thumbnail');
        $fieldset->addElement($field);

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
            'name'      => 'background_color',
            'label'     => Mage::helper('slider')->__('Background color'),
            'title'     => Mage::helper('slider')->__('Background color'),
            'note'      => Mage::helper('slider')->__('Slide background-color'),
            'required'  => false,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('background_color');
        $fieldset->addElement($field);


        $fieldset = $form->addFieldset('transitions_fieldset', array('legend'=>Mage::helper('slider')->__('Transitions'), 'class' => 'fieldset-wide'));

        $fieldset->addField('delay', 'text', array(
            'name'      => 'delay',
            'label'     => Mage::helper('slider')->__('Delay'),
            'title'     => Mage::helper('slider')->__('Delay'),
            'required'  => false,
            'note'      => Mage::helper('slider')->__('The time one slide stays on the screen (leave blank to use slideshow settings).'),
            'class'     => 'validate-not-negative-number'
        ));

        $fieldset->addField('transition_style', 'select', array(
            'name'      => 'transition_style',
            'label'     => Mage::helper('slider')->__('Transition style'),
            'title'     => Mage::helper('slider')->__('Transition style'),
            'required'  => false,
            'note'      => Mage::helper('slider')->__('Transition style (leave blank to use slideshow settings).'),
            'values'    => Mage::getModel('slider/source_transitionstyle')->toOptionArray(false),
        ));

        $fieldset->addField('transition_duration', 'text', array(
            'name'      => 'transition_duration',
            'label'     => Mage::helper('slider')->__('Transition speed'),
            'title'     => Mage::helper('slider')->__('Transition speed'),
            'required'  => false,
            'note'      => Mage::helper('slider')->__('Transition duration (leave blank to use slideshow settings).'),
            'class'     => 'validate-not-negative-number'
        ));

        $fieldset->addField('transition_slots', 'text', array(
            'name'      => 'transition_slots',
            'label'     => Mage::helper('slider')->__('Transition slots'),
            'title'     => Mage::helper('slider')->__('Transition of slots'),
            'required'  => false,
            'note'      => Mage::helper('slider')->__('amount of Slots (used by certain transitions) (leave blank to use slideshow settings). Values over 8 might have a performance impact.'),
            'class'     => 'validate-not-negative-number'
        ));

        $fieldset = $form->addFieldset('link_fieldset', array('legend'=>Mage::helper('slider')->__('Link'), 'class' => 'fieldset-wide'));

        $fieldset->addField('link', 'text', array(
            'name'      => 'link',
            'label'     => Mage::helper('slider')->__('Link'),
            'title'     => Mage::helper('slider')->__('Link'),
            'required'  => false,
            'note'      => Mage::helper('slider')->__('Link (when clicking the slide)'),
        ));
        $fieldset->addField('link_target', 'text', array(
            'name'      => 'link_target',
            'label'     => Mage::helper('slider')->__('Link target'),
            'title'     => Mage::helper('slider')->__('Link target'),
            'required'  => false,
            'note'      => Mage::helper('slider')->__('Target for the link (leave empty for same window, _blank for new window'),
        ));

        $form->setValues($model->get(), true);
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}
