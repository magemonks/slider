<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Block_Adminhtml_Slide_Grid_Render_Action extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $button = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(array(
            'id'        => "editCaptions",
            'onclick'   => "setLocation('".$this->getUrl('*/slider_caption', array('slide_id' => $row->getId()))."')",
            'label'     => Mage::helper('slider')->__('Edit Captions'),
            'class' => 'go'
        ));

        return '<a href="'.$this->getUrl('*/slider_caption', array('slide_id' => $row->getId())).'">'.$button->toHtml().'</a>';
    }
}
