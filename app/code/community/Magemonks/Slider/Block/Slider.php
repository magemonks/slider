<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Block_Slider extends Mage_Core_Block_Template {

    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('magemonks/slider/slider.phtml');

        $this->addData(array(
            'cache_lifetime'=> false,
            'cache_tags'    => array(
                Magemonks_Slider_Model_Slideshow::CACHE_TAG,
                Mage_Core_Model_Store::CACHE_TAG,
                Mage_Core_Model_Store_Group::CACHE_TAG,
            )
        ));
    }

    /**
     * Get cache key informative items
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        return array(
            'SLIDER',
            Mage::app()->getStore()->getId(),
            (int)Mage::app()->getStore()->isCurrentlySecure(),
            Mage::getDesign()->getPackageName(),
            Mage::getDesign()->getTheme('template'),
            $this->getSlideshow()->getData('identifier'),
        );
    }

    /**
     * Sets the menu identifier to load
     *
     * @param $identifier
     * @return Magemonks_Slider_Block_Slider
     */
    public function setIdentifer($identifier)
    {
        $this->setData('identifier', $identifier);
        return $this;
    }


    /**
     * Retrieve Menu instance
     *
     * @return Magemonks_Slider_Model_Slideshow
     */
    public function getSlideshow()
    {
        if (!$this->hasData('slideshow')) {
            $identifier = $this->getData('identifier', null);
            if ($identifier) {
                $slideshow = Mage::getModel('slider/slideshow')
                    ->setStoreId(Mage::app()->getStore()->getId())
                    ->load($identifier);
                if($slideshow->getData('is_active') == true){
                    $this->setData('slideshow', $slideshow);
                }
            }
        }
        return $this->getData('slideshow');
    }


    public function getSlideOpeningTag($slide)
    {
        $data = array();
        if($slide->getCombinedData('transition_style') != null){
            $data[] = "data-transition='".$slide->getCombinedData('transition_style')."'";
        }
        if($slide->getCombinedData('transition_slots') != null){
            $data[] = "data-slotamount='".$slide->getCombinedData('transition_slots')."'";
        }
        if($slide->getCombinedData('transition_duration') != null){
            $data[] = "data-masterspeed='".$slide->getCombinedData('transition_duration')."'";
        }
        if($slide->getCombinedData('delay') != null){
            $data[] = "data-delay='".$slide->getCombinedData( 'delay')."'";
        }
        if($slide->getCombinedData('link') != null){
            $data[] = "data-link='".$slide->getCombinedData('link')."'";
        }
        if($slide->getCombinedData('link_target') != null){
            $data[] = "data-target='".$slide->getCombinedData('link_target')."'";
        }

        return sprintf('<li %1$s>', implode(' ', $data));
    }

    public function getSlideClosingTag($slide)
    {
        return '</li>';
    }

    public function getCaptionHtml($caption)
    {
        $data = array();
        $data[] = "data-start='".$caption->getCombinedData('start')."'";
        $data[] = "data-x='".$caption->getCombinedData('position_x')."'";
        $data[] = "data-y='".$caption->getCombinedData('position_y')."'";
        $data[] = "data-speed='".$caption->getCombinedData('animation_duration')."'";
        $data[] = "data-easing='".$caption->getCombinedData('animation_easing')."'";
        $link = $caption->get('link');
        if(!empty($link)){
            $data[] = "data-link='".$link."'";
            $data[] = "data-target='".$caption->get('link_target')."'";
        }

        $classes = array();
        $classes[] = 'slider-caption-'.$caption->getCaptionTemplate()->get('css_class');
        $classes[] = 'slider-caption-template-'.$caption->getCaptionTemplate()->getId();
        $classes[] = 'slider-caption-caption-'.$caption->getId();
        $classes[] = 'slider-caption-animation-'.$caption->getCombinedData('animation_style');

        $content = $caption->get('content');

        return sprintf('<div class="slider-caption %1$s" %2$s>%3$s</div>', implode(' ', $classes), implode(' ', $data), $content);
    }

    public function getImageTag($slide)
    {
        $image = $slide->get('image');
        if(empty($image)){
            $coloredImageUrl = Mage::helper('magemonks/coloredimage')->getSolidColorImageUrl($slide->get('background_color'));

            if(empty($coloredImageUrl)) return '';
            $image = $coloredImageUrl;
        }

        return sprintf('<img src="%1$s" alt="Slider Image">', $image);
    }

    public function getSlideshowData($key)
    {
        return $this->getSlideShow()->getCombinedData($key);
    }
}