<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Model_Slide extends Magemonks_Core_Model_Configurationfields_Abstract
{
    const CACHE_TAG              = 'slider';
    protected $_cacheTag         = 'slider';

    protected $_slideshow;

    /**
     * Config fields saved (serialized) in de DB
     *
     * @var array
     */
    public $configFields = array('image', 'background_color', 'delay', 'transition_style', 'transition_duration', 'transition_slots', 'link', 'link_target');

    public function _construct()
    {
        parent::_construct();
        $this->_init('slider/slide');
    }

    public function getSlideshow()
    {
        if (is_null($this->_slideshow)) {
            $identifier = $this->getData('slideshow_id', null);
            if ($identifier) {
                $slideshow = Mage::getModel('slider/slideshow')
                    ->load($identifier);
                if($slideshow->getId()){
                    $this->_slideshow = $slideshow;
                }
            }
        }
        return $this->_slideshow;
    }

    public function setSlideShow(Magemonks_Slider_Model_Slideshow $slideshow)
    {
        $this->_slideshow = $slideshow;
    }

    public function getCaptions()
    {
        return $this->getResource()->getCaptions($this);
    }

    /**
     * Add a caption
     *
     * @param Magemonks_Slider_Model_Caption $caption
     * @return Magemonks_Slider_Model_Slide
     */
    public function addCaption(Magemonks_Slider_Model_Caption $caption)
    {
        $this->getResource()->addCaption($this, $caption);

        return $this;
    }



    /**
     * Get Combined data (slide + slideshow + slideshowdata)
     *
     * @param string $key
     * @param null $index
     * @return mixed
     */
    public function getCombinedData($key='', $index=null)
    {
        $slideData = $this->get($key, $index);
        if(!is_null($slideData)){
            return $slideData;
        }

        $slideshowData = $this->getSlideshow()->getCombinedData($key, $index);
        if(!is_null($slideshowData)){
            return $slideshowData;
        }

        return null;
    }

}