<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Model_Caption extends Magemonks_Core_Model_Configurationfields_Abstract
{
    const CACHE_TAG              = 'slider';
    protected $_cacheTag         = 'slider';

    protected $_slide;
    protected $_captionTemplate;

    /**
     * Config fields saved (serialized) in de DB
     *
     * @var array
     */
    public $configFields = array(
        'content',
        'link',
        'link_target',
        'text_color',
        'background_color',
        'border_color',
        'text_hover_color',
        'background_hover_color',
        'border_hover_color',
        'animation_style',
        'animation_easing',
        'animation_duration',
    );

    public function _construct()
    {
        parent::_construct();
        $this->_init('slider/caption');
    }

    public function getSlide()
    {
        if (is_null($this->slide)) {
            $identifier = $this->getData('slide_id', null);
            if ($identifier) {
                $slide = Mage::getModel('slider/slide')
                    ->load($identifier);
                if($slide->getId()){
                    $this->slide = $slide;
                }
            }
        }
        return $this->slide;
    }

    public function setSlide(Magemonks_Slider_Model_Slideshow $slide)
    {
        $this->set('slide_id', $slide->getId());
        $this->_slide = $slide;

        return $this;
    }

    public function getCaptionTemplate()
    {
        if (is_null($this->template)) {
            $identifier = $this->getData('captiontemplate_id', null);
            if ($identifier) {
                $captionTemplate = Mage::getModel('slider/captiontemplate')
                    ->load($identifier);
                if($captionTemplate->getId()){
                    $this->captionTemplate = $captionTemplate;
                }
            }
        }
        return $this->captionTemplate;
    }

    public function setCaptionTemplate(Magemonks_Slider_Model_Captiontemplate $captionTemplate)
    {
        $this->set('captiontemplate_id', $captionTemplate->getId());
        $this->_captionTempmlate = $captionTemplate;

        return $this;
    }


    /**
     * Get Combined data (caption + captiontemplate)
     *
     * @param string $key
     * @param null $index
     * @return mixed
     */
    public function getCombinedData($key='', $index=null)
    {
        $captionData = $this->get($key, $index);
        if(!is_null($captionData)){
            return $captionData;
        }

        $captionTemplateData = $this->getCaptionTemplate()->get($key, $index);
        if(!is_null($captionTemplateData)){
            return $captionTemplateData;
        }

        return null;
    }
}