<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Model_Resource_Caption_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Define resource model
     *
     */
    protected function _construct()
    {
        $this->_init('slider/caption');
        $this->_map['fields']['slide'] = 'slide_table.id';
    }

    /**
     * Returns pairs id - label
     *
     * @return array
     */
    public function toOptionArray()
    {
        return $this->_toOptionArray('id', 'label');
    }

    /**
     * Add filter by store
     *
     * @param int|Magemonks_Slider_Model_Slideshow $slideshow
     * @return Magemonks_Slider_Model_Resource_Slideshow_Collection
     */
    public function addSlideFilter($slide)
    {
        if ($slide instanceof Magemonks_Slider_Model_Slide) {
            $slide = $slide->getId();
        }

        $this->addFilter('slide', $slide);

        return $this;
    }

    /**
     * Get SQL for get record count.
     * Extra GROUP BY strip added.
     *
     * @return Varien_Db_Select
     */
    public function getSelectCountSql()
    {
        $countSelect = parent::getSelectCountSql();

        $countSelect->reset(Zend_Db_Select::GROUP);

        return $countSelect;
    }

    /**
     * Join store relation table if there is store filter
     */
    protected function _renderFiltersBefore()
    {
        if ($this->getFilter('slide')) {
            $this->getSelect()->join(
                array('slide_table' => $this->getTable('slider/slide')),
                'main_table.slide_id = slide_table.id',
                array()
            )->group('main_table.slide_id');

            /*
             * Allow analytic functions usage because of one field grouping
             */
            $this->_useAnalyticFunction = true;
        }
        return parent::_renderFiltersBefore();
    }

}
