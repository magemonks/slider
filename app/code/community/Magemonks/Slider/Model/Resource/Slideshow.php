<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Model_Resource_Slideshow extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('slider/slideshow', 'id');
    }


    /**
     * Process slideshow data before deleting
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Cms_Model_Resource_Page
     */
    protected function _beforeDelete(Mage_Core_Model_Abstract $object)
    {
        $condition = array(
            'slideshow_id = ?'     => (int) $object->getId(),
        );

        $this->_getWriteAdapter()->delete($this->getTable('slider/slideshow_store'), $condition);

        return parent::_beforeDelete($object);
    }

    /**
     * Perform operations before object save
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Magemonks_Slider_Model_Resource_Slideshow
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        if (!$this->getIsUniqueSlideshowToStores($object)) {
            Mage::throwException(Mage::helper('slider')->__('A slideshow identifier with the same properties already exists in the selected store.'));
        }

        if (! $object->getId()) {
            $object->setCreationTime(Mage::getSingleton('core/date')->gmtDate());
            $this->_createRootItem = true;
        }
        $object->setData('update_time', Mage::getSingleton('core/date')->gmtDate());
        return $this;
    }

    /**
     * Perform operations after object load
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Magemonks_Slider_Model_Resource_Slideshow
     */
    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
        if ($object->getId()) {
            $stores = $this->lookupStoreIds($object->getId());
            $object->setData('store_id', $stores);
        }
        return parent::_afterLoad($object);
    }

    /**
     * Perform operations after object save
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Magemonks_Slider_Model_Resource_Slideshow
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {

        /* Set the stores */

        $oldStores = $this->lookupStoreIds($object->getId());
        $newStores = (array)$object->getStores();

        $table  = $this->getTable('slider/slideshow_store');
        $insert = array_diff($newStores, $oldStores);
        $delete = array_diff($oldStores, $newStores);

        if ($delete) {
            $where = array(
                'slideshow_id = ?'     => (int) $object->getId(),
                'store_id IN (?)' => $delete
            );

            $this->_getWriteAdapter()->delete($table, $where);
        }

        if ($insert) {
            $data = array();

            foreach ($insert as $storeId) {
                $data[] = array(
                    'slideshow_id'  => (int) $object->getId(),
                    'store_id' => (int) $storeId
                );
            }

            $this->_getWriteAdapter()->insertMultiple($table, $data);
        }
        return parent::_afterSave($object);

    }

    /**
     * Load an object using 'identifier' field if there's no field specified and value is not numeric
     *
     * @param Mage_Core_Model_Abstract $object
     * @param mixed $value
     * @param string $field
     * @return Magemonks_Slider_Model_Resource_Slideshow
     */
    public function load(Mage_Core_Model_Abstract $object, $value, $field = null)
    {
        if (!is_numeric($value) && is_null($field)) {
            $field = 'identifier';
        }

        return parent::load($object, $value, $field);
    }


    /**
     * Get the visible slides
     *
     * @param Magemonks_Slider_Model_Slideshow $object
     * @return mixed
     */
    public function getVisibleSlides(Magemonks_Slider_Model_Slideshow $object)
    {
        return $this->getSlides($object, true);
    }


    /**
     * Get the slides
     *
     * @param Magemonks_Slider_Model_Slideshow $object
     * @param null $active
     * @return mixed
     */
    public function getSlides(Magemonks_Slider_Model_Slideshow $object, $active = null)
    {
        $slides = Mage::getModel('slider/slide')->getCollection()
            ->addFieldToFilter('slideshow_id', $object->getId());

        if(!is_null($active)){
            $slides->addFieldToFilter('is_active', (int) $active);
        }

        $slides->addOrder('position', Varien_Data_Collection::SORT_ORDER_ASC);

        return $slides;
    }


    /**
     * @param Magemonks_Slider_Model_Slideshow $object
     * @param Magemonks_Slider_Model_Slide $slide
     * @param null $position
     * @return Magemonks_Slider_Model_Resource_Slideshow
     */
    public function addSlide(Magemonks_Slider_Model_Slideshow $object, Magemonks_Slider_Model_Slide $slide, $position = null)
    {
        $siblings = $this->getSlides($object);

        if($position == null){
            $position = $slide->getData('position');
        }

        if($position == null || $position > count($siblings)){
            $position = count($siblings);
        }

        $slide->setData('position', $position);
        $slide->setData('slideshow_id', $object->getId());
        $slide->save();
        return $this;
    }


    /**
     * Retrieve select object for load object data
     *
     * @param string $field
     * @param mixed $value
     * @param Magemonks_Slider_Model_Slideshow $object
     * @return Zend_Db_Select
     */
    protected function _getLoadSelect($field, $value, $object)
    {
        $select = parent::_getLoadSelect($field, $value, $object);

        if ($object->getStoreId()) {
            $stores = array(
                (int) $object->getStoreId(),
                Mage_Core_Model_App::ADMIN_STORE_ID,
            );

            $select->join(
                array('sss' => $this->getTable('slider/slideshow_store')),
                $this->getMainTable().'.id = sss.slideshow_id',
                array('store_id')
            )->where('is_active = ?', 1)
                ->where('sss.store_id in (?) ', $stores)
                ->order('store_id DESC')
                ->limit(1);
        }

        return $select;
    }

    /**
     * Check for unique of identifier of slideshow to selected store(s).
     *
     * @param Mage_Core_Model_Abstract $object
     * @return bool
     */
    public function getIsUniqueSlideshowToStores(Mage_Core_Model_Abstract $object)
    {
        if (Mage::app()->isSingleStoreMode()) {
            $stores = array(Mage_Core_Model_App::ADMIN_STORE_ID);
        } else {
            $stores = (array)$object->getData('stores');
        }

        $select = $this->_getReadAdapter()->select()
            ->from(array('ss' => $this->getMainTable()))
            ->join(
            array('sss' => $this->getTable('slider/slideshow_store')),
            'ss.id = sss.slideshow_id',
            array()
        )->where('ss.identifier = ?', $object->getData('identifier'))
            ->where('sss.store_id IN (?)', $stores);

        if ($object->getId()) {
            $select->where('ss.id <> ?', $object->getId());
        }

        if ($this->_getReadAdapter()->fetchRow($select)) {
            return false;
        }

        return true;
    }

    /**
     * Get store ids to which specified item is assigned
     *
     * @param int $id
     * @return array
     */
    public function lookupStoreIds($id)
    {
        $adapter = $this->_getReadAdapter();

        $select  = $adapter->select()
            ->from($this->getTable('slider/slideshow_store'), 'store_id')
            ->where('slideshow_id = :slideshow_id');

        $binds = array(
            ':slideshow_id' => (int) $id
        );

        return $adapter->fetchCol($select, $binds);
    }
}