<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Model_Resource_Slide extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('slider/slide', 'id');
    }

    /**
     * Perform operations before object save
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Magemonks_Slider_Model_Resource_Slideshow
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        if (! $object->getId()) {
            $object->setCreationTime(Mage::getSingleton('core/date')->gmtDate());
            $this->_createRootItem = true;
        }
        $object->setData('update_time', Mage::getSingleton('core/date')->gmtDate());
        $this->_processImage($object, 'image')->_processImage($object, 'thumbnail');
        return $this;
    }


    /**
     * Perform operations before object delete
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Magemonks_Slider_Model_Resource_Slideshow
     */
    protected function _beforeDelete(Mage_Core_Model_Abstract $object)
    {
        //operations to maintain tree structure
        if($object->getId()){
            $connection  = $this->_getWriteAdapter();
            $connection->beginTransaction();
            $connection->query("update ".$this->getMainTable()." set position = position-1 where slideshow_id = ? and position >= ?", array($object->getData('slideshow_id'), $object->getData('position')));
            $connection->commit();
        }

        return $this;
    }

    /**
     * Process the image
     *
     * @param Magemonks_Slider_Model_Slide $object
     * @param $field
     * @return Magemonks_Slider_Model_Resource_Slide
     */
    protected function _processImage(Magemonks_Slider_Model_Slide $object, $field)
    {
        $image = $object->get($field);
        if(!empty($image)){
            if(strpos($image,'/admin/cms_wysiwyg/directive/___directive/') !== false){
                $parts = explode('/',parse_url($image, PHP_URL_PATH));
                $key = array_search('___directive', $parts);
                if($key !== false){
                    $directive = $parts[$key+1];
                    $src = Mage::getModel('core/email_template_filter')->filter(Mage::helper('core')->urlDecode($directive));
                    if(!empty($src)){
                        $object->set($field, parse_url($src, PHP_URL_PATH));
                    }
                }
            }
        }
        return $this;
    }


    /**
     * Get the visible slides
     *
     * @param Magemonks_Slider_Model_Slide $object
     * @return mixed
     */
    public function getVisibleCaptions(Magemonks_Slider_Model_Slide $object)
    {
        return $this->getCaptions($object, true);
    }


    /**
     * Get the slides
     *
     * @param Magemonks_Slider_Model_Slide $object
     * @param null $active
     * @return mixed
     */
    public function getCaptions(Magemonks_Slider_Model_Slide $object, $active = null)
    {
        $captions = Mage::getModel('slider/caption')->getCollection()
            ->addFieldToFilter('slide_id', $object->getId());

        if(!is_null($active)){
            $captions->addFieldToFilter('is_active', (int) $active);
        }

        return $captions;
    }


    /**
     * @param Magemonks_Slider_Model_Slide $object
     * @param Magemonks_Slider_Model_Caption $caption
     * @return Magemonks_Slider_Model_Resource_Slide
     */
    public function addCaption(Magemonks_Slider_Model_Slide $object, Magemonks_Slider_Model_Caption $caption)
    {
        $caption->setData('slide_id', $object->getId());
        $caption->save();
        return $this;
    }

}