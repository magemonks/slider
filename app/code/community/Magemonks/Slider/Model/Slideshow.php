<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Model_Slideshow extends Magemonks_Core_Model_Configurationfields_Abstract
{
    const CACHE_TAG              = 'slider';
    protected $_cacheTag         = 'slider';

    protected $_slideshowTemplate;

    public function _construct()
    {
        parent::_construct();
        $this->_init('slider/slideshow');
    }

    public function getSlideshowTemplate()
    {
        if (is_null($this->_slideshowTemplate)) {
            $identifier = $this->getData('slideshowtemplate_id', null);
            if ($identifier) {
                $slideshowTemplate = Mage::getModel('slider/slideshowtemplate')
                    ->load($identifier);
                if($slideshowTemplate->getId()){
                    $this->_slideshowTemplate = $slideshowTemplate;
                }
            }
        }
        return $this->_slideshowTemplate;
    }

    public function setSlideShowTemplate(Magemonks_Slider_Model_Slideshowtemplate $slideshowTemplate)
    {
        $this->slideshowTemplate = $slideshowTemplate;
    }


    /**
     * Get the visible slides
     * @return mixed
     */
    public function getVisibleSlides()
    {
        return $this->getResource()->getVisibleSlides($this);
    }


    /**
     * Add a slide
     *
     * @param Magemonks_Slider_Model_Slide $slide
     * @param null $position
     * @return Magemonks_Slider_Model_Slideshow
     */
    public function addSlide(Magemonks_Slider_Model_Slide $slide, $position = null)
    {
        $this->getResource()->addSlide($this, $slide, $position);

        return $this;
    }


    /**
     * Get Combined data (slideshow + slideshowdata)
     *
     * @param string $key
     * @param null $index
     * @return mixed
     */
    public function getCombinedData($key='', $index=null)
    {
        $slideshowData = $this->get($key, $index);

        if(!is_null($slideshowData)){
            return $slideshowData;
        }

        $slideshowTemplateData = $this->getSlideshowTemplate()->get($key, $index);
        if(!is_null($slideshowTemplateData)){
            return $slideshowTemplateData;
        }

        return null;
    }

}