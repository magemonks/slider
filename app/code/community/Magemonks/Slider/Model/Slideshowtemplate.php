<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Model_Slideshowtemplate extends Magemonks_Core_Model_Configurationfields_Abstract
{
    const CACHE_TAG              = 'slider';
    protected $_cacheTag         = 'slider';

    /**
     * Config fields saved (serialized) in de DB
     *
     * @var array
     */
    public $configFields = array(
        'css_class',
        'width',
        'height',
        'full_width',
        'full_width_center_images',
        'background_color',
        'touch_enabled',
        'on_hover_stop',
        'stop_after_loops',
        'stop_at_slide',
        'responsive',
        'delay',
        'transition_style',
        'transition_duration',
        'transition_slots',
        'loader_show',
        'loader_transition_duration',
        'init_transition_style',
        'init_transition_duration',
        'init_transition_slots',
        'timer_enable',
        'timer_show',
        'timer_position',
        'timer_size',
        'timer_color',
        'timer_opacity',
        'navigation_toggle',
        'navigation_toggle_delay',
        'bullets_show',
        'bullets_shape',
        'bullets_horizontal_space',
        'bullets_indicators_show',
        'bullets_arrows_show',
        'bullets_position',
        'bullets_offset_horizontal',
        'bullets_offset_vertical',
        'bullets_background_color',
        'bullets_indicator_color',
        'bullets_opacity',
        'bullets_border_size',
        'bullets_border_color',
        'bullets_shadow_size',
        'bullets_shadow_opacity',
        'bullets_selected_background_color',
        'bullets_selected_indicator_color',
        'bullets_selected_opacity',
        'bullets_selected_border_color',
        'bullets_hover_background_color',
        'bullets_hover_indicator_color',
        'bullets_hover_opacity',
        'bullets_hover_border_color',
        'bullets_arrows_background_color',
        'bullets_arrows_arrow_color',
        'bullets_arrows_opacity',
        'bullets_arrows_border_size',
        'bullets_arrows_border_color',
        'bullets_arrows_shadow_size',
        'bullets_arrows_shadow_opacity',
        'bullets_arrows_hover_background_color',
        'bullets_arrows_hover_arrow_color',
        'bullets_arrows_hover_opacity',
        'bullets_arrows_hover_border_color',
        'arrows_show',
        'arrows_shape',
        'arrows_position',
        'arrows_offset_horizontal',
        'arrows_offset_vertical',
        'arrows_background_color',
        'arrows_arrow_color',
        'arrows_opacity',
        'arrows_border_size',
        'arrows_border_color',
        'arrows_shadow_size',
        'arrows_shadow_opacity',
        'arrows_hover_background_color',
        'arrows_hover_arrow_color',
        'arrows_hover_opacity',
        'arrows_hover_border_color',
        'shadow_show',
        'shadow_style'
    );

    public function _construct()
    {
        parent::_construct();
        $this->_init('slider/slideshowtemplate');
    }

}