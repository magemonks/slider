<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Model_Source_Transitionstyle
{
    public function toOptionArray($isMultiselect=false)
    {
        $values = array();
        if(!$isMultiselect){
            $values[] = array('value' => '', 'label' => Mage::helper('slider')->__('--Please Select--'));
        }

        $values[] = array('value' => 'fade',                        'label' => Mage::helper('slider')->__('Fade'));
        $values[] = array('value' => 'boxfade',                     'label' => Mage::helper('slider')->__('Boxfade'));
        $values[] = array('value' => 'slotfade-horizontal',         'label' => Mage::helper('slider')->__('Slotfade - Horizontal'));
        $values[] = array('value' => 'slotfade-vertical',           'label' => Mage::helper('slider')->__('Slotfade - Vertical'));

        $values[] = array('value' => 'slideleft',                   'label' => Mage::helper('slider')->__('Slide Left'));
        $values[] = array('value' => 'slideright',                  'label' => Mage::helper('slider')->__('Slide Right'));
        $values[] = array('value' => 'slideup',                     'label' => Mage::helper('slider')->__('Slide Up'));
        $values[] = array('value' => 'slidedown',                   'label' => Mage::helper('slider')->__('Slide Down'));
        $values[] = array('value' => 'boxslide',                    'label' => Mage::helper('slider')->__('Boxslide'));
        $values[] = array('value' => 'slotslide-horizontal',        'label' => Mage::helper('slider')->__('Slotslide - Horizontal'));
        $values[] = array('value' => 'slotslide-vertical',          'label' => Mage::helper('slider')->__('Slotslide - Vertical'));


        $values[] = array('value' => 'slotzoom-horizontal',         'label' => Mage::helper('slider')->__('Slotzoom - Horizontal'));
        $values[] = array('value' => 'slotzoom-vertical',           'label' => Mage::helper('slider')->__('Slotzoom - Vertical'));

        $values[] = array('value' => 'curtain-1',                   'label' => Mage::helper('slider')->__('Curtain 1'));
        $values[] = array('value' => 'curtain-2',                   'label' => Mage::helper('slider')->__('Curtain 2'));
        $values[] = array('value' => 'curtain-3',                   'label' => Mage::helper('slider')->__('Curtain 3'));

        $values[] = array('value' => 'papercut',                    'label' => Mage::helper('slider')->__('Papercut'));
        $values[] = array('value' => 'flyin',                       'label' => Mage::helper('slider')->__('Flyin'));
        $values[] = array('value' => 'turnoff',                     'label' => Mage::helper('slider')->__('Turn off'));
        $values[] = array('value' => 'cubic',                       'label' => Mage::helper('slider')->__('Cubic'));
        $values[] = array('value' => '3dcurtain-vertical',          'label' => Mage::helper('slider')->__('3D Curtain - Vertical'));
        $values[] = array('value' => '3dcurtain-horizontal',        'label' => Mage::helper('slider')->__('3D Curtain - Horizontal'));

        $values[] = array('value' => 'random',                      'label' => Mage::helper('slider')->__('Random'));

        return $values;
    }
}