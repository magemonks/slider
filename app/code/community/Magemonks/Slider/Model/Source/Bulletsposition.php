<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Model_Source_Bulletsposition
{

    public function toOptionArray($isMultiselect=false)
    {
        $values = array();
        if(!$isMultiselect){
            $values[] = array('value' => '', 'label' => Mage::helper('slider')->__('--Please Select--'));
        }

        $values[] = array('value' => 'top', 'label' => Mage::helper('slider')->__('Top'));
        $values[] = array('value' => 'top-right', 'label' => Mage::helper('slider')->__('Top-Right'));
        $values[] = array('value' => 'right', 'label' => Mage::helper('slider')->__('Right'));
        $values[] = array('value' => 'bottom-right', 'label' => Mage::helper('slider')->__('Bottom-Right'));
        $values[] = array('value' => 'bottom', 'label' => Mage::helper('slider')->__('Bottom'));
        $values[] = array('value' => 'bottom-left', 'label' => Mage::helper('slider')->__('Bottom-Left'));
        $values[] = array('value' => 'left', 'label' => Mage::helper('slider')->__('Left'));
        $values[] = array('value' => 'top-left', 'label' => Mage::helper('slider')->__('Top-Left'));

        return $values;
    }
}