<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Model_Source_Bulletsshape
{

    public function toOptionArray($isMultiselect=false)
    {
        $values = array();
        if(!$isMultiselect){
            $values[] = array('value' => '', 'label' => Mage::helper('slider')->__('--Please Select--'));
        }

        $values[] = array('value' => 'round', 'label' => Mage::helper('slider')->__('Round'));
        $values[] = array('value' => 'square', 'label' => Mage::helper('slider')->__('Square'));

        return $values;
    }
}