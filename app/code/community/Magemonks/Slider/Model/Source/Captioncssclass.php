<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Model_Source_Captioncssclass
{
    const CSS_FILE_LOCATION = 'magemonks/slider/css/captions.css';

    public function toOptionArray($isMultiselect=false)
    {
        $values = array();
        if(!$isMultiselect){
            $values[] = array('value' => '', 'label' => Mage::helper('slider')->__('--Please Select--'));
        }

        $classes = array();
        $stores = Mage::getModel('core/store')->getCollection();
        $numStores = 0;
        foreach($stores as $store){
            $numStores++;
            $design = Mage::getModel('core/design_package')
                        ->setArea(Mage_Core_Model_Design_Package::DEFAULT_AREA)
                        ->setStore($store);
            $file = $design->getFilename(self::CSS_FILE_LOCATION, array('_type' => 'skin'));

            $css = file_get_contents($file);
            $storeClasses = array();
            $pattern = '#\.slider \.slider-caption-(\S+)#';
            preg_match_all($pattern, $css, $matches, PREG_SET_ORDER);
            if(is_array($matches)){
                foreach($matches as $match){
                    if(is_array($match) && isset($match[1])){
                        $storeClasses[] = $match[1];
                    }
                }
            }
            $storeClasses = array_unique($storeClasses);

            foreach($storeClasses as $storeClass){
                $packageSkin = substr($file, 0, strpos($file, self::CSS_FILE_LOCATION)-1);
                $packageSkin = explode(DS, $packageSkin);

                $package = $packageSkin[count($packageSkin)-2];
                $skin = $packageSkin[count($packageSkin)-1];

                $packageSkin = $package.DS.$skin;

                if(array_key_exists($storeClass, $classes)){
                    $classes[$storeClass][] = $packageSkin;
                }
                else{
                    $classes[$storeClass] = array($packageSkin);
                }

            }
        }

        foreach($classes as $classname => $packageSkins){
            $packageSkins = array_unique($packageSkins);
            $values[] = array('label' => $classname . ' (' . implode(', ', $packageSkins) . ')', 'value'=> $classname);
        }



        return $values;
    }
}