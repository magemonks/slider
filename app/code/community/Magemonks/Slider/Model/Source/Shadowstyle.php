<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Model_Source_Shadowstyle
{
    public function toOptionArray($isMultiselect=false)
    {
        $values = array();
        if(!$isMultiselect){
            $values[] = array('value' => '', 'label' => Mage::helper('slider')->__('--Please Select--'));
        }

        $values[] = array('value' => 'subtle', 'label' => Mage::helper('slider')->__('Subtle'));
        $values[] = array('value' => 'block', 'label' => Mage::helper('slider')->__('Block'));
        $values[] = array('value' => 'arch', 'label' => Mage::helper('slider')->__('Arch'));

        return $values;
    }
}