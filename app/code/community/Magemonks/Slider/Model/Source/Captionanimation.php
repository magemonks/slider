<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Model_Source_Captionanimation
{
    public function toOptionArray($isMultiselect=false)
    {
        $values = array();
        if(!$isMultiselect){
            $values[] = array('value' => '', 'label' => Mage::helper('slider')->__('--Please Select--'));
        }
        $values[] = array('value' => 'fade', 'label' => Mage::helper('slider')->__('Fade'));
        $values[] = array('value' => 'sft', 'label' => Mage::helper('slider')->__('Short from Top'));
        $values[] = array('value' => 'sfb', 'label' => Mage::helper('slider')->__('Short from Bottom'));
        $values[] = array('value' => 'sfr', 'label' => Mage::helper('slider')->__('Short from Right'));
        $values[] = array('value' => 'sfl', 'label' => Mage::helper('slider')->__('Short from left'));
        $values[] = array('value' => 'lft', 'label' => Mage::helper('slider')->__('Long from Top'));
        $values[] = array('value' => 'lfb', 'label' => Mage::helper('slider')->__('Long from Bottom'));
        $values[] = array('value' => 'lfr', 'label' => Mage::helper('slider')->__('Long from Right'));
        $values[] = array('value' => 'lfl', 'label' => Mage::helper('slider')->__('Long from Left'));
        $values[] = array('value' => 'randomrotate', 'label' => Mage::helper('slider')->__('Rotate and fade randomly'));

        return $values;
    }
}