<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Model_Source_Easing
{
    public function toOptionArray($isMultiselect=false)
    {
        $values = array();
        if(!$isMultiselect){
            $values[] = array('value' => '', 'label' => Mage::helper('slider')->__('--Please Select--'));
        }

        $values[] = array('value' => 'easeInBack', 'label' => Mage::helper('slider')->__('easeInBack'));
        $values[] = array('value' => 'easeInBounce', 'label' => Mage::helper('slider')->__('easeInBounce'));
        $values[] = array('value' => 'easeInCirc', 'label' => Mage::helper('slider')->__('easeInCirc'));
        $values[] = array('value' => 'easeInCubic', 'label' => Mage::helper('slider')->__('easeInCubic'));
        $values[] = array('value' => 'easeInElastic', 'label' => Mage::helper('slider')->__('easeInElastic'));
        $values[] = array('value' => 'easeInExpo', 'label' => Mage::helper('slider')->__('easeInExpo'));
        $values[] = array('value' => 'easeInQuad', 'label' => Mage::helper('slider')->__('easeInQuad'));
        $values[] = array('value' => 'easeInQuart', 'label' => Mage::helper('slider')->__('easeInQuart'));
        $values[] = array('value' => 'easeInQuint', 'label' => Mage::helper('slider')->__('easeInQuint'));
        $values[] = array('value' => 'easeInSine', 'label' => Mage::helper('slider')->__('easeInSine'));

        $values[] = array('value' => 'easeOutBack', 'label' => Mage::helper('slider')->__('easeOutBack'));
        $values[] = array('value' => 'easeOutBounce', 'label' => Mage::helper('slider')->__('easeOutBounce'));
        $values[] = array('value' => 'easeOutCirc', 'label' => Mage::helper('slider')->__('easeOutCirc'));
        $values[] = array('value' => 'easeOutCubic', 'label' => Mage::helper('slider')->__('easeOutCubic'));
        $values[] = array('value' => 'easeOutElastic', 'label' => Mage::helper('slider')->__('easeOutElastic'));
        $values[] = array('value' => 'easeOutExpo', 'label' => Mage::helper('slider')->__('easeOutExpo'));
        $values[] = array('value' => 'easeOutQuad', 'label' => Mage::helper('slider')->__('easeOutQuad'));
        $values[] = array('value' => 'easeOutQuart', 'label' => Mage::helper('slider')->__('easeOutQuart'));
        $values[] = array('value' => 'easeOutQuint', 'label' => Mage::helper('slider')->__('easeOutQuint'));
        $values[] = array('value' => 'easeOutSine', 'label' => Mage::helper('slider')->__('easeOutSine'));

        $values[] = array('value' => 'easeInOutBack', 'label' => Mage::helper('slider')->__('easeInOutBack'));
        $values[] = array('value' => 'easeInOutBounce', 'label' => Mage::helper('slider')->__('easeInOutBounce'));
        $values[] = array('value' => 'easeInOutCirc', 'label' => Mage::helper('slider')->__('easeInOutCirc'));
        $values[] = array('value' => 'easeInOutCubic', 'label' => Mage::helper('slider')->__('easeInOutCubic'));
        $values[] = array('value' => 'easeInOutElastic', 'label' => Mage::helper('slider')->__('easeInOutElastic'));
        $values[] = array('value' => 'easeInOutExpo', 'label' => Mage::helper('slider')->__('easeInOutExpo'));
        $values[] = array('value' => 'easeInOutQuad', 'label' => Mage::helper('slider')->__('easeInOutQuad'));
        $values[] = array('value' => 'easeInOutQuart', 'label' => Mage::helper('slider')->__('easeInOutQuart'));
        $values[] = array('value' => 'easeInOutQuint', 'label' => Mage::helper('slider')->__('easeInOutQuint'));
        $values[] = array('value' => 'easeInOutSine', 'label' => Mage::helper('slider')->__('easeInOutSine'));

        return $values;
    }
}