<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Slider_Model_Captiontemplate extends Magemonks_Core_Model_Configurationfields_Abstract
{
    const CACHE_TAG              = 'slider';
    protected $_cacheTag         = 'slider';

    /**
     * Config fields saved (serialized) in de DB
     *
     * @var array
     */
    public $configFields = array(
        'start',
        'position_x',
        'position_y',
        'css_class',
        'css_custom',
        'css_hover_custom',
        'text_color',
        'background_color',
        'border_color',
        'text_hover_color',
        'background_hover_color',
        'border_hover_color',
        'animation_style',
        'animation_easing',
        'animation_duration',
    );

    public function _construct()
    {
        parent::_construct();
        $this->_init('slider/captiontemplate');
    }

}