<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Slider
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */


/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * Create table 'slider/slideshowtemplate'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('slider/slideshowtemplate'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array('identity' => true,'nullable' => false,'primary' => true))
    ->addColumn('creation_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array())
    ->addColumn('update_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array())
    ->addColumn('label', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array('nullable' => false ))
    ->addColumn('configuration', Varien_Db_Ddl_Table::TYPE_TEXT, null, array('nullable' => true ))
    ->setComment('Magemonks slider slideshowtemplate table');
$installer->getConnection()->createTable($table);

/**
 * Create table 'slider/captiontemplate'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('slider/captiontemplate'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array('identity' => true,'nullable' => false,'primary' => true))
    ->addColumn('creation_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array())
    ->addColumn('update_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array())
    ->addColumn('label', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array('nullable' => false ))
    ->addColumn('configuration', Varien_Db_Ddl_Table::TYPE_TEXT, null, array('nullable' => true ))
    ->setComment('Magemonks slider captiontemplate table');
$installer->getConnection()->createTable($table);


/**
 * Create table 'slider/slideshow'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('slider/slideshow'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array('identity' => true, 'nullable' => false, 'primary' => true))
    ->addColumn('slideshowtemplate_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array('nullable' => false))
    ->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array('nullable' => false, 'default' => '1'))
    ->addColumn('creation_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array())
    ->addColumn('update_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array())
    ->addColumn('identifier', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array('nullable' => false))
    ->addColumn('label', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array('nullable' => false))
    ->addIndex(
        $installer->getIdxName('slider/slideshow', array('slideshowtemplate_id')),
        array('slideshowtemplate_id')
    )
    ->addForeignKey(
        $installer->getFkName('slider/slideshow', 'slideshowtemplate_id', 'slider/slideshowtemplate', 'id'),
        'slideshowtemplate_id',
        $installer->getTable('slider/slideshowtemplate'),
        'id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('Magemonks slider slideshow table');
$installer->getConnection()->createTable($table);

/**
 * Create table 'slider/slideshow_store (many to many table)'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('slider/slideshow_store'))
    ->addColumn('slideshow_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array('nullable' => false, 'primary' => true), 'Slideshow ID')
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array('unsigned' => true, 'nullable' => false, 'primary' => true ), 'Store ID')
    ->addIndex(
        $installer->getIdxName('slider/slideshow_store', array('store_id')),
        array('store_id')
    )
    ->addForeignKey(
        $installer->getFkName('slider/slideshow_store', 'slideshow_id', 'slider/slideshow', 'id'),
        'slideshow_id',
        $installer->getTable('slider/slideshow'),
        'id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName('slider/slideshow_store', 'store_id', 'core/store', 'store_id'),
        'store_id',
        $installer->getTable('core/store'),
        'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Magemonks slider slideshow to store linkage table');
$installer->getConnection()->createTable($table);



/**
 * Create table 'slider/slide'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('slider/slide'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array('identity' => true, 'nullable' => false, 'primary' => true))
    ->addColumn('slideshow_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array('nullable' => false))
    ->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array('nullable' => false, 'default' => '1' ))
    ->addColumn('creation_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array())
    ->addColumn('update_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array())
    ->addColumn('position', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array('nullable' => false))
    ->addColumn('label', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array('nullable' => false ))
    ->addColumn('configuration', Varien_Db_Ddl_Table::TYPE_TEXT, null, array('nullable' => true ))
    ->addForeignKey(
        $installer->getFkName('slider/slide', 'slideshow_id', 'slider/slideshow', 'id'),
        'slideshow_id',
        $installer->getTable('slider/slideshow'),
        'id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('Magemonks slider table');
$installer->getConnection()->createTable($table);


/**
 * Create table 'slider/caption'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('slider/caption'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array('identity' => true, 'nullable' => false, 'primary' => true))
    ->addColumn('slide_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array('nullable' => false))
    ->addColumn('captiontemplate_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array('nullable' => false))
    ->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array('nullable' => false, 'default' => '1' ))
    ->addColumn('creation_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array())
    ->addColumn('update_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array())
    ->addColumn('start', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array('nullable' => true))
    ->addColumn('position_x', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array('nullable' => true))
    ->addColumn('position_y', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array('nullable' => true))
    ->addColumn('label', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array('nullable' => false ))
    ->addColumn('configuration', Varien_Db_Ddl_Table::TYPE_TEXT, null, array('nullable' => true ))
    ->addForeignKey(
        $installer->getFkName('slider/caption', 'slide_id', 'slider/slide', 'id'),
        'slide_id',
        $installer->getTable('slider/slide'),
        'id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName('slider/caption', 'captiontemplate_id', 'slider/captiontemplate', 'id'),
        'captiontemplate_id',
        $installer->getTable('slider/captiontemplate'),
        'id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('Magemonks caption table');
$installer->getConnection()->createTable($table);



$installer->endSetup();