(function($,undefined){
    $.fn.extend({
        mmslider: function(options) {
            var defaults = {
                delay: 9000,
                startheight: 960,
                startwidth: 480,
                fullWidth: 0,

                navigationBullets: 1,
                navigationBulletsPosition: "bottom",    //top, top-right, right, botto-mright, bottom, bottom-left, left, top-left
                navigationBulletsIndicators: 1,
                navigationBulletsArrows: 1,
                navigationBulletsOffsetHorizontal: 0,
                navigationBulletsOffsetVertical: 20,

                navigationArrows: 1,
                navigationArrowsPosition: "middle",    //top, middle, bottom
                navigationArrowsOffsetHorizontal: 20,
                navigationArrowsOffsetVertical: 0,

                navigationToggle: 1,
                navigationToggleDelay: 200,

                touchenabled: 1,						// Enable Swipe Function : on/off
                onHoverStop: 1,						// Stop Banner Timet at Hover on Slide on/off
                stopAtSlide:-1,
                stopAfterLoops:-1,

                shadow:1,
                shadowStyle: 'subtle',

                initTransitionStyle: 'fade',
                initTransitionDuration: 100,
                initTransitionSlots: 3,
                loaderShow: 1,
                loaderTransitionSpeed: 500,

                timerShow: 1,
                timerEnable: 1,
                timerPosition: 'top'
            };

            options = $.extend(defaults, options);

            return this.each(function() {

                var opt=options;
                var container=$(this);

                // Delegate .transition() calls to .animate()
                // if the browser can't do CSS transitions.
                if (!$.support.transition)
                    $.fn.transition = $.fn.animate;

                $.cssEase['bounce'] = 'cubic-bezier(0,1,0.5,1.3)';

                // LOAD THE YOUTUBE API
                container.find('.slider-caption iframe').each(function() {
                    try {
                        if ($(this).attr('src').indexOf('you')>0) {
                            var s = document.createElement("script");
                            s.src = "http://www.youtube.com/player_api"; /* Load Player API*/
                            var before = document.getElementsByTagName("script")[0];
                            before.parentNode.insertBefore(s, before);
                        }
                    } catch(e) {}
                });

                // LOAD THE VIMEO API
                container.find('.slider-caption iframe').each(function() {
                    try{
                        if ($(this).attr('src').indexOf('vim')>0) {

                            var f = document.createElement("script");
                            f.src = "http://a.vimeocdn.com/js/froogaloop2.min.js"; /* Load Player API*/
                            var before = document.getElementsByTagName("script")[0];
                            before.parentNode.insertBefore(f, before);
                        }
                    } catch(e) {}
                });

                // SHUFFLE MODE
                if (opt.shuffle=="on") {
                    for (var u=0;u<container.find('>ul:first-child >li').length;u++) {
                        var it = Math.round(Math.random()*container.find('>ul:first-child >li').length);
                        container.find('>ul:first-child >li:eq('+it+')').prependTo(container.find('>ul:first-child'));
                    }
                }

                // CREATE SOME DEFAULT OPTIONS FOR LATER
                opt.slots=4;
                opt.act=-1;
                opt.next=0;
                opt.origcd = opt.delay;
                opt.stopLoop = 0;
                opt.init = true;
                opt.firefox13 = ($.browser.mozilla)  && (parseInt($.browser.version,0)==13 ||  parseInt($.browser.version,0)==14 ||  parseInt($.browser.version,0)==15 ||  parseInt($.browser.version,0)==16 );
                opt.ie = $.browser.msie && parseInt($.browser.version,0)<9;
                opt.ie9 = $.browser.msie && parseInt($.browser.version,0)==9;
                opt.container = container;

                // SHORTWAY USAGE OF OFFSETS
                opt.navOH = opt.navigationBulletsOffsetHorizontal;
                opt.navOV = opt.navigationBulletsOffsetVertical;

                if(opt.loaderShow == true){
                    container.append('<div class="slider-loader"></div>');
                }


                var bt = $('<div class="slider-timer"></div>');
                if(opt.timerPosition == 'bottom'){
                    bt.addClass('slider-timer-bottom');
                }
                bt.css({'width':'0%'});
                if(opt.timerShow == 0){
                    bt.css('display', 'none');
                }
                bt.appendTo(container);



                // AMOUNT OF THE SLIDES
                opt.slideamount = container.find('>ul:first >li').length;

                // A BASIC GRID MUST BE DEFINED. IF NO DEFAULT GRID EXIST THAN WE NEED A DEFAULT VALUE, ACTUAL SIZE OF CONAINER
                if (container.height()==0) container.height(opt.startheight);
                if (opt.startwidth==undefined || opt.startwidth==0) opt.startwidth=container.width();
                if (opt.startheight==undefined || opt.startheight==0) opt.startheight=container.height();

                // OPT WIDTH && HEIGHT SHOULD BE SET
                opt.width=container.width();
                opt.height=container.height();

                // DEFAULT DEPENDECIES
                opt.bw = opt.startwidth / container.width();
                opt.bh = opt.startheight / container.height();

                // IF THE ITEM ALREADY IN A RESIZED FORM
                if (opt.width != opt.startwidth) {
                    opt.height = Math.round(opt.startheight * (opt.width/opt.startwidth));
                    container.height(opt.height);
                }

                // LETS SEE IF THERE IS ANY SHADOW
                if (opt.shadow == 1 && opt.shadowStyle != '') {
                    var shadow = $('<div>')
                        .addClass('slider-shadow slider-shadow-style-'+opt.shadowStyle)
                        .css({'width':opt.width})
                        .appendTo(container.parent());
                }

                // IF IMAGES HAS BEEN LOADED
                container.waitForImages(function() {
                    // PREPARE THE SLIDES
                    prepareSlides(container,opt);

                    // CREATE BULLETS / ARROWS
                    if (opt.slideamount > 1){
                        if(opt.navigationBullets || opt.navigationArrows){
                            if(opt.navigationBullets){
                                createNavigationBullets(container,opt);
                            }
                            if(opt.navigationArrows){
                                createNavigationArrows(container,opt);
                            }
                            setNavigationPositions(container,opt);

                            if(opt.navigationToggle == 1){
                                toggleNavigation(container,opt);
                            }
                        }
                        swipeAction(container,opt);
                    }

                    function startSlider(){
                       swapSlide(container,opt);
                        // START COUNTDOWN
                       if (opt.slideamount >1 && opt.timerEnable === 1) countDown(container,opt);
                    }

                    container.waitForImages(function() {
                        // START THE FIRST SLIDE
                        if(opt.loaderShow == true){
                            container.find('.slider-loader').fadeOut(opt.loaderTransitionSpeed, startSlider);
                        }
                        else{
                            startSlider();
                        }
                    });
                });

                // WINDOW RESIZE
                $(window).resize(function() {
                    if (container.outerWidth(true) != opt.width) {
                        containerResized(container,opt);
                    }
                });
            })
        },


        // METHODE PAUSE
        revpause: function(options) {
            return this.each(function() {
                var container=$(this);
                container.data('conthover',1);
                container.data('conthover-changed',1);
                container.trigger('revolution.slide.onpause');
                var bt = container.parent().find('.slider-timer');
                bt.stop();
            })
        },

        // METHODE RESUME
        revresume: function(options) {
            return this.each(function() {
                var container=$(this);
                container.data('conthover',0);
                container.data('conthover-changed',1);
                container.trigger('revolution.slide.onresume');
                var bt = container.parent().find('.slider-timer');
                var opt = bt.data('opt');

                bt.animate({'width':"100%"},{duration:((opt.delay-opt.cd)-100),queue:false, easing:"linear"});
            })
        },

        // METHODE NEXT
        revnext: function(options) {
            return this.each(function() {
                // CATCH THE CONTAINER
                var container=$(this);
                container.parent().find('.tp-rightarrow').click();
            })
        },

        // METHODE RESUME
        revprev: function(options) {
            return this.each(function() {
                // CATCH THE CONTAINER
                var container=$(this);
                container.parent().find('.tp-leftarrow').click();
            })
        },

        // METHODE LENGTH
        revmaxslide: function(options) {
            // CATCH THE CONTAINER
            return $(this).find('>ul:first-child >li').length;
        },

        // METHODE RESUME
        revshowslide: function(slide) {
            return this.each(function() {
                // CATCH THE CONTAINER
                var container=$(this);
                container.data('showus',slide);
                container.parent().find('.tp-rightarrow').click();
            })
        }
    })

    //////////////////////////
    //	CONTAINER RESIZED	//
    /////////////////////////
    function containerResized(container,opt) {
        container.find('.defaultimg').each(function(i) {
            setSize($(this),opt);

            opt.height = Math.round(opt.startheight * (opt.width/opt.startwidth));
            container.height(opt.height);

            setSize($(this),opt);

            try{
                container.parent().find('.slider-shadow').css({'width':opt.width});
            } catch(e) {}

            var actsh = container.find('>ul >li:eq('+opt.act+') .slotholder');
            var nextsh = container.find('>ul >li:eq('+opt.next+') .slotholder');
            removeSlots(container,opt);
            nextsh.find('.defaultimg').css({'opacity':0});
            actsh.find('.defaultimg').css({'opacity':1});

            setCaptionPositions(container,opt);

            var nextli = container.find('>ul >li:eq('+opt.next+')');
            container.find('.slider-caption').each(function() { $(this).stop(true,true);});
            animateTheCaptions(nextli, opt);

            restartBannerTimer(opt,container);

        });
    }

    ////////////////////////////////
    //	RESTART THE BANNER TIMER //
    //////////////////////////////
    function restartBannerTimer(opt,container) {
        opt.cd=0;
        if (opt.videoplaying !=true) {
            var bt=	container.find('.slider-timer');
            if (bt.length>0) {
                bt.stop();
                bt.css({'width':'0%'});
                bt.animate({'width':"100%"},{duration:(opt.delay-100),queue:false, easing:"linear"});
            }
            clearTimeout(opt.positionTimer);
            opt.positionTimer = setTimeout(function() {
                setNavigationPositions(container,opt);
            },200);
        }
    }

    function callingNewSlide(opt,container) {
        opt.cd=0;
        swapSlide(container,opt);

        // STOP TIMER AND RESCALE IT
        var bt=	container.find('.slider-timer');
        if (bt.length>0) {
            bt.stop();
            bt.css({'width':'0%'});
            bt.animate({'width':"100%"},{duration:(opt.delay-100),queue:false, easing:"linear"});
        }
    }

    ////////////////////////////////
    //	-	CREATE THE BULLETS -  //
    ////////////////////////////////
    function createNavigationBullets(container,opt) {
        var bullets = $('<div>')
            .addClass('slider-bullets')
            .appendTo(container.parent());

        if(opt.navigationBulletsArrows){
            var bulletArrowLeft = $('<div>')
                .addClass('slider-bullet-arrow slider-bullet-arrow-left')
                .on('click', function(event){
                    arrowClick(container,opt, 'left');
                })
                .appendTo(bullets);
        }

        container.find('>ul:first >li').each(function(index) {
            var bullet = $('<div>')
                .addClass('slider-bullet')
                .on('click', function(event){
                    if (opt.transition==0 && index != opt.act) {
                        opt.next = index;
                        callingNewSlide(opt,container);
                    }
                }).appendTo(bullets);

            if(opt.navigationBulletsIndicators){
                var bulletIndex = $('<span>'+(index+1)+'</span>')
                    .addClass('slider-bullet-index')
                    .appendTo(bullet);

            }
        });


        if(opt.navigationBulletsArrows){
            var bulletArrowRight = $('<div>')
                .addClass('slider-bullet-arrow slider-bullet-arrow-right')
                .on('click', function(event){
                    arrowClick(container,opt, 'right');
                })
                .appendTo(bullets);
        }
    }

    //////////////////////
    //	CREATE ARROWS	//
    /////////////////////
    function createNavigationArrows(container,opt) {
        $('<div>')
            .addClass('slider-arrow slider-arrow-left')
            .on('click', function(event){
                arrowClick(container,opt, 'left');
            })
            .appendTo(container.parent());

        $('<div>')
            .addClass('slider-arrow slider-arrow-right')
            .on('click', function(event){
                arrowClick(container,opt, 'right');
            })
            .appendTo(container.parent());

        setNavigationPositions(container,opt);
    }

    function arrowClick(container, opt, direction){
        if(direction == 'left'){
            if (opt.transition==0) {
                opt.next = opt.next-1;
                opt.leftarrowpressed=1;
                if (opt.next < 0) opt.next=opt.slideamount-1;
                callingNewSlide(opt,container);
            }
        }
        else{ //right
            if (opt.transition == 0) {
                if (container.data('showus') !=undefined && container.data('showus') != -1)
                    opt.next = container.data('showus')-1;
                else
                    opt.next = opt.next+1;
                container.data('showus',-1);
                if (opt.next >= opt.slideamount) opt.next=0;
                if (opt.next<0) opt.next=0;

                if (opt.act !=opt.next) callingNewSlide(opt,container);
            }
        }
    }



    ///////////////////////////////////////////////////////////////////
    // SHOW AND HIDE THE NAVIGATION IF MOUE GOES OUT OF THE BANNER  ///
    ///////////////////////////////////////////////////////////////////
    function toggleNavigation(container,opt) {
        var bullets = container.parent().find('.slider-bullets').hide();
        var arrows = container.parent().find('.slider-arrow').hide();

        bullets.hover(
            function() {
                bullets.addClass("hovered");
                clearTimeout(opt.navigationToggleTimer);
                bullets.fadeIn();
                arrows.fadeIn();
            },
            function() {
                bullets.removeClass("hovered");
                if (!container.hasClass("hovered") && !bullets.hasClass("hovered"))
                    opt.navigationToggleTimer = setTimeout(function() {
                        bullets.fadeOut();
                        arrows.fadeOut();
                    }, opt.navigationToggleDelay);
            }
        );


        arrows.hover(
            function() {
                arrows.addClass("hovered");
                clearTimeout(opt.navigationToggleTimer);
                bullets.fadeIn();
                arrows.fadeIn();
            },
            function() {
                arrows.removeClass("hovered");
                if (!container.hasClass("hovered") && !bullets.hasClass("hovered")){
                    opt.navigationToggleTimer = setTimeout(function() {
                        bullets.fadeOut();
                        arrows.fadeOut();
                    },opt.navigationToggleDelay);
                }
            }
        );

        container.on('mouseenter', function() {
            container.addClass("hovered");
            clearTimeout(opt.navigationToggleTimer);
            bullets.fadeIn();
            arrows.fadeIn();
        });

        container.on('mouseleave', function() {
            container.removeClass("hovered");
            if (!container.hasClass("hovered") && !bullets.hasClass("hovered")){
                opt.navigationToggleTimer = setTimeout(function() {
                    bullets.fadeOut();
                    arrows.fadeOut();
                },opt.navigationToggleDelay);
            }
        });
    }

    //////////////////////////////
    //	SET NAVIGATION POSITIN OF BULLETS	//
    //////////////////////////////
    function setNavigationPositions(container,opt) {
        var containerParent = container.parent();

        if(opt.navigationArrows){
            var arrows = containerParent.find('.slider-arrow');
            var arrowLeft = containerParent.find('.slider-arrow-left');
            var arrowRight = containerParent.find('.slider-arrow-right');

            var arrowWidth = arrowLeft.outerWidth(true);
            var arrowHeight = arrowLeft.outerHeight(true);

            arrowLeft.css({
                marginLeft: opt.navigationArrowsOffsetHorizontal
            });

            arrowRight.css({
                marginRight: opt.navigationArrowsOffsetHorizontal
            });

            switch(opt.navigationArrowsPosition){
                case 'top':
                    arrows.css({
                        top: 0,
                        marginTop: opt.navigationArrowsOffsetVertical
                    });
                    break;
                case 'middle':
                    arrows.css({
                        top: '50%',
                        marginTop: (arrowHeight / -2) + opt.navigationArrowsOffsetVertical
                    });
                    break;
                case 'bottom':
                    arrows.css({
                        bottom: 0,
                        marginBottom: opt.navigationArrowsOffsetVertical
                    });
                    break;
            }
        }

        if(opt.navigationBullets){
            var bulletsContainer = containerParent.find('.slider-bullets');

            var bulletContainerWidth = 0;
            var bulletContainerHeight = 0;
            bulletsContainer.children().each(function(index, bullet){
                bulletContainerWidth += $(bullet).outerWidth(true);
                var bulletHeight = $(bullet).outerHeight(true);
                bulletContainerHeight = bulletHeight > bulletContainerHeight ? bulletHeight : bulletContainerHeight;
            });
            bulletsContainer.width(bulletContainerWidth);
            bulletsContainer.height(bulletContainerHeight);

            switch(opt.navigationBulletsPosition){
                case 'top':
                    bulletsContainer.css({
                        top: 0,
                        left: '50%',
                        marginLeft: (bulletContainerWidth / -2) + opt.navigationBulletsOffsetHorizontal,
                        marginTop: opt.navigationBulletsOffsetVertical
                    });
                    break;
                case 'top-right':
                    bulletsContainer.css({
                        top: 0,
                        right: 0,
                        marginRight: opt.navigationBulletsOffsetHorizontal,
                        marginTop: opt.navigationBulletsOffsetVertical
                    });
                    break;
                case 'right':
                    bulletsContainer.css({
                        top: '50%',
                        right: 0,
                        marginRight: opt.navigationBulletsOffsetHorizontal,
                        marginTop: (bulletContainerHeight / -2) + opt.navigationBulletsOffsetVertical
                    });
                    break;
                case 'bottom-right':
                    bulletsContainer.css({
                        bottom: 0,
                        right: 0,
                        marginRight: opt.navigationBulletsOffsetHorizontal,
                        marginBottom: opt.navigationBulletsOffsetVertical
                    });
                    break;
                case 'bottom':
                    bulletsContainer.css({
                        bottom: 0,
                        left: '50%',
                        marginLeft: (bulletContainerWidth / -2) + opt.navigationBulletsOffsetHorizontal,
                        marginBottom: opt.navigationBulletsOffsetVertical
                    });
                    break;
                case 'bottom-left':
                    bulletsContainer.css({
                        bottom: 0,
                        left: 0,
                        marginLeft: opt.navigationBulletsOffsetHorizontal,
                        marginBottom: opt.navigationBulletsOffsetVertical
                    });
                    break;
                case 'left':
                    bulletsContainer.css({
                        top: '50%',
                        left: 0,
                        marginLeft: opt.navigationBulletsOffsetHorizontal,
                        marginTop: (bulletContainerHeight / -2) + opt.navigationBulletsOffsetVertical
                    });
                    break;
                case 'top-left':
                    bulletsContainer.css({
                        top: 0,
                        left: 0,
                        marginLeft: opt.navigationBulletsOffsetHorizontal,
                        marginTop: opt.navigationBulletsOffsetVertical
                    });
                    break;
            }
        }

    }

    ////////////////////////////
    // SET THE SWIPE FUNCTION //
    ////////////////////////////
    function swipeAction(container,opt) {
        // TOUCH ENABLED SCROLL

        if (opt.touchenabled === 1)
            container.swipe( {data:container,
                swipeRight:function()
                {

                    if (opt.transition==0) {
                        opt.next = opt.next-1;
                        opt.leftarrowpressed=1;
                        if (opt.next < 0) opt.next=opt.slideamount-1;
                        callingNewSlide(opt,container);
                    }
                },
                swipeLeft:function()
                {

                    if (opt.transition==0) {
                        opt.next = opt.next+1;
                        if (opt.next == opt.slideamount) opt.next=0;
                        callingNewSlide(opt,container);
                    }
                },
                allowPageScroll:"auto"} );
    }    

    //////////////////////////////////////////////////////////
    //	-	SET THE IMAGE SIZE TO FIT INTO THE CONTIANER -  //
    ////////////////////////////////////////////////////////
    function setSize(img,opt) {
        opt.width = parseInt(opt.container.width(),0);
        opt.height = parseInt(opt.container.height(),0);

        opt.bw = opt.width / opt.startwidth;
        opt.bh = opt.height / opt.startheight;

        if (opt.bh > 1) {
            opt.bw = 1;
            opt.bh = 1;
        }


        // IF IMG IS ALREADY PREPARED, WE RESET THE SIZE FIRST HERE
        if (img.data('orgw')!=undefined) {
            img.width(img.data('orgw'));
            img.height(img.data('orgh'));
        }


        var fw = opt.width / img.width();
        var fh = opt.height / img.height();


        opt.fw = fw;
        opt.fh = fh;

        if (img.data('orgw')==undefined) {
            img.data('orgw',img.width());
            img.data('orgh',img.height());
        }



        if (opt.fullWidth=== 1) {

            var cow = opt.container.parent().width();
            var coh = opt.container.parent().height();
            var ffh = coh / img.data('orgh');
            var ffw = cow / img.data('orgw');


            img.width(img.width()*ffh);
            img.height(coh);

            if (img.width()<cow) {
                img.width(cow+50);
                var ffw = img.width() / img.data('orgw');
                img.height(img.data('orgh')*ffw);
            }

            if (img.width()>cow) {
                img.data("fxof",(cow/2 - img.width()/2));

                img.css({'position':'absolute','left':img.data('fxof')+"px"});
            }


        } else {

            img.width(opt.width);
            img.height(img.height()*fw);

            if (img.height()<opt.height && img.height()!=0 && img.height()!=null) {

                img.height(opt.height);
                img.width(img.data('orgw')*fh);
            }
        }



        img.data('neww',img.width());
        img.data('newh',img.height());
        if (opt.fullWidth === 1) {
            opt.slotw=Math.ceil(img.width()/opt.slots);
        } else {
            opt.slotw=Math.ceil(opt.width/opt.slots);
        }
        opt.sloth=Math.ceil(opt.height/opt.slots);

    }

    /////////////////////////////////////////
    //	-	PREPARE THE SLIDES / SLOTS -  //
    ///////////////////////////////////////
    function prepareSlides(container,opt) {

        container.find('.slider-caption').each(function(index, caption) {
            var caption = $(caption);
            caption.addClass(caption.data('transition')).addClass('start');
            var link = caption.data('link');
            if($.type(link) !== 'undefined'){
                var target = caption.data('target') || '_self';
                caption.on('click', function(event){
                    event.preventDefault();
                    event.stopPropagation();
                    window.open(link, target);
                }).addClass('slider-caption-islink');
            }
        });

        container.find('>ul:first >li').each(function(index, slide){
            var slide = $(slide);
            var link = slide.data('link');
            if($.type(link) !== 'undefined'){
                var target = slide.data('target') || '_self';
                slide.on('click', function(event){
                    event.preventDefault();
                    event.stopPropagation();
                    window.open(link, target);
                }).addClass('slider-slide-islink');
            }
        });

        container.find('>ul:first >li >img').each(function(j) {

            var img=$(this);
            img.addClass('defaultimg');
            setSize(img,opt);
            setSize(img,opt);
            img.wrap('<div class="slotholder"></div>');
            img.css({'opacity':0});
            img.data('li-id',j);

        });
    }

    ///////////////////////
    // PREPARE THE SLIDE //
    //////////////////////
    function prepareOneSlide(slotholder,opt,visible) {

        var sh=slotholder;
        var img = sh.find('img')
        setSize(img,opt)
        var src = img.attr('src');
        var w = img.data('neww');
        var h = img.data('newh');
        var fulloff = img.data("fxof");
        if (fulloff==undefined) fulloff=0;


        var off=0;


        if (!visible)
            var off=0-opt.slotw;

        for (var i=0;i<opt.slots;i++)
            sh.append('<div class="slot" style="position:absolute;top:0px;left:'+(fulloff+i*opt.slotw)+'px;overflow:hidden;width:'+opt.slotw+'px;height:'+h+'px"><div class="slotslide" style="position:absolute;top:0px;left:'+off+'px;width:'+opt.slotw+'px;height:'+h+'px;overflow:hidden;"><img style="position:absolute;top:0px;left:'+(0-(i*opt.slotw))+'px;width:'+w+'px;height:'+h+'px" src="'+src+'"></div></div>');

    }

    ///////////////////////
    // PREPARE THE SLIDE //
    //////////////////////
    function prepareOneSlideV(slotholder,opt,visible) {

        var sh=slotholder;
        var img = sh.find('img')
        setSize(img,opt)
        var src = img.attr('src');
        var w = img.data('neww');
        var h = img.data('newh');
        var fulloff = img.data("fxof");
        if (fulloff==undefined) fulloff=0;
        var off=0;


        if (!visible)
            var off=0-opt.sloth;

        for (var i=0;i<opt.slots;i++)
            sh.append('<div class="slot" style="position:absolute;top:'+(i*opt.sloth)+'px;left:'+(fulloff)+'px;overflow:hidden;width:'+w+'px;height:'+(opt.sloth)+'px"><div class="slotslide" style="position:absolute;top:'+off+'px;left:0px;width:'+w+'px;height:'+opt.sloth+'px;overflow:hidden;"><img style="position:absolute;top:'+(0-(i*opt.sloth))+'px;left:0px;width:'+w+'px;height:'+h+'px" src="'+src+'"></div></div>');

    }

    ///////////////////////
    // PREPARE THE SLIDE //
    //////////////////////
    function prepareOneSlideBox(slotholder,opt,visible) {

        var sh=slotholder;
        var img = sh.find('img')
        setSize(img,opt)
        var src = img.attr('src');
        var w = img.data('neww');
        var h = img.data('newh');
        var fulloff = img.data("fxof");
        if (fulloff==undefined) fulloff=0;
        var off=0;




        // SET THE MINIMAL SIZE OF A BOX
        var basicsize = 0;
        if (opt.sloth>opt.slotw)
            basicsize=opt.sloth
        else
            basicsize=opt.slotw;


        if (!visible) {
            var off=0-basicsize;
        }

        opt.slotw = basicsize;
        opt.sloth = basicsize;
        var x=0;
        var y=0;



        for (var j=0;j<opt.slots;j++) {

            y=0;
            for (var i=0;i<opt.slots;i++) 	{


                sh.append('<div class="slot" '+
                    'style="position:absolute;'+
                    'top:'+y+'px;'+
                    'left:'+(fulloff+x)+'px;'+
                    'width:'+basicsize+'px;'+
                    'height:'+basicsize+'px;'+
                    'overflow:hidden;">'+

                    '<div class="slotslide" data-x="'+x+'" data-y="'+y+'" '+
                    'style="position:absolute;'+
                    'top:'+(0)+'px;'+
                    'left:'+(0)+'px;'+
                    'width:'+basicsize+'px;'+
                    'height:'+basicsize+'px;'+
                    'overflow:hidden;">'+

                    '<img style="position:absolute;'+
                    'top:'+(0-y)+'px;'+
                    'left:'+(0-x)+'px;'+
                    'width:'+w+'px;'+
                    'height:'+h+'px"'+
                    'src="'+src+'"></div></div>');
                y=y+basicsize;
            }
            x=x+basicsize;
        }
    }

    ///////////////////////
    //	REMOVE SLOTS	//
    /////////////////////
    function removeSlots(container,opt,time) {
        if (time==undefined)
            time==80

        setTimeout(function() {
            container.find('.slotholder .slot').each(function() {
                clearTimeout($(this).data('tout'));
                $(this).remove();
            });
            opt.transition = 0;
        },time);
    }

    ////////////////////////
    //	CAPTION POSITION  //
    ///////////////////////
    function setCaptionPositions(container,opt) {

        // FIND THE RIGHT CAPTIONS
        var actli = container.find('>li:eq('+opt.act+')');
        var nextli = container.find('>li:eq('+opt.next+')');

        // SET THE NEXT CAPTION AND REMOVE THE LAST CAPTION
        var nextcaption=nextli.find('.slider-caption');

        if (nextcaption.find('iframe')==0) {

            // MOVE THE CAPTIONS TO THE RIGHT POSITION
            if (nextcaption.hasClass('hcenter'))
                nextcaption.css({'height':opt.height+"px",'top':'0px','left':(opt.width/2 - nextcaption.outerWidth()/2)+'px'});
            else
            if (nextcaption.hasClass('vcenter'))
                nextcaption.css({'width':opt.width+"px",'left':'0px','top':(opt.height/2 - nextcaption.outerHeight()/2)+'px'});
        }
    }

    //////////////////////////////
    //                         //
    //	-	SWAP THE SLIDES -  //
    //                        //
    ////////////////////////////
    function swapSlide(container,opt) {


        opt.transition = 1;
        opt.videoplaying = false;

        try{
            var actli = container.find('>ul:first-child >li:eq('+opt.act+')');
        } catch(e) {
            var actli=container.find('>ul:first-child >li:eq(1)');
        }


        var nextli = container.find('>ul:first-child >li:eq('+opt.next+')');

        var actsh = actli.find('.slotholder');
        var nextsh = nextli.find('.slotholder');
        actli.css({'visibility':'visible'});
        nextli.css({'visibility':'visible'});

        if ($.browser.msie && $.browser.version<9) {
            if (nextli.data('transition')=="boxfade") nextli.data('transition',"boxslide");
            if (nextli.data('transition')=="slotfade-vertical") nextli.data('transition',"slotzoom-vertical");
            if (nextli.data('transition')=="slotfade-horizontal") nextli.data('transition',"slotzoom-horizontal");
        }


        // IF DELAY HAS BEEN SET VIA THE SLIDE, WE TAKE THE NEW VALUE, OTHER WAY THE OLD ONE...
        if (nextli.data('delay')!=undefined) {
            opt.cd=0;
            opt.delay=nextli.data('delay');
        } else {
            opt.delay=opt.origcd;
        }

        // RESET POSITION AND FADES OF LI'S
        actli.css({'left':'0px','top':'0px'});
        nextli.css({'left':'0px','top':'0px'});

        ///////////////////////////////////////
        // TRANSITION CHOOSE - RANDOM EFFECTS//
        ///////////////////////////////////////
        var nexttrans = 0;

        //ADDED BY MAGEMONKS
        var transition = nextli.data('transition');
        if(opt.init === true && opt.initTransitionStyle){
            transition = opt.initTransitionStyle;
        }

        if (transition =="boxslide") nexttrans = 0;
        else if (transition=="boxfade") nexttrans = 1;
        else if (transition=="slotslide-horizontal") nexttrans = 2
        else if (transition=="slotslide-vertical") nexttrans = 3
        else if (transition=="curtain-1") nexttrans = 4
        else if (transition=="curtain-2") nexttrans = 5
        else if (transition=="curtain-3") nexttrans = 6
        else if (transition=="slotzoom-horizontal") nexttrans = 7
        else if (transition=="slotzoom-vertical")  nexttrans = 8
        else if (transition=="slotfade-horizontal")  nexttrans = 9
        else if (transition=="slotfade-vertical") nexttrans = 10
        else if (transition=="fade") nexttrans = 11
        else if (transition=="slideleft")  nexttrans = 12
        else if (transition=="slideup") nexttrans = 13
        else if (transition=="slidedown") nexttrans = 14
        else if (transition=="slideright") nexttrans = 15;
        else if (transition=="papercut") nexttrans = 16;
        else if (transition=="3dcurtain-horizontal") nexttrans = 17;
        else if (transition=="3dcurtain-vertical") nexttrans = 18;
        else if (transition=="cubic") nexttrans = 19;
        else if (transition=="flyin") nexttrans = 20;
        else if (transition=="turnoff") nexttrans = 21;
        else {
            nexttrans=Math.round(Math.random()*20);
            nextli.data('slotamount',Math.round(Math.random()*12+4));
        }

        var direction=-1;
        if (opt.leftarrowpressed==1 || opt.act>opt.next) direction=1;

        if (nextli.data('transition')=="slidehorizontal") {
            nexttrans = 12
            if (opt.leftarrowpressed==1)
                nexttrans = 15
        }

        if (nextli.data('transition')=="slidevertical") {
            nexttrans = 13
            if (opt.leftarrowpressed==1)
                nexttrans = 14
        }

        opt.leftarrowpressed=0;

        if (nexttrans>21) nexttrans = 21;
        if (nexttrans<0) nexttrans = 0;

        if (!$.support.transition && nexttrans >16) {
            nexttrans=Math.round(Math.random()*16);
            nextli.data('slotamount',Math.round(Math.random()*12+4));
        };
        if (opt.ie && (nexttrans==17 || nexttrans==16 || nexttrans==2 || nexttrans==3 || nexttrans==9 || nexttrans==10 || nexttrans==11)) nexttrans=Math.round(Math.random()*3+12);


        // DEFINE THE MASTERSPEED FOR THE SLIDE //
        var masterspeed=300;
        if(opt.init === true && opt.initTransitionDuration){
            masterspeed = opt.initTransitionDuration;
        }
        else if (nextli.data('masterspeed') != undefined){
            masterspeed = nextli.data('masterspeed');
        }

        if(masterspeed < 100){
            masterspeed = 100;
        }
        if(masterspeed + 1000 >= opt.delay){
            masterspeed = opt.delay - 1000;
        }




        /////////////////////////////////////////////
        // SET THE BULLETS SELECTED OR UNSELECTED  //
        /////////////////////////////////////////////


        container.parent().find(".slider-bullet").each(function(index, bullet) {
            $(bullet).removeClass("selected");
            if (index == opt.next) $(bullet).addClass('selected');
        });


        //////////////////////////////////////////////////////////////////
        // 		SET THE NEXT CAPTION AND REMOVE THE LAST CAPTION		//
        //////////////////////////////////////////////////////////////////

        container.find('>li').each(function() {
            var li = $(this);
            if (li.index!=opt.act && li.index!=opt.next) li.css({'z-index':16});
        });

        actli.css({'z-index':18});
        nextli.css({'z-index':20});
        nextli.css({'opacity':0});


        ///////////////////////////
        //	ANIMATE THE CAPTIONS //
        ///////////////////////////
        removeTheCaptions(actli,opt);
        animateTheCaptions(nextli, opt);




        /////////////////////////////////////////////
        //	SET THE ACTUAL AMOUNT OF SLIDES !!     //
        //  SET A RANDOM AMOUNT OF SLOTS          //
        ///////////////////////////////////////////
        if(opt.init === true && opt.initTransitionSlots){
            opt.slots = opt.initTransitionSlots;
        }
        else if (nextli.data('slotamount')==undefined || nextli.data('slotamount')<1) {
            opt.slots=Math.round(Math.random()*12+4);
            if (nextli.data('transition')=="boxslide")
                opt.slots=Math.round(Math.random()*6+3);
        } else {
            opt.slots=nextli.data('slotamount');

        }

        /////////////////////////////////////////////
        //	SET THE ACTUAL AMOUNT OF SLIDES !!     //
        //  SET A RANDOM AMOUNT OF SLOTS          //
        ///////////////////////////////////////////
        if (nextli.data('rotate')==undefined)
            opt.rotate = 0
        else
        if (nextli.data('rotate')==999)
            opt.rotate=Math.round(Math.random()*360);
        else
            opt.rotate=nextli.data('rotate');
        if (!$.support.transition  || opt.ie || opt.ie9) opt.rotate=0;



        /////////////////////////////////////
        // THE SLOTSLIDE - TRANSITION I.  //
        ////////////////////////////////////
        if (nexttrans==0) {								// BOXSLIDE

            masterspeed = masterspeed + 100;
            if (opt.slots>10) opt.slots=10;

            nextli.css({'opacity':1});

            // PREPARE THE SLOTS HERE
            prepareOneSlideBox(actsh,opt,true);
            prepareOneSlideBox(nextsh,opt,false);

            //SET DEFAULT IMG UNVISIBLE
            nextsh.find('.defaultimg').css({'opacity':0});
            //actsh.find('.defaultimg').css({'opacity':0});


            // ALL NEW SLOTS SHOULD BE SLIDED FROM THE LEFT TO THE RIGHT


            nextsh.find('.slotslide').each(function(j) {
                var ss=$(this);
                if (opt.ie9)
                    ss.transition({top:(0-opt.sloth),left:(0-opt.slotw)},0);
                else
                    ss.transition({top:(0-opt.sloth),left:(0-opt.slotw), rotate:opt.rotate},0);
                setTimeout(function() {
                    ss.transition({top:0, left:0, scale:1, rotate:0},masterspeed*1.5,function() {

                        if (j==(opt.slots*opt.slots)-1) {
                            removeSlots(container,opt);
                            nextsh.find('.defaultimg').css({'opacity':1});

                            if (nextli.index()!=actli.index()) actsh.find('.defaultimg').css({'opacity':0});
                            opt.act=opt.next;
                        }
                    });
                },j*15);
            });
        }



        /////////////////////////////////////
        // THE SLOTSLIDE - TRANSITION I.  //
        ////////////////////////////////////
        if (nexttrans==1) {


            if (opt.slots>5) opt.slots=5;
            nextli.css({'opacity':1});

            // PREPARE THE SLOTS HERE
            //prepareOneSlideBox(actsh,opt,true);
            prepareOneSlideBox(nextsh,opt,false);

            //SET DEFAULT IMG UNVISIBLE
            nextsh.find('.defaultimg').css({'opacity':0});
            //actsh.find('.defaultimg').css({'opacity':0});


            // ALL NEW SLOTS SHOULD BE SLIDED FROM THE LEFT TO THE RIGHT

            nextsh.find('.slotslide').each(function(j) {
                var ss=$(this);
                ss.css({'opacity':0});
                ss.find('img').css({'opacity':0});
                if (opt.ie9)
                    ss.find('img').transition({'top':(Math.random()*opt.slotw-opt.slotw)+"px",'left':(Math.random()*opt.slotw-opt.slotw)+"px"},0);
                else
                    ss.find('img').transition({'top':(Math.random()*opt.slotw-opt.slotw)+"px",'left':(Math.random()*opt.slotw-opt.slotw)+"px", rotate:opt.rotate},0);

                var rand=Math.random()*1000+(masterspeed + 200);
                if (j==(opt.slots*opt.slots)-1) rand=1500;

                ss.find('img').transition({'opacity':1,'top':(0-ss.data('y'))+"px",'left':(0-ss.data('x'))+'px', rotate:0},rand);
                ss.transition({'opacity':1},rand,function() {
                    if (j==(opt.slots*opt.slots)-1) {
                        removeSlots(container,opt);
                        nextsh.find('.defaultimg').css({'opacity':1});
                        if (nextli.index()!=actli.index()) actsh.find('.defaultimg').css({'opacity':0});
                        opt.act=opt.next;
                    }

                });


            });
        }


        /////////////////////////////////////
        // THE SLOTSLIDE - TRANSITION I.  //
        ////////////////////////////////////
        if (nexttrans==2) {


            masterspeed = masterspeed + 200;

            nextli.css({'opacity':1});

            // PREPARE THE SLOTS HERE
            prepareOneSlide(actsh,opt,true);
            prepareOneSlide(nextsh,opt,false);

            //SET DEFAULT IMG UNVISIBLE
            nextsh.find('.defaultimg').css({'opacity':0});
            //actsh.find('.defaultimg').css({'opacity':0});


            // ALL OLD SLOTS SHOULD BE SLIDED TO THE RIGHT
            actsh.find('.slotslide').each(function() {
                var ss=$(this);


                //ss.animate({'left':opt.slotw+'px'},{duration:masterspeed,queue:false,complete:function() {
                ss.transit({'left':opt.slotw+'px',rotate:(0-opt.rotate)},masterspeed,function() {
                    removeSlots(container,opt);
                    nextsh.find('.defaultimg').css({'opacity':1});
                    if (nextli.index()!=actli.index()) actsh.find('.defaultimg').css({'opacity':0});
                    opt.act=opt.next;
                });

            });

            // ALL NEW SLOTS SHOULD BE SLIDED FROM THE LEFT TO THE RIGHT
            nextsh.find('.slotslide').each(function() {
                var ss=$(this);
                if (opt.ie9)
                    ss.transit({'left':(0-opt.slotw)+"px"},0);
                else
                    ss.transit({'left':(0-opt.slotw)+"px",rotate:opt.rotate},0);

                ss.transit({'left':'0px',rotate:0},masterspeed,function() {
                    removeSlots(container,opt);
                    nextsh.find('.defaultimg').css({'opacity':1});
                    if (nextli.index()!=actli.index()) actsh.find('.defaultimg').css({'opacity':0});
                    if ($.browser.msie && $.browser.version<9) actsh.find('.defaultimg').css({'opacity':1});
                    opt.act=opt.next;
                });

            });
        }



        /////////////////////////////////////
        // THE SLOTSLIDE - TRANSITION I.  //
        ////////////////////////////////////
        if (nexttrans==3) {


            masterspeed = masterspeed + 200;
            nextli.css({'opacity':1});

            // PREPARE THE SLOTS HERE
            prepareOneSlideV(actsh,opt,true);
            prepareOneSlideV(nextsh,opt,false);

            //SET DEFAULT IMG UNVISIBLE
            nextsh.find('.defaultimg').css({'opacity':0});
            //actsh.find('.defaultimg').css({'opacity':0});

            // ALL OLD SLOTS SHOULD BE SLIDED TO THE RIGHT
            actsh.find('.slotslide').each(function() {
                var ss=$(this);

                ss.transit({'top':opt.sloth+'px',rotate:opt.rotate},masterspeed,function() {
                    removeSlots(container,opt);
                    nextsh.find('.defaultimg').css({'opacity':1});
                    if (nextli.index()!=actli.index()) actsh.find('.defaultimg').css({'opacity':0});
                    opt.act=opt.next;
                });

            });

            // ALL NEW SLOTS SHOULD BE SLIDED FROM THE LEFT TO THE RIGHT
            nextsh.find('.slotslide').each(function() {
                var ss=$(this);
                if (opt.ie9)
                    ss.transit({'top':(0-opt.sloth)+"px"},0);
                else
                    ss.transit({'top':(0-opt.sloth)+"px",rotate:opt.rotate},0);
                ss.transit({'top':'0px',rotate:0},masterspeed,function() {
                    removeSlots(container,opt);
                    nextsh.find('.defaultimg').css({'opacity':1});
                    if (nextli.index()!=actli.index()) actsh.find('.defaultimg').css({'opacity':0});
                    opt.act=opt.next;
                });

            });
        }



        /////////////////////////////////////
        // THE SLOTSLIDE - TRANSITION I.  //
        ////////////////////////////////////
        if (nexttrans==4) {



            nextli.css({'opacity':1});

            // PREPARE THE SLOTS HERE
            prepareOneSlide(actsh,opt,true);
            prepareOneSlide(nextsh,opt,true);

            //SET DEFAULT IMG UNVISIBLE
            nextsh.find('.defaultimg').css({'opacity':0});
            actsh.find('.defaultimg').css({'opacity':0});


            // ALL NEW SLOTS SHOULD BE SLIDED FROM THE LEFT TO THE RIGHT
            actsh.find('.slotslide').each(function(i) {
                var ss=$(this);

                ss.transit({'top':(0+(opt.height))+"px",'opacity':1,rotate:opt.rotate},masterspeed+(i*(70-opt.slots)));
            });

            // ALL NEW SLOTS SHOULD BE SLIDED FROM THE LEFT TO THE RIGHT
            nextsh.find('.slotslide').each(function(i) {
                var ss=$(this);
                if (opt.ie9)
                    ss.transition({'top':(0-(opt.height))+"px",'opacity':0},0);
                else
                    ss.transition({'top':(0-(opt.height))+"px",'opacity':0,rotate:opt.rotate},0);

                ss.transition({'top':'0px','opacity':1,rotate:0},masterspeed+(i*(70-opt.slots)),function() {
                    if (i==opt.slots-1) {
                        removeSlots(container,opt);
                        nextsh.find('.defaultimg').css({'opacity':1});
                        if (nextli.index()!=actli.index()) actsh.find('.defaultimg').css({'opacity':0});
                        opt.act=opt.next;
                    }

                });

            });
        }


        /////////////////////////////////////
        // THE SLOTSLIDE - TRANSITION I.  //
        ////////////////////////////////////
        if (nexttrans==5) {



            nextli.css({'opacity':1});

            // PREPARE THE SLOTS HERE
            prepareOneSlide(actsh,opt,true);
            prepareOneSlide(nextsh,opt,true);

            //SET DEFAULT IMG UNVISIBLE
            nextsh.find('.defaultimg').css({'opacity':0});
            actsh.find('.defaultimg').css({'opacity':0});

            // ALL NEW SLOTS SHOULD BE SLIDED FROM THE LEFT TO THE RIGHT
            actsh.find('.slotslide').each(function(i) {
                var ss=$(this);

                ss.transition({'top':(0+(opt.height))+"px",'opacity':1,rotate:opt.rotate},masterspeed+((opt.slots-i)*(70-opt.slots)));

            });

            // ALL NEW SLOTS SHOULD BE SLIDED FROM THE LEFT TO THE RIGHT
            nextsh.find('.slotslide').each(function(i) {
                var ss=$(this);
                if (opt.ie9)
                    ss.transition({'top':(0-(opt.height))+"px",'opacity':0},0);
                else
                    ss.transition({'top':(0-(opt.height))+"px",'opacity':0,rotate:opt.rotate},0);

                ss.transition({'top':'0px','opacity':1,rotate:0},masterspeed+((opt.slots-i)*(70-opt.slots)),function() {
                    if (i==0) {
                        removeSlots(container,opt);
                        nextsh.find('.defaultimg').css({'opacity':1});
                        if (nextli.index()!=actli.index()) actsh.find('.defaultimg').css({'opacity':0});
                        opt.act=opt.next;
                    }
                });

            });
        }


        /////////////////////////////////////
        // THE SLOTSLIDE - TRANSITION I.  //
        ////////////////////////////////////
        if (nexttrans==6) {



            nextli.css({'opacity':1});
            if (opt.slots<2) opt.slots=2;
            // PREPARE THE SLOTS HERE
            prepareOneSlide(actsh,opt,true);
            prepareOneSlide(nextsh,opt,true);

            //SET DEFAULT IMG UNVISIBLE
            nextsh.find('.defaultimg').css({'opacity':0});
            actsh.find('.defaultimg').css({'opacity':0});


            actsh.find('.slotslide').each(function(i) {
                var ss=$(this);

                if (i<opt.slots/2)
                    var tempo = (i+2)*60;
                else
                    var tempo = (2+opt.slots-i)*60;


                ss.transition({'top':(0+(opt.height))+"px",'opacity':1},masterspeed+tempo);

            });

            // ALL NEW SLOTS SHOULD BE SLIDED FROM THE LEFT TO THE RIGHT
            nextsh.find('.slotslide').each(function(i) {
                var ss=$(this);
                if (opt.ie9)
                    ss.transition({'top':(0-(opt.height))+"px",'opacity':0},0);
                else
                    ss.transition({'top':(0-(opt.height))+"px",'opacity':0,rotate:opt.rotate},0);
                if (i<opt.slots/2)
                    var tempo = (i+2)*60;
                else
                    var tempo = (2+opt.slots-i)*60;


                ss.transition({'top':'0px','opacity':1,rotate:0},masterspeed+tempo,function() {
                    if (i==Math.round(opt.slots/2)) {
                        removeSlots(container,opt);
                        nextsh.find('.defaultimg').css({'opacity':1});
                        if (nextli.index()!=actli.index()) actsh.find('.defaultimg').css({'opacity':0});
                        opt.act=opt.next;
                    }
                });
            });
        }


        ////////////////////////////////////
        // THE SLOTSZOOM - TRANSITION II. //
        ////////////////////////////////////
        if (nexttrans==7) {

            masterspeed = masterspeed * 3;
            nextli.css({'opacity':1});

            // PREPARE THE SLOTS HERE
            prepareOneSlide(actsh,opt,true);
            prepareOneSlide(nextsh,opt,true);

            //SET DEFAULT IMG UNVISIBLE
            nextsh.find('.defaultimg').css({'opacity':0});
            //actsh.find('.defaultimg').css({'opacity':0});

            // ALL OLD SLOTS SHOULD BE SLIDED TO THE RIGHT
            actsh.find('.slotslide').each(function() {
                var ss=$(this).find('img');

                ss.transition({'left':(0-opt.slotw/2)+'px',
                    'top':(0-opt.height/2)+'px',
                    'width':(opt.slotw*2)+"px",
                    'height':(opt.height*2)+"px",
                    opacity:0,
                    rotate:opt.rotate
                },masterspeed,function() {
                    removeSlots(container,opt);
                    nextsh.find('.defaultimg').css({'opacity':1});
                    if (nextli.index()!=actli.index()) actsh.find('.defaultimg').css({'opacity':0});
                    opt.act = opt.next;
                });

            });

            /						//////////////////////////////////////////////////////////////
            // ALL NEW SLOTS SHOULD BE SLIDED FROM THE LEFT TO THE RIGHT //
            ///////////////////////////////////////////////////////////////
            nextsh.find('.slotslide').each(function(i) {
                var ss=$(this).find('img');

                if (opt.ie9)
                    ss.transition({'left':(0)+'px','top':(0)+'px',opacity:0},0);
                else
                    ss.transition({'left':(0)+'px','top':(0)+'px',opacity:0,rotate:opt.rotate},0);
                ss.transition({'left':(0-i*opt.slotw)+'px',
                    'top':(0)+'px',
                    'width':(nextsh.find('.defaultimg').data('neww'))+"px",
                    'height':(nextsh.find('.defaultimg').data('newh'))+"px",
                    opacity:1,rotate:0

                },masterspeed,function() {
                    removeSlots(container,opt);
                    nextsh.find('.defaultimg').css({'opacity':1});
                    if (nextli.index()!=actli.index()) actsh.find('.defaultimg').css({'opacity':0});
                    opt.act = opt.next;
                });
            });
        }




        ////////////////////////////////////
        // THE SLOTSZOOM - TRANSITION II. //
        ////////////////////////////////////
        if (nexttrans==8) {

            masterspeed = masterspeed * 3;
            nextli.css({'opacity':1});

            // PREPARE THE SLOTS HERE
            prepareOneSlideV(actsh,opt,true);
            prepareOneSlideV(nextsh,opt,true);

            //SET DEFAULT IMG UNVISIBLE
            nextsh.find('.defaultimg').css({'opacity':0});
            //actsh.find('.defaultimg').css({'opacity':0});

            // ALL OLD SLOTS SHOULD BE SLIDED TO THE RIGHT
            actsh.find('.slotslide').each(function() {
                var ss=$(this).find('img');

                ss.transition({'left':(0-opt.width/2)+'px',
                    'top':(0-opt.sloth/2)+'px',
                    'width':(opt.width*2)+"px",
                    'height':(opt.sloth*2)+"px",
                    opacity:0,rotate:opt.rotate
                },masterspeed,function() {
                    removeSlots(container,opt);
                    nextsh.find('.defaultimg').css({'opacity':1});
                    if (nextli.index()!=actli.index()) actsh.find('.defaultimg').css({'opacity':0});

                    opt.act = opt.next;
                });

            });


            // ALL NEW SLOTS SHOULD BE SLIDED FROM THE LEFT TO THE RIGHT //
            ///////////////////////////////////////////////////////////////
            nextsh.find('.slotslide').each(function(i) {
                var ss=$(this).find('img');
                if (opt.ie9)
                    ss.transition({'left':(0)+'px','top':(0)+'px',opacity:0},0);
                else
                    ss.transition({'left':(0)+'px','top':(0)+'px',opacity:0,rotate:opt.rotate},0);
                ss.transition({'left':(0)+'px',
                    'top':(0-i*opt.sloth)+'px',
                    'width':(nextsh.find('.defaultimg').data('neww'))+"px",
                    'height':(nextsh.find('.defaultimg').data('newh'))+"px",
                    opacity:1,rotate:0
                },masterspeed,function() {
                    removeSlots(container,opt);
                    nextsh.find('.defaultimg').css({'opacity':1});
                    if (nextli.index()!=actli.index()) actsh.find('.defaultimg').css({'opacity':0});

                    opt.act = opt.next;
                });

            });
        }


        ////////////////////////////////////////
        // THE SLOTSFADE - TRANSITION III.   //
        //////////////////////////////////////
        if (nexttrans==9) {



            nextli.css({'opacity':1});

            opt.slots = opt.width/20;

            prepareOneSlide(nextsh,opt,true);


            //actsh.find('.defaultimg').css({'opacity':0});
            nextsh.find('.defaultimg').css({'opacity':0});

            var ssamount=0;
            // ALL NEW SLOTS SHOULD BE SLIDED FROM THE LEFT TO THE RIGHT
            nextsh.find('.slotslide').each(function(i) {
                var ss=$(this);
                ssamount++;
                ss.transition({'opacity':0,x:0,y:0},0);
                ss.data('tout',setTimeout(function() {
                    ss.transition({x:0,y:0,'opacity':1},masterspeed);

                },i*4)
                );

            });

            //nextsh.find('.defaultimg').transition({'opacity':1},(masterspeed+(ssamount*4)));

            setTimeout(function() {
                removeSlots(container,opt);
                nextsh.find('.defaultimg').css({'opacity':1});
                if (nextli.index()!=actli.index()) actsh.find('.defaultimg').css({'opacity':0});
                if ($.browser.msie && $.browser.version<9) actsh.find('.defaultimg').css({'opacity':1});

                opt.act = opt.next;
            },(masterspeed+(ssamount*4)));
        }




        ////////////////////////////////////////
        // THE SLOTSFADE - TRANSITION III.   //
        //////////////////////////////////////
        if (nexttrans==10) {



            nextli.css({'opacity':1});

            opt.slots = opt.height/20;

            prepareOneSlideV(nextsh,opt,true);


            //actsh.find('.defaultimg').css({'opacity':0});
            nextsh.find('.defaultimg').css({'opacity':0});

            var ssamount=0;
            // ALL NEW SLOTS SHOULD BE SLIDED FROM THE LEFT TO THE RIGHT
            nextsh.find('.slotslide').each(function(i) {
                var ss=$(this);
                ssamount++;
                ss.transition({'opacity':0,x:0,y:0},0);
                ss.data('tout',setTimeout(function() {
                    ss.transition({x:0,y:0,'opacity':1},masterspeed);

                },i*4)
                );

            });

            //nextsh.find('.defaultimg').transition({'opacity':1},(masterspeed+(ssamount*4)));

            setTimeout(function() {
                removeSlots(container,opt);
                nextsh.find('.defaultimg').css({'opacity':1});
                if (nextli.index()!=actli.index()) actsh.find('.defaultimg').css({'opacity':0});
                if ($.browser.msie && $.browser.version<9) actsh.find('.defaultimg').css({'opacity':1});

                opt.act = opt.next;
            },(masterspeed+(ssamount*4)));
        }


        ///////////////////////////
        // SIMPLE FADE ANIMATION //
        ///////////////////////////

        if (nexttrans==11) {
            nextli.css({'opacity':1});
            opt.slots = 1;

            prepareOneSlide(nextsh,opt,true);

            //actsh.find('.defaultimg').css({'opacity':0});
            nextsh.find('.defaultimg').css({'opacity':0});

            var ssamount=0;
            // ALL NEW SLOTS SHOULD BE SLIDED FROM THE LEFT TO THE RIGHT

            nextsh.find('.slotslide').each(function(i) {
                var ss=$(this);
                ssamount++;
                if (opt.ie9)
                    ss.transition({'opacity':0},0);
                else
                    ss.transition({'opacity':0,rotate:opt.rotate},0);

                ss.transition({'opacity':1,rotate:0},masterspeed);
            });

            setTimeout(function() {
                removeSlots(container,opt);
                nextsh.find('.defaultimg').css({'opacity':1});
                if (nextli.index()!=actli.index()) actsh.find('.defaultimg').css({'opacity':0});
                if ($.browser.msie && $.browser.version<9) actsh.find('.defaultimg').css({'opacity':1});

                opt.act = opt.next;
            },masterspeed);
        }





        if (nexttrans==12 || nexttrans==13 || nexttrans==14 || nexttrans==15) {

            masterspeed = masterspeed * 3;
            nextli.css({'opacity':1});

            opt.slots = 1;

            prepareOneSlide(nextsh,opt,true);
            prepareOneSlide(actsh,opt,true);


            actsh.find('.defaultimg').css({'opacity':0});
            nextsh.find('.defaultimg').css({'opacity':0});

            var oow = opt.width;
            var ooh = opt.height;
            if (opt.fullWidth === 1) {
                oow=opt.container.parent().width();
                ooh=opt.container.parent().height();

            }

            // ALL NEW SLOTS SHOULD BE SLIDED FROM THE LEFT TO THE RIGHT
            var ssn=nextsh.find('.slotslide')
            if (nexttrans==12)
                if (opt.ie9)
                    ssn.transition({'left':oow+"px"},0);
                else
                    ssn.transition({'left':oow+"px",rotate:opt.rotate},0);
            else
            if (nexttrans==15)
                if (opt.ie9)
                    ssn.transition({'left':(0-opt.width)+"px"},0);
                else
                    ssn.transition({'left':(0-opt.width)+"px",rotate:opt.rotate},0);
            else
            if (nexttrans==13)
                if (opt.ie9)
                    ssn.transition({'top':(ooh)+"px"},0);
                else
                    ssn.transition({'top':(ooh)+"px",rotate:opt.rotate},0);
            else
            if (nexttrans==14)
                if (opt.ie9)
                    ssn.transition({'top':(0-opt.height)+"px"},0);
                else
                    ssn.transition({'top':(0-opt.height)+"px",rotate:opt.rotate},0);


            ssn.transition({'left':'0px','top':'0px',opacity:1,rotate:0},masterspeed,function() {


                removeSlots(container,opt,0);
                if (nextli.index()!=actli.index()) actsh.find('.defaultimg').css({'opacity':0});
                nextsh.find('.defaultimg').css({'opacity':1});
                opt.act = opt.next;
            });



            var ssa=actsh.find('.slotslide');

            if (nexttrans==12)
                ssa.transition({'left':(0-oow)+'px',opacity:1,rotate:0},masterspeed);
            else
            if (nexttrans==15)
                ssa.transition({'left':(oow)+'px',opacity:1,rotate:0},masterspeed);
            else
            if (nexttrans==13)
                ssa.transition({'top':(0-ooh)+'px',opacity:1,rotate:0},masterspeed);
            else
            if (nexttrans==14)
                ssa.transition({'top':(ooh)+'px',opacity:1,rotate:0},masterspeed);



        }


        //////////////////////////////////////
        // THE SLOTSLIDE - TRANSITION XVI.  //
        //////////////////////////////////////
        if (nexttrans==16) {						// PAPERCUT

            actli.css({'position':'absolute','z-index':20});
            nextli.css({'position':'absolute','z-index':15});
            // PREPARE THE CUTS
            actli.wrapInner('<div class="tp-half-one"></div>');
            actli.find('.tp-half-one').clone(true).appendTo(actli).addClass("tp-half-two");
            actli.find('.tp-half-two').removeClass('tp-half-one');
            actli.find('.tp-half-two').wrapInner('<div class="tp-offset"></div>');

            // ANIMATE THE CUTS
            actli.find('.tp-half-one').css({'width':opt.width+"px",'height':opt.height/2+"px",'overflow':'hidden','position':'absolute','top':'0px','left':'0px'});
            actli.find('.tp-half-two').css({'width':opt.width+"px",'height':opt.height/2+"px",'overflow':'hidden','position':'absolute','top':opt.height/2+'px','left':'0px'});
            actli.find('.tp-half-two .tp-offset').css({'position':'absolute','top':(0-opt.height/2)+'px','left':'0px'});

            // Delegate .transition() calls to .animate()
            // if the browser can't do CSS transitions.
            if (!$.support.transition) {

                actli.find('.tp-half-one').animate({'top':(0-opt.height/2)+"px"},{duration: 500,queue:false});
                actli.find('.tp-half-two').animate({'top':(opt.height)+"px"},{duration: 500,queue:false});
            } else {
                var ro1=Math.round(Math.random()*40-20);
                var ro2=Math.round(Math.random()*40-20);
                var sc1=Math.random()*1+1;
                var sc2=Math.random()*1+1;
                actli.find('.tp-half-one').transition({opacity:1, scale:sc1, rotate:ro1,y:(0-opt.height/1.4)+"px"},800,'in');
                actli.find('.tp-half-two').transition({opacity:1, scale:sc2, rotate:ro2,y:(0+opt.height/1.4)+"px"},800,'in');

                if (actli.html()!=null) nextli.transition({scale:0.8,x:opt.width*0.1, y:opt.height*0.1, rotate:ro1},0).transition({rotate:0, scale:1,x:0,y:0},600,'snap');
            }
            nextsh.find('.defaultimg').css({'opacity':1});
            setTimeout(function() {


                // CLEAN UP BEFORE WE START
                actli.css({'position':'absolute','z-index':18});
                nextli.css({'position':'absolute','z-index':20});
                nextsh.find('.defaultimg').css({'opacity':1});
                actsh.find('.defaultimg').css({'opacity':0});
                if (actli.find('.tp-half-one').length>0)  {
                    actli.find('.tp-half-one >img, .tp-half-one >div').unwrap();

                }
                actli.find('.tp-half-two').remove();
                opt.transition = 0;
                opt.act = opt.next;

            },800);
            nextli.css({'opacity':1});

        }

        ////////////////////////////////////////
        // THE SLOTSLIDE - TRANSITION XVII.  //
        ///////////////////////////////////////
        if (nexttrans==17) {								// 3D CURTAIN HORIZONTAL

            masterspeed = masterspeed + 100;
            if (opt.slots>10) opt.slots=10;

            nextli.css({'opacity':1});

            // PREPARE THE SLOTS HERE
            prepareOneSlideV(actsh,opt,true);
            prepareOneSlideV(nextsh,opt,false);

            //SET DEFAULT IMG UNVISIBLE
            nextsh.find('.defaultimg').css({'opacity':0});
            //actsh.find('.defaultimg').css({'opacity':0});


            // ALL NEW SLOTS SHOULD BE SLIDED FROM THE LEFT TO THE RIGHT


            nextsh.find('.slotslide').each(function(j) {
                var ss=$(this);
                ss.transition({ rotateY:350 ,rotateX:40, perspective:'1400px', rotate:0},0);
                setTimeout(function() {
                    ss.transition({top:0, left:0, scale:1, perspective:'150px', rotate:0,rotateY:0, rotateX:0},masterspeed*2,function() {

                        if (j==opt.slots-1) {
                            removeSlots(container,opt);
                            nextsh.find('.defaultimg').css({'opacity':1});

                            if (nextli.index()!=actli.index()) actsh.find('.defaultimg').css({'opacity':0});
                            opt.act=opt.next;
                        }
                    });
                },j*100);
            });
        }



        ////////////////////////////////////////
        // THE SLOTSLIDE - TRANSITION XVIII.  //
        ///////////////////////////////////////
        if (nexttrans==18) {								// 3D CURTAIN VERTICAL

            masterspeed = masterspeed + 100;
            if (opt.slots>10) opt.slots=10;

            nextli.css({'opacity':1});

            // PREPARE THE SLOTS HERE
            prepareOneSlide(actsh,opt,true);
            prepareOneSlide(nextsh,opt,false);

            //SET DEFAULT IMG UNVISIBLE
            nextsh.find('.defaultimg').css({'opacity':0});
            //actsh.find('.defaultimg').css({'opacity':0});


            // ALL NEW SLOTS SHOULD BE SLIDED FROM THE LEFT TO THE RIGHT


            nextsh.find('.slotslide').each(function(j) {
                var ss=$(this);
                ss.transition({ rotateX:10 ,rotateY:310, perspective:'1400px', rotate:0,opacity:0},0);
                setTimeout(function() {
                    ss.transition({top:0, left:0, scale:1, perspective:'150px', rotate:0,rotateY:0, rotateX:0,opacity:1},masterspeed*2,function() {

                        if (j==opt.slots-1) {
                            removeSlots(container,opt);
                            nextsh.find('.defaultimg').css({'opacity':1});

                            if (nextli.index()!=actli.index()) actsh.find('.defaultimg').css({'opacity':0});
                            opt.act=opt.next;
                        }
                    });
                },j*100);
            });
        }

        ////////////////////////////////////////
        // THE SLOTSLIDE - TRANSITION XIX.  //
        ///////////////////////////////////////
        if (nexttrans==19) {								// CUBIC VERTICAL
            masterspeed = masterspeed + 100;
            if (opt.slots>10) opt.slots=10;

            nextli.css({'opacity':1});

            // PREPARE THE SLOTS HERE
            prepareOneSlide(actsh,opt,true);
            prepareOneSlide(nextsh,opt,false);

            //SET DEFAULT IMG UNVISIBLE
            nextsh.find('.defaultimg').css({'opacity':0});
            //actsh.find('.defaultimg').css({'opacity':0});


            // ALL NEW SLOTS SHOULD BE SLIDED FROM THE LEFT TO THE RIGHT


            nextsh.find('.slotslide').each(function(j) {
                var ss=$(this);
                if (direction==1)
                    ss.transition({ left:0,top:0+(opt.height/2),perspective: opt.height*2,rotate3d: '100, 0, 0, -90deg'},0);
                else
                    ss.transition({ left:0,top:0-(opt.height/2),perspective: opt.height*2,rotate3d: '100, 0, 0, 90deg'},0);
                setTimeout(function() {
                    ss.transition({top:0,perspective: opt.height*2,rotate3d: '0, 0, 0, 0deg'},masterspeed*2,function() {

                        if (j==opt.slots-1) {
                            removeSlots(container,opt);
                            nextsh.find('.defaultimg').css({'opacity':1});

                            if (nextli.index()!=actli.index()) actsh.find('.defaultimg').css({'opacity':0});
                            opt.act=opt.next;
                        }
                    });
                },j*100);
            });

            actsh.find('.slotslide').each(function(j) {
                var ss=$(this);
                ss.transition({ top:0,perspective: opt.height*2,rotate3d: '0, 0, 0, 0deg'},0);
                actsh.find('.defaultimg').css({'opacity':0});
                setTimeout(function() {
                    if (direction==1)
                        ss.transition({left:0,top:(0-opt.height/2),perspective: opt.height,rotate3d: '100, 0, 0, 90deg'},masterspeed*1.5,function() {});
                    else
                        ss.transition({left:0,top:(opt.height/2),perspective: opt.height,rotate3d: '100, 0, 0, -90deg'},masterspeed*1.5,function() {});
                },j*100);
            });
        }

        ////////////////////////////////////////
        // THE SLOTSLIDE - TRANSITION XX.  //
        ///////////////////////////////////////
        if (nexttrans==20) {								// FLYIN
            masterspeed = masterspeed + 100;
            if (opt.slots>10) opt.slots=10;

            nextli.css({'opacity':1});

            // PREPARE THE SLOTS HERE
            prepareOneSlideV(actsh,opt,true);
            prepareOneSlideV(nextsh,opt,false);

            //SET DEFAULT IMG UNVISIBLE
            nextsh.find('.defaultimg').css({'opacity':0});
            //actsh.find('.defaultimg').css({'opacity':0});

            // ALL NEW SLOTS SHOULD BE SLIDED FROM THE LEFT TO THE RIGHT

            nextsh.find('.slotslide').each(function(j) {
                var ss=$(this);
                if (direction==1)
                    ss.transition({ scale:0.8,top:0,left:0-opt.width,perspective:opt.width,rotate3d: '2, 5, 0, 110deg'},0);
                else
                    ss.transition({ scale:0.8,top:0,left:0+opt.width,perspective:opt.width,rotate3d: '2, 5, 0, -110deg'},0);
                setTimeout(function() {
                    ss.transition({scale:0.8,left:0,perspective: opt.width,rotate3d: '1, 5, 0, 0deg'},masterspeed*2,'ease').transition({scale:1},200,'out',function() {

                        if (j==opt.slots-1) {
                            removeSlots(container,opt);
                            nextsh.find('.defaultimg').css({'opacity':1});

                            if (nextli.index()!=actli.index()) actsh.find('.defaultimg').css({'opacity':0});
                            opt.act=opt.next;
                        }
                    });
                },j*100);
            });

            actsh.find('.slotslide').each(function(j) {
                var ss=$(this);
                ss.transition({ scale:0.5,left:0,perspective: 500,rotate3d: '1, 5, 0, 5deg'},300,'in-out');
                actsh.find('.defaultimg').css({'opacity':0});
                setTimeout(function() {
                    if (direction==1)
                        ss.transition({top:0,left:opt.width/2,perspective: opt.width,rotate3d: '0, -3, 0, 70deg',opacity:0},masterspeed*2,'out',function() {});
                    else
                        ss.transition({top:0,left:0-opt.width/2,perspective: opt.width,rotate3d: '0, -3, 0, -70deg',opacity:0},masterspeed*2,'out',function() {});
                },j*100);
            });
        }


        ////////////////////////////////////////
        // THE SLOTSLIDE - TRANSITION XX.  //
        ///////////////////////////////////////
        if (nexttrans==21) {								// TURNOFF
            masterspeed = masterspeed + 100;
            if (opt.slots>10) opt.slots=10;

            nextli.css({'opacity':1});

            // PREPARE THE SLOTS HERE
            prepareOneSlideV(actsh,opt,true);
            prepareOneSlideV(nextsh,opt,false);

            //SET DEFAULT IMG UNVISIBLE
            nextsh.find('.defaultimg').css({'opacity':0});
            //actsh.find('.defaultimg').css({'opacity':0});


            // ALL NEW SLOTS SHOULD BE SLIDED FROM THE LEFT TO THE RIGHT


            nextsh.find('.slotslide').each(function(j) {
                var ss=$(this);
                if (direction==1)
                    ss.transition({ top:0,left:0-(opt.width/2),perspective: opt.width*2,rotate3d: '0, 100, 0, 90deg'},0);
                else
                    ss.transition({ top:0,left:0+(opt.width/2),perspective: opt.width*2,rotate3d: '0, 100, 0, -90deg'},0);
                setTimeout(function() {
                    ss.transition({left:0,perspective: opt.width*2,rotate3d: '0, 0, 0, 0deg'},masterspeed*2,function() {

                        if (j==opt.slots-1) {
                            removeSlots(container,opt);
                            nextsh.find('.defaultimg').css({'opacity':1});

                            if (nextli.index()!=actli.index()) actsh.find('.defaultimg').css({'opacity':0});
                            opt.act=opt.next;
                        }
                    });
                },j*100);
            });

            actsh.find('.slotslide').each(function(j) {
                var ss=$(this);
                ss.transition({ left:0,perspective: opt.width*2,rotate3d: '0, 0, 0, 0deg'},0);
                actsh.find('.defaultimg').css({'opacity':0});
                setTimeout(function() {
                    if (direction==1)
                        ss.transition({top:0,left:(opt.width/2),perspective: opt.width,rotate3d: '0, 1000, 0, -90deg'},masterspeed*1.5,function() {});
                    else
                        ss.transition({top:0,left:(0-opt.width/2),perspective: opt.width,rotate3d: '0, 1000, 0, +90deg'},masterspeed*1.5,function() {});

                },j*100);
            });
        }

        opt.init = false;

        var data={};
        data.slideIndex=opt.next+1;
        container.trigger('revolution.slide.onchange',data);
        container.trigger('revolution.slide.onvideostop');
    }

    //////////////////////////
    //	REMOVE THE CAPTIONS //
    /////////////////////////
    function removeTheCaptions(actli,opt) {
        actli.find('.slider-caption').each(function(i) {
            var nextcaption=actli.find('.slider-caption:eq('+i+')');
            nextcaption.stop(true,true);
            clearTimeout(nextcaption.data('timer'));
            var easetype=nextcaption.data('easing');
            easetype="easeInOutSine";
            var ll = nextcaption.data('repx');
            var tt = nextcaption.data('repy');
            var oo = nextcaption.data('repo');
            var rot = nextcaption.data('rotate');
            var sca = nextcaption.data('scale');


            if (nextcaption.find('iframe').length>0) {
                var par=nextcaption.find('iframe').parent();
                var iframe=par.html();
                setTimeout(function() {
                    nextcaption.find('iframe').remove();
                    par.append(iframe);
                },nextcaption.data('speed'));
            }
            try {
                if (rot!=undefined || sca!=undefined)
                {
                    if (rot==undefined) rot=0;
                    if (sca==undefined) sca=1;
                    nextcaption.transition({'rotate':rot, 'scale':sca, 'opacity':oo,'left':ll+'px','top':tt+"px"},(nextcaption.data('speed')+10), function() { nextcaption.removeClass('noFilterClass');nextcaption.css({'visibility':'hidden'})});
                } else {
                    nextcaption.animate({'opacity':oo,'left':ll+'px','top':tt+"px"},{duration:(nextcaption.data('speed')+10), easing:easetype, complete:function() { nextcaption.removeClass('noFilterClass');nextcaption.css({'visibility':'hidden'})}});
                }
            } catch(e) {}
        });
    }

    function onYouTubePlayerAPIReady() {

    }

    //////////////////////////////////////////
    // CHANG THE YOUTUBE PLAYER STATE HERE //
    ////////////////////////////////////////
    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING) {

            var bt = $('body').find('.slider-timer');
            var opt = bt.data('opt');
            bt.stop();
            opt.videoplaying=true;
            opt.videostartednow=1;

        } else {

            var bt = $('body').find('.slider-timer');
            var opt = bt.data('opt');
            if (opt.conthover==0)
                bt.animate({'width':"100%"},{duration:((opt.delay-opt.cd)-100),queue:false, easing:"linear"});
            opt.videoplaying=false;
            opt.videostoppednow=1;
        }
    }

    ///////////////////////////////
    //	YOUTUBE VIDEO AUTOPLAY //
    ///////////////////////////////
    function onPlayerReady(event) {
        event.target.playVideo();
    }

    ////////////////////////
    // VIMEO ADD EVENT /////
    ////////////////////////
    function addEvent(element, eventName, callback) {

        if (element.addEventListener) {

            element.addEventListener(eventName, callback, false);
        }
        else {

            element.attachEvent(eventName, callback, false);
        }


    }

    //////////////////////////////////////////
    // CHANG THE YOUTUBE PLAYER STATE HERE //
    ////////////////////////////////////////
    function vimeoready(player_id) {

        var froogaloop = $f(player_id);


        froogaloop.addEvent('ready', function(data) {

        });

        froogaloop.addEvent('play', function(data) {
            var bt = $('body').find('.slider-timer');
            var opt = bt.data('opt');
            bt.stop();
            opt.videoplaying=true;
        });

        froogaloop.addEvent('finish', function(data) {
            var bt = $('body').find('.slider-timer');
            var opt = bt.data('opt');
            if (opt.conthover==0)
                bt.animate({'width':"100%"},{duration:((opt.delay-opt.cd)-100),queue:false, easing:"linear"});
            opt.videoplaying=false;
            opt.videostartednow=1;
        });

        froogaloop.addEvent('pause', function(data) {
            var bt = $('body').find('.slider-timer');
            var opt = bt.data('opt');
            if (opt.conthover==0)
                bt.animate({'width':"100%"},{duration:((opt.delay-opt.cd)-100),queue:false, easing:"linear"});
            opt.videoplaying=false;
            opt.videostoppednow=1;
        });


    }

    function vimeoready_auto(player_id) {

        var froogaloop = $f(player_id);


        froogaloop.addEvent('ready', function(data) {
            froogaloop.api('play');
        });

        froogaloop.addEvent('play', function(data) {
            var bt = $('body').find('.slider-timer');
            var opt = bt.data('opt');
            bt.stop();
            opt.videoplaying=true;
        });

        froogaloop.addEvent('finish', function(data) {
            var bt = $('body').find('.slider-timer');
            var opt = bt.data('opt');
            if (opt.conthover==0)
                bt.animate({'width':"100%"},{duration:((opt.delay-opt.cd)-100),queue:false, easing:"linear"});
            opt.videoplaying=false;
            opt.videostartednow=1;
        });

        froogaloop.addEvent('pause', function(data) {
            var bt = $('body').find('.slider-timer');
            var opt = bt.data('opt');
            if (opt.conthover==0)
                bt.animate({'width':"100%"},{duration:((opt.delay-opt.cd)-100),queue:false, easing:"linear"});
            opt.videoplaying=false;
            opt.videostoppednow=1;
        });


    }

    ////////////////////////
    // SHOW THE CAPTION  //
    ///////////////////////
    function animateTheCaptions(nextli, opt,actli) {




        nextli.find('.slider-caption').each(function(i) {

            offsetx = opt.width/2 - opt.startwidth/2;

            if (opt.bh>1) {
                opt.bw=1;
                opt.bh=1;
            }

            if (opt.bw>1) {
                opt.bw=1;
                opt.bh=1;
            }

            var xbw = opt.bw;
            var xbh = opt.bh;



            var nextcaption=nextli.find('.slider-caption:eq('+i+')');
            nextcaption.stop(true,true);


            if (nextcaption.hasClass("coloredbg")) offsetx=0;
            if (offsetx<0) offsetx=0;

            var offsety = 0; //opt.height/2 - (opt.startheight*xbh)/2;

            clearTimeout(nextcaption.data('timer'));



            // YOUTUBE AND VIMEO LISTENRES INITIALISATION

            var frameID = "iframe"+Math.round(Math.random()*1000+1);

            if (nextcaption.find('iframe').length>0) {
                var ifr = nextcaption.find('iframe');

                if (ifr.attr('src').toLowerCase().indexOf('youtube')>=0) {

                    try {
                        ifr.attr('id',frameID);

                        var player;
                        if (nextcaption.data('autoplay')==true)
                            player = new YT.Player(frameID, {
                                events: {
                                    "onStateChange": onPlayerStateChange,
                                    'onReady': onPlayerReady
                                }
                            });
                        else
                            player = new YT.Player(frameID, {
                                events: {
                                    "onStateChange": onPlayerStateChange
                                }
                            });
                        ifr.addClass("HasListener");


                    } catch(e) {}

                } else {
                    if (ifr.attr('src').toLowerCase().indexOf('vimeo')>=0) {

                        if (!ifr.hasClass("HasListener")) {
                            ifr.addClass("HasListener");
                            ifr.attr('id',frameID);
                            var isrc = ifr.attr('src');
                            var queryParameters = {}, queryString = isrc,
                                re = /([^&=]+)=([^&]*)/g, m;
                            // Creates a map with the query string parameters
                            while (m = re.exec(queryString)) {
                                queryParameters[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
                            }


                            if (queryParameters['player_id']!=undefined) {

                                isrc = isrc.replace(queryParameters['player_id'],frameID);
                            } else {
                                isrc=isrc+"&player_id="+frameID;
                            }

                            try{
                                isrc = isrc.replace('api=0','api=1');
                            } catch(e) {}

                            isrc=isrc+"&api=1";



                            ifr.attr('src',isrc);
                            var player = nextcaption.find('iframe')[0];
                            if (nextcaption.data('autoplay')==true)
                                $f(player).addEvent('ready', vimeoready_auto);
                            else
                                $f(player).addEvent('ready', vimeoready);


                        }

                    }
                }
            }



            if (nextcaption.hasClass("slider-caption-animation-randomrotate") && (opt.ie || opt.ie9)) nextcaption.removeClass("slider-caption-animation-randomrotate").addClass("slider-caption-animation-sfb");
            nextcaption.removeClass('noFilterClass');


            var imw =0;
            var imh = 0;

            if (nextcaption.find('img').length>0) {
                var im = nextcaption.find('img');
                if (im.data('ww') == undefined) im.data('ww',im.width());
                if (im.data('hh') == undefined) im.data('hh',im.height());

                var ww = im.data('ww');
                var hh = im.data('hh');


                im.width(ww*opt.bw);
                im.height(hh*opt.bh);
                imw = im.width();
                imh = im.height();
            } else {

                if (nextcaption.find('iframe').length>0) {

                    var im = nextcaption.find('iframe');
                    if (nextcaption.data('ww') == undefined) {
                        nextcaption.data('ww',im.width());
                    }
                    if (nextcaption.data('hh') == undefined) nextcaption.data('hh',im.height());

                    var ww = nextcaption.data('ww');
                    var hh = nextcaption.data('hh');

                    var nc =nextcaption;
                    if (nc.data('fsize') == undefined) nc.data('fsize',parseInt(nc.css('font-size'),0) || 0);
                    if (nc.data('pt') == undefined) nc.data('pt',parseInt(nc.css('padding-top'),0) || 0);
                    if (nc.data('pb') == undefined) nc.data('pb',parseInt(nc.css('padding-bottom'),0) || 0);
                    if (nc.data('pl') == undefined) nc.data('pl',parseInt(nc.css('padding-left'),0) || 0);
                    if (nc.data('pr') == undefined) nc.data('pr',parseInt(nc.css('padding-right'),0) || 0);

                    if (nc.data('mt') == undefined) nc.data('mt',parseInt(nc.css('margin-top'),0) || 0);
                    if (nc.data('mb') == undefined) nc.data('mb',parseInt(nc.css('margin-bottom'),0) || 0);
                    if (nc.data('ml') == undefined) nc.data('ml',parseInt(nc.css('margin-left'),0) || 0);
                    if (nc.data('mr') == undefined) nc.data('mr',parseInt(nc.css('margin-right'),0) || 0);

                    if (nc.data('bt') == undefined) nc.data('bt',parseInt(nc.css('border-top-width'),0) || 0);
                    if (nc.data('bb') == undefined) nc.data('bb',parseInt(nc.css('border-bottom-width'),0) || 0);
                    if (nc.data('bl') == undefined) nc.data('bl',parseInt(nc.css('border-left-width'),0) || 0);
                    if (nc.data('br') == undefined) nc.data('br',parseInt(nc.css('border-right-width'),0) || 0);

                    if (nc.data('lh') == undefined) nc.data('lh',parseInt(nc.css('line-height'),0) || 0);



                    var fvwidth=opt.width;
                    var fvheight=opt.height;
                    if (fvwidth>opt.startwidth) fvwidth=opt.startwidth;
                    if (fvheight>opt.startheight) fvheight=opt.startheight;

                    if (!nextcaption.hasClass('fullscreenvideo'))
                        nextcaption.css({
                            'font-size': (nc.data('fsize') * opt.bw)+"px",

                            'padding-top': (nc.data('pt') * opt.bh) + "px",
                            'padding-bottom': (nc.data('pb') * opt.bh) + "px",
                            'padding-left': (nc.data('pl') * opt.bw) + "px",
                            'padding-right': (nc.data('pr') * opt.bw) + "px",

                            'margin-top': (nc.data('mt') * opt.bh) + "px",
                            'margin-bottom': (nc.data('mb') * opt.bh) + "px",
                            'margin-left': (nc.data('ml') * opt.bw) + "px",
                            'margin-right': (nc.data('mr') * opt.bw) + "px",

                            'border-top-width': (nc.data('bt') * opt.bh) + "px",
                            'border-bottom-width': (nc.data('bb') * opt.bh) + "px",
                            'border-left-width': (nc.data('bl') * opt.bw) + "px",
                            'border-right-width': (nc.data('br') * opt.bw) + "px",

                            'line-height': (nc.data('lh') * opt.bh) + "px",
                            'height':(hh*opt.bh)+'px',
                            'white-space':"nowrap"
                        });
                    else
                        nextcaption.css({
                            'width':opt.startwidth*opt.bw,
                            'height':opt.startheight*opt.bh
                        });


                    im.width(ww*opt.bw);
                    im.height(hh*opt.bh);
                    imw = im.width();
                    imh = im.height();
                } else {

                    var nc =nextcaption;
                    if (nc.data('fsize') == undefined) nc.data('fsize',parseInt(nc.css('font-size'),0) || 0);
                    if (nc.data('pt') == undefined) nc.data('pt',parseInt(nc.css('padding-top'),0) || 0);
                    if (nc.data('pb') == undefined) nc.data('pb',parseInt(nc.css('padding-bottom'),0) || 0);
                    if (nc.data('pl') == undefined) nc.data('pl',parseInt(nc.css('padding-left'),0) || 0);
                    if (nc.data('pr') == undefined) nc.data('pr',parseInt(nc.css('padding-right'),0) || 0);

                    if (nc.data('mt') == undefined) nc.data('mt',parseInt(nc.css('margin-top'),0) || 0);
                    if (nc.data('mb') == undefined) nc.data('mb',parseInt(nc.css('margin-bottom'),0) || 0);
                    if (nc.data('ml') == undefined) nc.data('ml',parseInt(nc.css('margin-left'),0) || 0);
                    if (nc.data('mr') == undefined) nc.data('mr',parseInt(nc.css('margin-right'),0) || 0);

                    if (nc.data('bt') == undefined) nc.data('bt',parseInt(nc.css('border-top-width'),0) || 0);
                    if (nc.data('bb') == undefined) nc.data('bb',parseInt(nc.css('border-bottom-width'),0) || 0);
                    if (nc.data('bl') == undefined) nc.data('bl',parseInt(nc.css('border-left-width'),0) || 0);
                    if (nc.data('br') == undefined) nc.data('br',parseInt(nc.css('border-right-width'),0) || 0);

                    if (nc.data('lh') == undefined) nc.data('lh',parseInt(nc.css('line-height'),0) || 0);

                    nextcaption.css({
                        'font-size': (nc.data('fsize') * opt.bw)+"px",

                        'padding-top': (nc.data('pt') * opt.bh) + "px",
                        'padding-bottom': (nc.data('pb') * opt.bh) + "px",
                        'padding-left': (nc.data('pl') * opt.bw) + "px",
                        'padding-right': (nc.data('pr') * opt.bw) + "px",

                        'margin-top': (nc.data('mt') * opt.bh) + "px",
                        'margin-bottom': (nc.data('mb') * opt.bh) + "px",
                        'margin-left': (nc.data('ml') * opt.bw) + "px",
                        'margin-right': (nc.data('mr') * opt.bw) + "px",

                        'border-top-width': (nc.data('bt') * opt.bh) + "px",
                        'border-bottom-width': (nc.data('bb') * opt.bh) + "px",
                        'border-left-width': (nc.data('bl') * opt.bw) + "px",
                        'border-right-width': (nc.data('br') * opt.bw) + "px",

                        'line-height': (nc.data('lh') * opt.bh) + "px",
                        'white-space':"nowrap"


                    });
                    imh=nextcaption.outerHeight(true);
                    imw=nextcaption.outerWidth(true);
                }
            }

            nextcaption.find('a.slider-caption-link').each(function(index, element){
                $(element).height(nextcaption.height());
                $(element).width(nextcaption.width());
            });


            if (nextcaption.hasClass('slider-caption-animation-fade')) {

                nextcaption.css({'opacity':0,'left':(xbw*nextcaption.data('x')+offsetx)+'px','top':(opt.bh*nextcaption.data('y'))+"px"});
            }

            if (nextcaption.hasClass("slider-caption-animation-randomrotate")) {
                nextcaption.css({'left':(xbw*nextcaption.data('x')+offsetx)+'px','top':((xbh*nextcaption.data('y'))+offsety)+"px" });
                var sc=Math.random()*2+1;
                var ro=Math.round(Math.random()*200-100);
                var xx=Math.round(Math.random()*200-100);
                var yy=Math.round(Math.random()*200-100);
                nextcaption.data('repx',xx);
                nextcaption.data('repy',yy);
                nextcaption.data('repo',nextcaption.css('opacity'));
                nextcaption.data('rotate',ro);
                nextcaption.data('scale',sc);

                nextcaption.transition({opacity:0, scale:sc, rotate:ro, x:xx, y: yy,duration: '0ms'});
            }

            if (nextcaption.hasClass('slider-caption-animation-lfr')) {

                nextcaption.css({'opacity':1,'left':(15+opt.width)+'px','top':(opt.bh*nextcaption.data('y'))+"px"});

            }

            if (nextcaption.hasClass('slider-caption-animation-lfl')) {

                nextcaption.css({'opacity':1,'left':(-15-imw)+'px','top':(opt.bh*nextcaption.data('y'))+"px"});

            }

            if (nextcaption.hasClass('slider-caption-animation-sfl')) {

                nextcaption.css({'opacity':0,'left':((xbw*nextcaption.data('x'))-50+offsetx)+'px','top':(opt.bh*nextcaption.data('y'))+"px"});
            }

            if (nextcaption.hasClass('slider-caption-animation-sfr')) {
                nextcaption.css({'opacity':0,'left':((xbw*nextcaption.data('x'))+50+offsetx)+'px','top':(opt.bh*nextcaption.data('y'))+"px"});
            }




            if (nextcaption.hasClass('slider-caption-animation-lft')) {

                nextcaption.css({'opacity':1,'left':(xbw*nextcaption.data('x')+offsetx)+'px','top':(-25 - imh)+"px"});

            }

            if (nextcaption.hasClass('slider-caption-animation-lfb')) {
                nextcaption.css({'opacity':1,'left':(xbw*nextcaption.data('x')+offsetx)+'px','top':(25+opt.height)+"px"});
            }

            if (nextcaption.hasClass('slider-caption-animation-sft')) {
                nextcaption.css({'opacity':0,'left':(xbw*nextcaption.data('x')+offsetx)+'px','top':((opt.bh*nextcaption.data('y'))-50)+"px"});
            }

            if (nextcaption.hasClass('slider-caption-animation-sfb')) {
                nextcaption.css({'opacity':0,'left':(xbw*nextcaption.data('x')+offsetx)+'px','top':((opt.bh*nextcaption.data('y'))+50)+"px"});
            }




            nextcaption.data('timer',setTimeout(function() {
                nextcaption.css({'visibility':'visible'});
                if (nextcaption.hasClass('slider-caption-animation-fade')) {
                    nextcaption.data('repo',nextcaption.css('opacity'));
                    nextcaption.animate({'opacity':1},{duration:nextcaption.data('speed')});
                    if (opt.ie) nextcaption.addClass('noFilterClass');
                }

                if (nextcaption.hasClass("slider-caption-animation-randomrotate")) {
                    nextcaption.transition({opacity:1, scale:1, 'left':(xbw*nextcaption.data('x')+offsetx)+'px','top':(xbh*(nextcaption.data('y'))+offsety)+"px", rotate:0, x:0, y:0,duration: nextcaption.data('speed')});
                    if (opt.ie) nextcaption.addClass('noFilterClass');
                }

                if (nextcaption.hasClass('slider-caption-animation-lfr') ||
                    nextcaption.hasClass('slider-caption-animation-lfl') ||
                    nextcaption.hasClass('slider-caption-animation-sfr') ||
                    nextcaption.hasClass('slider-caption-animation-sfl') ||
                    nextcaption.hasClass('slider-caption-animation-lft') ||
                    nextcaption.hasClass('slider-caption-animation-lfb') ||
                    nextcaption.hasClass('slider-caption-animation-sft') ||
                    nextcaption.hasClass('slider-caption-animation-sfb')
                    )
                {
                    var easetype=nextcaption.data('easing');
                    if (easetype==undefined) easetype="linear";
                    nextcaption.data('repx',nextcaption.position().left);
                    nextcaption.data('repy',nextcaption.position().top);

                    nextcaption.data('repo',nextcaption.css('opacity'));
                    nextcaption.animate({'opacity':1,'left':(xbw*nextcaption.data('x')+offsetx)+'px','top':opt.bh*(nextcaption.data('y'))+"px"},{duration:nextcaption.data('speed'), easing:easetype});
                    if (opt.ie) nextcaption.addClass('noFilterClass');
                }
            },nextcaption.data('start')));

        })
    }

    ///////////////////////////
    //	-	COUNTDOWN	-	//
    /////////////////////////
    function countDown(container,opt) {
        opt.cd=0;
        opt.loop=0;
        if (opt.stopAfterLoops!=undefined && opt.stopAfterLoops>-1)
            opt.looptogo=opt.stopAfterLoops;
        else
            opt.looptogo=9999999;

        if (opt.stopAtSlide!=undefined && opt.stopAtSlide>-1)
            opt.lastslidetoshow=opt.stopAtSlide;
        else
            opt.lastslidetoshow=999;

        if (opt.looptogo==0) opt.stopLoop= 1;



        if (opt.slideamount >1 && !(opt.stopAfterLoops==0 && opt.stopAtSlide==1) ) {
            var bt=container.find('.slider-timer');
            if (bt.length>0) {
                bt.css({'width':'0%'});
                bt.animate({'width':"100%"},{duration:(opt.delay-100),queue:false, easing:"linear"});
            }

            bt.data('opt',opt);
            opt.cdint=setInterval(function() {

                if (container.data('conthover-changed') == 1) {
                    opt.conthover=	container.data('conthover');
                    container.data('conthover-changed',0);
                }

                if (opt.conthover!=1 && opt.videoplaying!=true) opt.cd=opt.cd+100;

                // EVENT TRIGGERING IN CASE VIDEO HAS BEEN STARTED
                if (opt.videostartednow==1) {
                    container.trigger('revolution.slide.onvideoplay');
                    opt.videostartednow=0;
                }

                // EVENT TRIGGERING IN CASE VIDEO HAS BEEN STOPPED
                if (opt.videostoppednow==1) {
                    container.trigger('revolution.slide.onvideostop');
                    opt.videostoppednow=0;
                }


                if (opt.cd>=opt.delay) {
                    opt.cd=0;
                    // SWAP TO NEXT BANNER
                    opt.act=opt.next;
                    opt.next=opt.next+1;
                    if (opt.next>container.find('>ul >li').length-1) {
                        opt.next=0;
                        opt.looptogo=opt.looptogo-1;
                        if (opt.loop<=0) {
                            opt.stopLoop= 1;

                        }
                    }

                    // STOP TIMER IF NO LOOP NO MORE NEEDED.
                    if (opt.stopLoop === 1 && opt.next==opt.lastslidetoshow-1) {
                        clearInterval(opt.cdint);
                        container.find('.slider-timer').css({'visibility':'hidden'});
                        container.trigger('revolution.slide.onstop');
                    }

                    // SWAP THE SLIDES
                    swapSlide(container,opt);


                    // Clear the Timer
                    if (bt.length>0) {
                        bt.css({'width':'0%'});
                        bt.animate({'width':"100%"},{duration:(opt.delay-100),queue:false, easing:"linear"});
                    }
                }
            },100);


            container.parent().hover(
                function() {
                    opt.conthover=1;
                    if (opt.onHoverStop === 1) {
                        bt.stop();
                        container.trigger('revolution.slide.onpause');
                    }
                },
                function() {
                    if (container.data('conthover')!=1) {
                        container.trigger('revolution.slide.onresume');
                        opt.conthover=0;
                        if (opt.onHoverStop == 1 && opt.videoplaying!=true) {
                            bt.animate({'width':"100%"},{duration:((opt.delay-opt.cd)-100),queue:false, easing:"linear"});
                        }
                    }
                });
        }
    }
})(jQuery);




